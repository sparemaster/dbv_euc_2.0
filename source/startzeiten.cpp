//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "startzeiten.h"
#include "datamodule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "LMDButton"
#pragma link "LMDControl"
#pragma link "LMDCustomBevelPanel"
#pragma link "LMDCustomButton"
#pragma link "LMDCustomControl"
#pragma link "LMDCustomPanel"
#pragma link "LMDSimplePanel"
#pragma link "DbAGrids"
#pragma link "dagTmplt"
#pragma resource "*.dfm"
TfrmStartTime *frmStartTime;
//---------------------------------------------------------------------------
__fastcall TfrmStartTime::TfrmStartTime(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmStartTime::FormShow(TObject *Sender)
{
	dmMain->qryGroup->Open();
}
//---------------------------------------------------------------------------

void __fastcall TfrmStartTime::FormClose(TObject *Sender, TCloseAction &Action)
{
	dmMain->qryGroup->Close();
}
//---------------------------------------------------------------------------

void __fastcall TfrmStartTime::btnCloseClick(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------
