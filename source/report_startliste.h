//----------------------------------------------------------------------------
#ifndef report_startlisteH
#define report_startlisteH
//----------------------------------------------------------------------------
#include <DB.hpp>
#include <ADODB.hpp>
#include <QRCtrls.hpp>
#include <QuickRpt.hpp>
#include "QRPDFFilt.hpp"
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
//----------------------------------------------------------------------------
class TfrmReportStartliste : public TForm
{
__published:
        TQuickRep *qrpStartliste;
        TQRBand *QRBand1;
        TQRSysData *QRSysData1;
        TQRLabel *lblSubcaption;
        TQRLabel *lblStand;
        TQRLabel *lblTitel;
        TQRBand *detailband;
        TADOQuery *qryTeams;
        TQRDBText *QRDBText1;
        TQRBand *PageFooterBand1;
        TQRSysData *QRSysData2;
        TQRLabel *QRLabel5;
        TQRGroup *gruppenkopf;
        TQRBand *gruppenfuss;
        TStringField *qryTeamsName;
        TDateTimeField *qryTeamsStartzeit;
        TStringField *qryTeamsGruppe;
        TQRDBText *QRDBText2;
        TQRDBText *QRDBText3;
        TIntegerField *qryTeamsNummer;
        TQRDBText *QRDBText4;
        TStringField *qryTeamsNummer2;
        TQRLabel *QRLabel1;
        TQRLabel *QRLabel2;
        TQRLabel *QRLabel6;
        TQRPDFFilter *QRPDFFilter1;
        TDateTimeField *qryTeamsMeldezeit;
        TQRLabel *QRLabel3;
        TQRDBText *QRDBText5;
	void __fastcall qryTeamsCalcFields(TDataSet *DataSet);
private:
public:
	virtual __fastcall TfrmReportStartliste(TComponent* AOwner);
        void __fastcall PrepareData();
        void __fastcall CloseData();
};
//----------------------------------------------------------------------------
extern PACKAGE TfrmReportStartliste *frmReportStartliste;
//----------------------------------------------------------------------------
#endif    
