//---------------------------------------------------------------------------

#ifndef varianceH
#define varianceH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DbAGrids.hpp"
#include <DBGrids.hpp>
#include <Grids.hpp>
#include "LMDControl.hpp"
#include "LMDCustomBevelPanel.hpp"
#include "LMDCustomButtonGroup.hpp"
#include "LMDCustomControl.hpp"
#include "LMDCustomGroupBox.hpp"
#include "LMDCustomPanel.hpp"
#include "LMDCustomParentPanel.hpp"
#include "LMDCustomRadioGroup.hpp"
#include "LMDRadioGroup.hpp"
#include "LMDSimplePanel.hpp"
//---------------------------------------------------------------------------
class TfrmVariance : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TLMDSimplePanel *LMDSimplePanel1;
	TDbAltGrid *tbMax;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall rgFilterChange(TObject *Sender, int ButtonIndex);
private:	// Benutzer-Deklarationen
	UnicodeString FSortField,FSortDir;
public:		// Benutzer-Deklarationen
	__fastcall TfrmVariance(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmVariance *frmVariance;
//---------------------------------------------------------------------------
#endif
