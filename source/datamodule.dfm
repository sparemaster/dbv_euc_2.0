object dmMain: TdmMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 451
  Width = 577
  object conMain: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=E:\Dokumente\RAD St' +
      'udio\Projekte\DBV EUC 2.0\datenbank.mdb;Persist Security Info=Fa' +
      'lse;Jet OLEDB:Database Password=test;'
    KeepConnection = False
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    OnConnectComplete = conMainConnectComplete
    Left = 48
    Top = 40
  end
  object qryTeams: TADOQuery
    AutoCalcFields = False
    Connection = conMain
    CursorType = ctStatic
    Filtered = True
    BeforeOpen = qryTeamsBeforeOpen
    AfterOpen = qryTeamsAfterOpen
    AfterScroll = qryTeamsAfterScroll
    OnCalcFields = qryTeamsCalcFields
    OnFilterRecord = qryTeamsFilterRecord
    Parameters = <
      item
        Name = 'Idx'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'From teams'
      'WHERE (Turnier=:Idx)'
      'ORDER BY'
      'Nummer'
      'ASC')
    Left = 48
    Top = 120
    object qryTeamsTeamIdx: TAutoIncField
      FieldName = 'TeamIdx'
      ReadOnly = True
    end
    object qryTeamsTurnier: TIntegerField
      FieldName = 'Turnier'
    end
    object qryTeamsName: TWideStringField
      FieldName = 'Name'
      Size = 40
    end
    object qryTeamsNummer: TIntegerField
      FieldName = 'Nummer'
    end
    object qryTeamsHdc: TSmallintField
      FieldName = 'Hdc'
    end
    object qryTeamsHDC_Finale: TSmallintField
      FieldName = 'HDC_Finale'
    end
    object qryTeamsAusland: TBooleanField
      FieldName = 'Ausland'
    end
    object qryTeamsBahn: TIntegerField
      FieldName = 'Bahn'
    end
    object qryTeamsBahn_ZR: TIntegerField
      FieldName = 'Bahn_ZR'
    end
    object qryTeamsBahn_F: TIntegerField
      FieldName = 'Bahn_F'
    end
    object qryTeamsAnlage: TIntegerField
      FieldName = 'Anlage'
    end
    object qryTeamsStatus: TIntegerField
      FieldName = 'Status'
    end
    object qryTeamsGruppe: TIntegerField
      FieldName = 'Gruppe'
    end
    object qryTeamsVorrunde: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Vorrunde'
      Calculated = True
    end
    object qryTeamsZwischenrunde: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Zwischenrunde'
      Calculated = True
    end
    object qryTeamsFinale: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Finale'
      Calculated = True
    end
    object qryTeamsVR1_D1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'VR1_D1'
      Calculated = True
    end
    object qryTeamsVR1_D2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'VR1_D2'
      Calculated = True
    end
    object qryTeamsVR1_D3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'VR1_D3'
      Calculated = True
    end
    object qryTeamsVR2_D1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'VR2_D1'
      Calculated = True
    end
    object qryTeamsVR2_D2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'VR2_D2'
      Calculated = True
    end
    object qryTeamsVR2_D3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'VR2_D3'
      Calculated = True
    end
    object qryTeamsF_D1: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'F_D1'
      Calculated = True
    end
    object qryTeamsF_D2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'F_D2'
      Calculated = True
    end
    object qryTeamsF_D3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'F_D3'
      Calculated = True
    end
    object qryTeamsVorrundeGesamt: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'VorrundeGesamt'
      Calculated = True
    end
  end
  object dtsTeams: TDataSource
    DataSet = qryTeams
    Left = 128
    Top = 120
  end
  object tblEUC: TADOTable
    Connection = conMain
    CursorType = ctStatic
    BeforePost = tblEUCBeforePost
    TableName = 'turnier'
    Left = 48
    Top = 184
  end
  object qryCenter: TADOQuery
    Connection = conMain
    Parameters = <>
    SQL.Strings = (
      'select * from anlage'
      'order by anlagenname ASC')
    Left = 48
    Top = 248
  end
  object qryGroup: TADOQuery
    Connection = conMain
    CursorType = ctStatic
    BeforeOpen = qryGroupBeforeOpen
    Parameters = <
      item
        Name = 'Idx'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      'from startgruppe'
      'where Turnier=:Idx'
      'order by Gruppe ASC')
    Left = 48
    Top = 304
  end
  object tblInit: TADOTable
    Connection = conMain
    TableName = 'init'
    Left = 392
    Top = 32
  end
  object dtsGroup: TDataSource
    DataSet = qryGroup
    Left = 128
    Top = 304
  end
  object conMV: TADOConnection
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\BDS Projects\DBV' +
      ' Mitgliederverwaltung\datenbank\database.mdb;Persist Security In' +
      'fo=False;Jet OLEDB:Database Password=test'
    KeepConnection = False
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 144
    Top = 40
  end
  object qryMaxSpiel: TADOQuery
    Connection = conMain
    CursorType = ctStatic
    OnCalcFields = qryMaxSpielCalcFields
    Parameters = <
      item
        Name = 'Idx'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT Max(ergebnis.spiel1) AS Maxvonspiel1, Max(ergebnis.spiel2' +
        ') AS Maxvonspiel2, Max(ergebnis.spiel3) AS Maxvonspiel3, Max(erg' +
        'ebnis.spiel4) AS Maxvonspiel4, Max(ergebnis.spiel5) AS Maxvonspi' +
        'el5, Max(ergebnis.spiel6) AS Maxvonspiel6, Max(ergebnis.spiel7) ' +
        'AS Maxvonspiel7, Max(ergebnis.spiel8) AS Maxvonspiel8, Max(ergeb' +
        'nis.spiel9) AS Maxvonspiel9'
      'FROM ergebnis'
      'GROUP BY ergebnis.Turnier'
      'HAVING (ergebnis.Turnier=:Idx)')
    Left = 384
    Top = 296
  end
  object qryAnlage: TADOQuery
    Connection = conMV
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from anlage'
      'order by anlagenname ASC')
    Left = 208
    Top = 41
  end
  object qryMaxResults: TADOQuery
    Connection = conMain
    CursorType = ctStatic
    Filtered = True
    BeforeOpen = qryMaxResultsBeforeOpen
    OnFilterRecord = qryMaxResultsFilterRecord
    Parameters = <
      item
        Name = 'Idx'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = '2'
      end>
    SQL.Strings = (
      'select * '
      'from MaxResults '
      'where (Turnier=:Idx)'
      'ORDER BY'
      'MaxAll DESC,Gruppe ASC')
    Left = 48
    Top = 368
  end
  object dtsMaxResults: TDataSource
    DataSet = qryMaxResults
    Left = 128
    Top = 368
  end
  object qryStartCount: TADOQuery
    Connection = conMain
    CursorType = ctStatic
    BeforeOpen = qryStartCountBeforeOpen
    AfterOpen = qryStartCountAfterOpen
    Parameters = <
      item
        Name = 'Idx'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT AnzahlStarts.Turnier, Sum(AnzahlStarts.VR1) AS VR1, Sum(A' +
        'nzahlStarts.VR2) AS VR2, Sum(AnzahlStarts.F) AS F'
      'FROM AnzahlStarts'
      'GROUP BY AnzahlStarts.Turnier'
      'HAVING (((AnzahlStarts.Turnier)=:Idx));')
    Left = 384
    Top = 368
  end
  object qryVariance: TADOQuery
    Connection = conMain
    CursorType = ctStatic
    BeforeOpen = qryVarianceBeforeOpen
    OnCalcFields = qryVarianceCalcFields
    Parameters = <
      item
        Name = 'turnier'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = '4'
      end>
    SQL.Strings = (
      
        'SELECT spieler.SpielerIdx, spieler.Nachname, spieler.Vorname, sp' +
        'ieler.Name, spieler.Gruppe, spieler.mnummer, spieler.Team, spiel' +
        'er.anlage, spieler.turnier,ergebnis.spiel1,ergebnis.spiel2,ergeb' +
        'nis.spiel3,ergebnis.spiel4,ergebnis.spiel5,ergebnis.spiel6,ergeb' +
        'nis.spiel7,ergebnis.spiel8,ergebnis.spiel9'
      
        'FROM spieler INNER JOIN ergebnis ON spieler.SpielerIdx = ergebni' +
        's.Spieler'
      
        'WHERE (((spieler.turnier)=:turnier) AND ((ergebnis.spiel1)>0) AN' +
        'D ((ergebnis.spiel2)>0) AND ((ergebnis.spiel3)>0) AND ((ergebnis' +
        '.spiel4)>0) AND ((ergebnis.spiel5)>0) AND ((ergebnis.spiel6)>0) ' +
        'AND ((ergebnis.spiel7)>0) AND ((ergebnis.spiel8)>0) AND ((ergebn' +
        'is.spiel9)>0));')
    Left = 384
    Top = 232
    object qryVarianceNachname: TWideStringField
      FieldName = 'Nachname'
    end
    object qryVarianceVorname: TWideStringField
      FieldName = 'Vorname'
    end
    object qryVarianceName: TWideStringField
      FieldName = 'Name'
      Size = 42
    end
    object qryVarianceGruppe: TWideStringField
      FieldName = 'Gruppe'
      Size = 10
    end
    object qryVariancemnummer: TIntegerField
      FieldName = 'mnummer'
    end
    object qryVarianceTeam: TIntegerField
      FieldName = 'Team'
    end
    object qryVarianceanlage: TIntegerField
      FieldName = 'anlage'
    end
    object qryVarianceturnier: TIntegerField
      FieldName = 'turnier'
    end
    object qryVariancespiel1: TSmallintField
      FieldName = 'spiel1'
    end
    object qryVariancespiel2: TSmallintField
      FieldName = 'spiel2'
    end
    object qryVariancespiel3: TSmallintField
      FieldName = 'spiel3'
    end
    object qryVariancespiel4: TIntegerField
      FieldName = 'spiel4'
    end
    object qryVariancespiel5: TIntegerField
      FieldName = 'spiel5'
    end
    object qryVariancespiel6: TIntegerField
      FieldName = 'spiel6'
    end
    object qryVariancespiel7: TIntegerField
      FieldName = 'spiel7'
    end
    object qryVariancespiel8: TIntegerField
      FieldName = 'spiel8'
    end
    object qryVariancespiel9: TIntegerField
      FieldName = 'spiel9'
    end
    object qryVarianceVarianz: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Varianz'
      Calculated = True
    end
    object qryVarianceSpielerIdx: TAutoIncField
      FieldName = 'SpielerIdx'
      ReadOnly = True
    end
    object qryVarianceMinimum: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Minimum'
      Calculated = True
    end
    object qryVarianceMaximum: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Maximum'
      Calculated = True
    end
  end
  object dtsVariance: TDataSource
    DataSet = qryVarianceTarget
    Left = 472
    Top = 232
  end
  object qryVarianceTarget: TADOQuery
    Connection = conMain
    CursorType = ctStatic
    BeforeOpen = qryVarianceBeforeOpen
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from variance'
      ''
      'order by'
      'Varianzzz ASC, Minimum ASC')
    Left = 472
    Top = 168
    object qryVarianceTargetIdx: TAutoIncField
      FieldName = 'Idx'
      ReadOnly = True
    end
    object qryVarianceTargetSpielerIdx: TIntegerField
      FieldName = 'SpielerIdx'
    end
    object qryVarianceTargetMinimum: TIntegerField
      FieldName = 'Minimum'
    end
    object qryVarianceTargetMaximum: TIntegerField
      FieldName = 'Maximum'
    end
    object qryVarianceTargetSpielername: TStringField
      FieldKind = fkLookup
      FieldName = 'Spielername'
      LookupDataSet = tblSpieler
      LookupKeyFields = 'SpielerIdx'
      LookupResultField = 'Name'
      KeyFields = 'SpielerIdx'
      Size = 50
      Lookup = True
    end
    object qryVarianceTargetVarianzzz: TIntegerField
      FieldName = 'Varianzzz'
    end
  end
  object tblSpieler: TADOTable
    Connection = conMain
    CursorType = ctStatic
    TableName = 'spieler'
    Left = 472
    Top = 128
  end
  object cmdUpdate: TADOCommand
    Connection = conMain
    ExecuteOptions = [eoExecuteNoRecords]
    Parameters = <>
    Left = 472
    Top = 32
  end
  object eucEUC: TDBVEUC
    Connection = conMain
    IdxField = 'TurnierIdx'
    Target = etEUC
    HDCBasis = 220
    HDCProzent = 75
    Left = 296
    Top = 232
  end
  object eucTeam: TDBVEUCTeam
    Connection = conMain
    IdxField = 'TeamIdx'
    Target = etEUC
    MVConnection = conMV
    Left = 296
    Top = 296
  end
end
