//---------------------------------------------------------------------------

#ifndef spielerliste_mvH
#define spielerliste_mvH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "LMDButton.hpp"
#include "LMDControl.hpp"
#include "LMDCustomBevelPanel.hpp"
#include "LMDCustomButton.hpp"
#include "LMDCustomControl.hpp"
#include "LMDCustomPanel.hpp"
#include "LMDSimplePanel.hpp"
#include "DbAGrids.hpp"
#include <DBGrids.hpp>
#include <Grids.hpp>
#include "LMDBaseEdit.hpp"
#include "LMDCustomEdit.hpp"
#include "LMDCustomExtCombo.hpp"
#include "LMDCustomListComboBox.hpp"
#include "LMDCustomMaskEdit.hpp"
#include <ADODB.hpp>
#include <DB.hpp>
#include "LMDListComboBox.hpp"

//---------------------------------------------------------------------------
class TfrmSpielerlisteMV : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TLMDSimplePanel *LMDSimplePanel1;
	TLMDButton *btnOK;
	TLMDButton *btnCancel;
	TDbAltGrid *DbAltGrid1;
	TADOQuery *qryMitgliederMV;
	TDataSource *dtsMitgliederMV;
	TLMDLabeledListComboBox *cmbAnlage;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall DbAltGrid1TitleClick(TColumn *Column);
	void __fastcall btnCancelClick(TObject *Sender);
	void __fastcall btnOKClick(TObject *Sender);
	void __fastcall cmbAnlageChange(TObject *Sender);
private:	// Benutzer-Deklarationen
	UnicodeString FSortDir, FSortField;
	int FNummer;
public:		// Benutzer-Deklarationen
	__fastcall TfrmSpielerlisteMV(TComponent* Owner);
	int __fastcall Search();
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmSpielerlisteMV *frmSpielerlisteMV;
//---------------------------------------------------------------------------
#endif
