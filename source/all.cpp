//---------------------------------------------------------------------------


#pragma hdrstop

#include "all.h"
#include "datamodule.h"
#include "DbAGrids.hpp"
#include "fortschritt.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)

void __fastcall fillAnlageCombo(TADOConnection *con,TLMDLabeledListComboBox *combo)
{
	combo->Items->Clear();
	TADOQuery *q=new TADOQuery(con->Owner);
	q->Connection=con;
	q->SQL->Add("select * from anlage order by anlagenname ASC");

	try {
		q->Open();
		while (!q->Eof)
		{
			TDBVCenterObject *anlage=new TDBVCenterObject;
			anlage->Idx=q->FieldByName("idx")->AsInteger;
			anlage->Name=q->FieldByName("anlagenname")->AsString;
			anlage->Anlagennummer=q->FieldByName("anlagennummer")->AsInteger;
			combo->Items->AddObject(AnsiString(anlage->Name),(TObject*)anlage);
			q->Next();
		}
	}
	__finally {
		q->Close();
		delete q;
    }

}
//---------------------------------------------------------------------------

int __fastcall getAnlageComboIdx2(TLMDLabeledListComboBox *combo, int anlagennummer)
{
	int ret=-1;

	for (int i=0; i<combo->Items->Count; i++) {
		TDBVCenterObject *a=(TDBVCenterObject*)(combo->Items->Objects[i] );
		if (a->Anlagennummer==anlagennummer) {
			ret=i;
			break;
		}
	}

	return ret;
}
//---------------------------------------------------------------------------

void __fastcall getMVMemberData(TADOConnection *con, TDBVEUCPlayerData &data)
{
	if (data.mnummer==0) {
		return;
	}
	
	TADOQuery *q=new TADOQuery(con->Owner);
	q->Connection=con;
	q->SQL->Add("select m.*, a.anlagennummer from mitglied m, anlage a where (m.anlage=a.idx) and (mnummer="+IntToStr(data.mnummer)+")");
	try {
		q->Open();
		if (q->RecordCount==1) {
			data.idx=q->FieldByName("Idx")->AsInteger;
			data.mnummer=q->FieldByName("mnummer")->AsInteger;
			strncpy(data.vorname,q->FieldByName("vorname")->AsAnsiString.c_str(),DBV_MEMBERVORNAME_Size);
			strncpy(data.nachname,q->FieldByName("nachname")->AsAnsiString.c_str(),DBV_MEMBERNACHNAME_Size);
			if (q->FieldByName("weiblich")->AsBoolean) {
				strcpy(data.gruppe,AnsiString("Damen").c_str());
			}
			else {
				strcpy(data.gruppe,AnsiString("Herren").c_str());
			}
			data.spiele_dbv=q->FieldByName("spiele")->AsInteger;
			data.pins_dbv=q->FieldByName("pins")->AsInteger;
			data.anlage=q->FieldByName("anlagennummer")->AsInteger;

			if (data.spiele_dbv==0) {
				data.schnitt_dbv=0;
			}
			else {
				data.schnitt_dbv=RoundTo(((double)data.pins_dbv)/((double)data.spiele_dbv)*100,0);
			}
		}
	}
	__finally {
		q->Close();
		delete q;
    }
}
//---------------------------------------------------------------------------


