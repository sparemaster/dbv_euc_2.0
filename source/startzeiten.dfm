object frmStartTime: TfrmStartTime
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = 'Startzeiten'
  ClientHeight = 206
  ClientWidth = 389
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LMDSimplePanel1: TLMDSimplePanel
    Left = 0
    Top = 158
    Width = 389
    Height = 48
    Align = alBottom
    Bevel.StyleInner = bvRaised
    Bevel.StyleOuter = bvFrameLowered
    Bevel.Mode = bmCustom
    TabOrder = 0
    ExplicitLeft = 8
    ExplicitTop = 248
    ExplicitWidth = 492
    object btnClose: TLMDButton
      Left = 8
      Top = 12
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Schliessen'
      TabOrder = 0
      OnClick = btnCloseClick
      ButtonStyle = ubsWin40Ext
    end
  end
  object DbAltGrid1: TDbAltGrid
    Left = 8
    Top = 16
    Width = 371
    Height = 129
    AltColors.Stripe1 = 16776176
    AltOptions = [dagAutoWidth, dagDiscreteBandHeight, dagStripedBackground, dagStripedRows]
    DataSource = dmMain.dtsGroup
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Gruppe'
        ReadOnly = True
        Title.Alignment = taCenter
        Width = 50
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Meldezeit'
        Title.Alignment = taCenter
        Width = 154
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'Startzeit'
        Title.Alignment = taCenter
        Width = 148
      end>
  end
end
