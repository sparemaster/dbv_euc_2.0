//---------------------------------------------------------------------------

#ifndef eucH
#define eucH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "LMDButton.hpp"
#include "LMDControl.hpp"
#include "LMDCustomBevelPanel.hpp"
#include "LMDCustomButton.hpp"
#include "LMDCustomControl.hpp"
#include "LMDCustomPanel.hpp"
#include "LMDSimplePanel.hpp"
#include "LMDBaseEdit.hpp"
#include "LMDCustomEdit.hpp"
#include "LMDEdit.hpp"
//---------------------------------------------------------------------------
class TfrmEUC : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TLMDSimplePanel *LMDSimplePanel1;
	TLMDButton *btnOK;
	TLMDButton *btnCancel;
	TLMDLabeledEdit *edName1;
	TLMDLabeledEdit *edName2;
	void __fastcall btnOKClick(TObject *Sender);
	void __fastcall btnCancelClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
private:	// Benutzer-Deklarationen
	void __fastcall Edit2Data();
	void __fastcall Data2Edit();
	void __fastcall clearEdits();
public:		// Benutzer-Deklarationen
	__fastcall TfrmEUC(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmEUC *frmEUC;
//---------------------------------------------------------------------------
#endif
