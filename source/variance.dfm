object frmVariance: TfrmVariance
  Left = 0
  Top = 0
  Caption = 'best'#228'ndigste Spieler'
  ClientHeight = 225
  ClientWidth = 435
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LMDSimplePanel1: TLMDSimplePanel
    Left = 0
    Top = 0
    Width = 435
    Height = 225
    Align = alClient
    Bevel.BorderWidth = 10
    Bevel.Mode = bmCustom
    TabOrder = 0
    object tbMax: TDbAltGrid
      Left = 10
      Top = 10
      Width = 415
      Height = 205
      Align = alClient
      AltColors.BandLine = 15790320
      AltColors.ColLine = 15790320
      AltColors.FixedLine = 14474460
      AltColors.RowLine = 15790320
      AltColors.Stripe1 = 16776176
      AltOptions = [dagAutoWidth, dagBandLines, dagDiscreteBandHeight, dagStripedBackground, dagStripedRows]
      DataSource = dmMain.dtsVariance
      FixedColor = clWhite
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Spielername'
          Width = 120
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Minimum'
          Title.Alignment = taCenter
          Title.Caption = 'niedrigstes Spiel'
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Maximum'
          Title.Alignment = taCenter
          Title.Caption = 'h'#246'chstes Spiel'
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Varianzzz'
          Title.Alignment = taCenter
          Title.Caption = 'Differenz'
        end>
    end
  end
end
