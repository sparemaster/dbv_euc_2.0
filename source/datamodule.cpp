//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "datamodule.h"
#include "Registry.hpp"
#include "all.h"
#include "main.h"
#include "MaxResults.h"
#include "DBVRoutines.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DBVEUCTeam"
#pragma link "DBVEUCPlayer"
#pragma link "DBVEUCResult"
#pragma link "DBVEUCBaseComponent"
#pragma link "DBVEUCResultClass"
#pragma link "DBVEUCTeamClass"
#pragma link "DBVEUC"
#pragma resource "*.dfm"
TdmMain *dmMain;
//---------------------------------------------------------------------------
__fastcall TdmMain::TdmMain(TComponent* Owner)
	: TDataModule(Owner)
{
}
//---------------------------------------------------------------------------

_GUID __fastcall TdmMain::getProgID()
{
	tblInit->Open();
	_GUID ret=Sysutils::StringToGUID(tblInit->FieldByName("ProgID")->AsString);
	tblInit->Close();
	return ret;
}
//---------------------------------------------------------------------------

bool __fastcall TdmMain::selectEUC(int EUCIdx, bool reload)
{
	bool ret=eucEUC->readFromDB(EUCIdx);
	if (ret && reload) {
        eucTeam->Active=false;
		qryTeams->Close();
		qryTeams->Parameters->ParamByName("Idx")->Value=EUCIdx;
		qryTeams->Open();
	}
	return ret;
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryTeamsAfterScroll(TDataSet *DataSet)
{
	eucTeam->readFromDB(DataSet->FieldByName("TeamIdx")->AsInteger);
}
//---------------------------------------------------------------------------


void __fastcall TdmMain::qryTeamsCalcFields(TDataSet *DataSet)
{
	DataSet->DisableControls();
	Screen->Cursor=crHourGlass;
	try {
		eucTeam->readFromDB(DataSet->FieldByName("TeamIdx")->AsInteger);
		if (eucTeam->ErgebnisVRBrutto==0) {
			DataSet->FieldByName("Vorrunde")->Clear();
		}
		else {
			DataSet->FieldByName("Vorrunde")->AsInteger=eucTeam->ErgebnisVRBrutto;
		}
		if (eucTeam->ErgebnisZRBrutto==0) {
			DataSet->FieldByName("Zwischenrunde")->Clear();
		}
		else {
			DataSet->FieldByName("Zwischenrunde")->AsInteger=eucTeam->ErgebnisZRBrutto;
		}
		if (eucTeam->ErgebnisFBrutto==0) {
			DataSet->FieldByName("Finale")->Clear();
		}
		else {
			DataSet->FieldByName("Finale")->AsInteger=eucTeam->ErgebnisFBrutto;
		}
		if ((eucTeam->ErgebnisVRBrutto==0) && (eucTeam->ErgebnisZRBrutto==0)) {
			DataSet->FieldByName("VorrundeGesamt")->AsInteger=0;
		}
		else {
			DataSet->FieldByName("VorrundeGesamt")->AsInteger=eucTeam->ErgebnisVRBrutto+eucTeam->ErgebnisZRBrutto;
		}

		if (eucTeam->TeamHdcFinal==0) {
			DataSet->FieldByName("HDC_Finale")->Clear();
		}
		else {
			DataSet->FieldByName("HDC_Finale")->AsInteger=eucTeam->TeamHdcFinal;
		}

		TDBVEUCResultSumData data=eucTeam->getResultSumData();

		if (data.DVR1_1==0) DataSet->FieldByName("VR1_D1")->Clear(); else DataSet->FieldByName("VR1_D1")->AsInteger=data.DVR1_1;
		if (data.DVR1_2==0) DataSet->FieldByName("VR1_D2")->Clear(); else DataSet->FieldByName("VR1_D2")->AsInteger=data.DVR1_2;
		if (data.DVR1_3==0) DataSet->FieldByName("VR1_D3")->Clear(); else DataSet->FieldByName("VR1_D3")->AsInteger=data.DVR1_3;

		if (data.DVR2_1==0) DataSet->FieldByName("VR2_D1")->Clear(); else DataSet->FieldByName("VR2_D1")->AsInteger=data.DVR2_1;
		if (data.DVR2_2==0) DataSet->FieldByName("VR2_D2")->Clear(); else DataSet->FieldByName("VR2_D2")->AsInteger=data.DVR2_2;
		if (data.DVR2_3==0) DataSet->FieldByName("VR2_D3")->Clear(); else DataSet->FieldByName("VR2_D3")->AsInteger=data.DVR2_3;

		if (data.DF_1==0) DataSet->FieldByName("F_D1")->Clear(); else DataSet->FieldByName("F_D1")->AsInteger=data.DF_1;
		if (data.DF_2==0) DataSet->FieldByName("F_D2")->Clear(); else DataSet->FieldByName("F_D2")->AsInteger=data.DF_2;
		if (data.DF_3==0) DataSet->FieldByName("F_D3")->Clear(); else DataSet->FieldByName("F_D3")->AsInteger=data.DF_3;
	}
	__finally {
		Screen->Cursor=crDefault;
		DataSet->EnableControls();
    }
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryGroupBeforeOpen(TDataSet *DataSet)
{
	qryGroup->Parameters->ParamByName("Idx")->Value=eucEUC->Idx;
}
//---------------------------------------------------------------------------

TDBVEUCItemType __fastcall TdmMain::getCurrentRound()
{
	TDBVEUCItemType ret;
	qryMaxSpiel->Close();
	qryMaxSpiel->Parameters->ParamByName("Idx")->Value=dmMain->eucEUC->Idx;
	qryMaxSpiel->Open();
	if (qryMaxSpiel->FieldByName("Maxvonspiel7")->AsInteger>0) {
		ret=itAuswertungF;
	}
	else if (qryMaxSpiel->FieldByName("Maxvonspiel4")->AsInteger>0) {
			ret=itAuswertungVR2;
		 }
		 else {
			 ret=itAuswertungVR1;
		 }
	return ret;
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryTeamsBeforeOpen(TDataSet *DataSet)
{
	Screen->Cursor=crHourGlass;
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryTeamsAfterOpen(TDataSet *DataSet)
{
	Screen->Cursor=crDefault;
	qryStartCount->Close();
	qryStartCount->Open();
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryMaxResultsBeforeOpen(TDataSet *DataSet)
{
	qryMaxResults->Parameters->ParamByName("Idx")->Value=eucEUC->Idx;
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::DataModuleCreate(TObject *Sender)
{
	conMain->Close();
	conMV->Close();
	db_path_mv="";
	db_path="";
	db_name="datenbank.mdb";
	//hole Pfad aus der Registry
	TRegistry *reg=new TRegistry();
	reg->RootKey=HKEY_LOCAL_MACHINE;
	bool open=reg->OpenKey(regstr_mv,false);
	if (open) {
		db_path_mv=reg->ReadString("DatabasePath")+"\\";
		reg->CloseKey();
	}
	if (db_path_mv=="") {
		MessageDlg("Die Mitgliederverwaltung ist auf diesem PC nicht ordnungsgem�� installiert. \r\nSpielerdaten k�nnen nicht aktualisiert werden.",mtError,TMsgDlgButtons()<<mbOK,0);
	}

	open=reg->OpenKey(regstr,false);
	if (open) {
		db_path=reg->ReadString("DatabasePath");
		if ((db_path=="\\") || (db_path==""))
			db_path=ExtractFilePath(Application->ExeName);

		db_name=reg->ReadString("DatabaseName");
		if (db_name=="") {
			db_name="datenbank.mdb";
		}
		reg->CloseKey();
	}
	else {
		db_path=ExtractFilePath(Application->ExeName);
	}

	if (db_path[db_path.Length()]=='\\') {
		db_path=db_path.SubString(1,db_path.Length()-1);
	}

	delete reg;

	conMain->Close();
	conMV->Close();

	conMain->ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+db_path+"\\"+db_name+";Persist Security Info=False;Jet OLEDB:Database Password=test";
	conMV->ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+db_path_mv+"\\database.mdb;Persist Security Info=False";
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryStartCountBeforeOpen(TDataSet *DataSet)
{
	qryStartCount->Parameters->ParamByName("Idx")->Value=eucEUC->Idx;
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryStartCountAfterOpen(TDataSet *DataSet)
{
	frmMain->showFooterValues();	
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryMaxResultsFilterRecord(TDataSet *DataSet, bool &Accept)

{
	if (frmMaxResults->Visible) {
		switch (frmMaxResults->rgFilter->ItemIndex) {
			case 0: Accept=DataSet->FieldByName("Gruppe")->AsString=="Damen";break;
			case 1: Accept=DataSet->FieldByName("Gruppe")->AsString=="Herren";break;
			case 2: Accept=true;break;
        default:
            ;
		}
	}
	else
		Accept=true;
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryMaxSpielCalcFields(TDataSet *DataSet)
{
	//berechne Varianz
	int max1=getMaxValue(DataSet->FieldByName("Spiel1")->AsInteger,DataSet->FieldByName("Spiel2")->AsInteger,DataSet->FieldByName("Spiel3")->AsInteger);
	int max2=getMaxValue(DataSet->FieldByName("Spiel4")->AsInteger,DataSet->FieldByName("Spiel5")->AsInteger,DataSet->FieldByName("Spiel6")->AsInteger);
	int max3=getMaxValue(DataSet->FieldByName("Spiel7")->AsInteger,DataSet->FieldByName("Spiel8")->AsInteger,DataSet->FieldByName("Spiel9")->AsInteger);

	int min1=getMinValue(DataSet->FieldByName("Spiel1")->AsInteger,DataSet->FieldByName("Spiel2")->AsInteger,DataSet->FieldByName("Spiel3")->AsInteger);
	int min2=getMinValue(DataSet->FieldByName("Spiel4")->AsInteger,DataSet->FieldByName("Spiel5")->AsInteger,DataSet->FieldByName("Spiel6")->AsInteger);
	int min3=getMinValue(DataSet->FieldByName("Spiel7")->AsInteger,DataSet->FieldByName("Spiel8")->AsInteger,DataSet->FieldByName("Spiel9")->AsInteger);


	DataSet->FieldByName("Varianz")->AsInteger=getMaxValue(max1,max2,max3)-getMinValue(min1,min2,min3);
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryVarianceBeforeOpen(TDataSet *DataSet)
{
	qryVariance->Parameters->ParamByName("turnier")->Value=eucEUC->Idx;
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryVarianceCalcFields(TDataSet *DataSet)
{
	//berechne Varianz
	int max1=getMaxValue(DataSet->FieldByName("Spiel1")->AsInteger,DataSet->FieldByName("Spiel2")->AsInteger,DataSet->FieldByName("Spiel3")->AsInteger);
	int max2=getMaxValue(DataSet->FieldByName("Spiel4")->AsInteger,DataSet->FieldByName("Spiel5")->AsInteger,DataSet->FieldByName("Spiel6")->AsInteger);
	int max3=getMaxValue(DataSet->FieldByName("Spiel7")->AsInteger,DataSet->FieldByName("Spiel8")->AsInteger,DataSet->FieldByName("Spiel9")->AsInteger);

	int min1=getMinValue(DataSet->FieldByName("Spiel1")->AsInteger,DataSet->FieldByName("Spiel2")->AsInteger,DataSet->FieldByName("Spiel3")->AsInteger);
	int min2=getMinValue(DataSet->FieldByName("Spiel4")->AsInteger,DataSet->FieldByName("Spiel5")->AsInteger,DataSet->FieldByName("Spiel6")->AsInteger);
	int min3=getMinValue(DataSet->FieldByName("Spiel7")->AsInteger,DataSet->FieldByName("Spiel8")->AsInteger,DataSet->FieldByName("Spiel9")->AsInteger);


//	DataSet->FieldByName("Varianz")->AsInteger=getMaxValue(max1,max2,max3)-getMinValue(min1,min2,min3);
	DataSet->FieldByName("Minimum")->AsInteger=getMinValue(min1,min2,min3);
	DataSet->FieldByName("Maximum")->AsInteger=getMaxValue(min1,min2,min3);
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::conMainConnectComplete(TADOConnection *Connection, const Error *Error,
          TEventStatus &EventStatus)
{
	//Update
	bool IsError=false;
	tblInit->Open();
	int version=tblInit->FieldByName("version")->AsInteger;
	if (version<=4) {
		cmdUpdate->CommandText="ALTER TABLE turnier ADD EventGUID GUID";
		try {
			cmdUpdate->Execute();
		}
		catch (Exception &E) {
			IsError=true;
		}
		if (!IsError) {
			tblInit->Edit();
			tblInit->FieldByName("version")->AsInteger=5;
			tblInit->Post();
			version=5;
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::tblEUCBeforePost(TDataSet *DataSet)
{
	TGUID eventGUID;
	if (((TGuidField*)DataSet->FieldByName("EventGUID"))->IsNull) {
    	((TGuidField*)DataSet->FieldByName("EventGUID"))->AsGuid=getNewGUID();
	}
}
//---------------------------------------------------------------------------

void __fastcall TdmMain::qryTeamsFilterRecord(TDataSet *DataSet, bool &Accept)
{
	if (!frmMain->edFilter->Text.IsEmpty()) {
		UnicodeString f=frmMain->edFilter->Text.UpperCase();
		UnicodeString d=DataSet->FieldByName("Name")->AsString.UpperCase();
		Accept=d.Pos(f)>0;
	}
	else {
		Accept=true;
	}
}
//---------------------------------------------------------------------------

