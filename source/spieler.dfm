object frmSpieler: TfrmSpieler
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = 'neuen Spieler eingeben'
  ClientHeight = 282
  ClientWidth = 372
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LMDAssist1: TLMDAssist
    Left = 0
    Top = 0
    Width = 372
    Height = 282
    ActivePage = aspNummer
    CustomStrings.Strings = (
      'Neu'
      'btn2'
      '< Zur'#252'ck'
      'Weiter >'
      'Abbrechen'
      'Fertig!'
      'Schliessen')
    TabOrder = 0
    ThemeMode = ttmPlatform
    OnCancelClick = LMDAssist1CancelClick
    OnCustomClick = LMDAssist1CustomClick
    OnCompleted = LMDAssist1Completed
    object aspNummer: TLMDAssistPage
      Left = 2
      Top = 2
      Width = 368
      Height = 231
      object lblSpieler: TLMDLabel
        Left = 190
        Top = 102
        Width = 44
        Height = 15
        Bevel.Mode = bmCustom
        Options = []
        Caption = 'lblSpieler'
      end
      object LMDSimpleLabel1: TLMDSimpleLabel
        Left = 12
        Top = 8
        Width = 344
        Height = 34
        Alignment = agCenter
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        MultiLine = True
        ParentFont = False
        Caption = 
          'Bitte die EDV-Nummer des Mitglieds angeben.'#13#10'F'#252'r Spieler ohne ED' +
          'V-Nummer bitte 0 eingeben.'
        Options = []
      end
      object Bevel2: TBevel
        Left = 12
        Top = 45
        Width = 344
        Height = 20
        Shape = bsTopLine
      end
      object edEDV: TLMDLabeledMaskEdit
        Left = 94
        Top = 98
        Width = 58
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 0
        OnChange = edEDVChange
        CustomButtons = <>
        MaskType = meInteger
        TimeSettings.AMSign = 'am'
        TimeSettings.PMSign = 'pm'
        TimeSettings.MSign = 'm'
        LabelPosition = lpLeft
        LabelSpacing = 6
        EditLabel.Width = 64
        EditLabel.Height = 15
        EditLabel.Alignment = agBottomLeft
        EditLabel.AutoSize = False
        EditLabel.Caption = 'EDV-Nummer'
        Value = 0
      end
      object btnSearch: TLMDDockButton
        Left = 153
        Top = 98
        Width = 22
        Height = 21
        TabOrder = 1
        OnClick = btnSearchClick
        ButtonStyle = ubsWin40Ext
        Glyph.Data = {
          E6000000424DE60000000000000076000000280000000D0000000E0000000100
          0400000000007000000000000000000000001000000010000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3000333333333333300033333333330030003337773330103000337000770103
          3000370FFFF01033300030FFF8880333300030FFF8880733300030FFF8880733
          300030FFF88807333000370F8880733330003370000733333000333777733333
          30003333333333333000}
        Control = edEDV
        GlyphKind = gkSearch
      end
    end
    object aspDaten: TLMDAssistPage
      Left = 2
      Top = 2
      Width = 368
      Height = 231
      OnCompleted = aspDatenCompleted
      OnBeforeShowPage = aspDatenBeforeShowPage
      object Bevel1: TBevel
        Left = 12
        Top = 45
        Width = 344
        Height = 20
        Shape = bsTopLine
      end
      object lblCaption2: TLMDSimpleLabel
        Left = 12
        Top = 8
        Width = 344
        Height = 34
        Alignment = agCenter
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        MultiLine = True
        ParentFont = False
        Caption = 
          'Bitte die EDV-Nummer des Mitglieds angeben.'#13#10'F'#252'r Spieler ohne ED' +
          'V-Nummer bitte 0 eingeben.'
        Options = []
      end
      object edNachname: TLMDLabeledEdit
        Left = 36
        Top = 98
        Width = 136
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 0
        OnChange = edNachnameChange
        CustomButtons = <>
        PasswordChar = #0
        EditLabel.Width = 52
        EditLabel.Height = 15
        EditLabel.Caption = 'Nachname'
      end
      object edVorname: TLMDLabeledEdit
        Left = 190
        Top = 98
        Width = 136
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 1
        OnChange = edVornameChange
        CustomButtons = <>
        PasswordChar = #0
        EditLabel.Width = 44
        EditLabel.Height = 15
        EditLabel.Caption = 'Vorname'
      end
      object cmbGruppe: TLMDLabeledListComboBox
        Left = 36
        Top = 150
        Width = 136
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        Caret.CanEnable = False
        TabOrder = 2
        OnChange = cmbGruppeChange
        Items.Strings = (
          'Damen'
          'Herren')
        ItemIndex = -1
        Style = csDropDownList
        EditLabel.Width = 37
        EditLabel.Height = 15
        EditLabel.Caption = 'Gruppe'
      end
      object cbAusland: TLMDCheckBox
        Left = 215
        Top = 156
        Width = 105
        Height = 17
        Caption = 'Ausland'
        Alignment.Alignment = agTopLeft
        Alignment.Spacing = 4
        TabOrder = 3
        OnChange = cbAuslandChange
      end
    end
    object LMDAssistPage1: TLMDAssistPage
      Left = 2
      Top = 2
      Width = 368
      Height = 231
      OnBeforeShowPage = LMDAssistPage1BeforeShowPage
      object lblCaption3: TLMDSimpleLabel
        Left = 12
        Top = 8
        Width = 344
        Height = 34
        Alignment = agCenter
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBtnText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        MultiLine = True
        ParentFont = False
        Caption = 
          'Bitte die vorliegenden Schnitte angeben und anschlie'#223'end auf Fer' +
          'tig! klicken'
        Options = []
      end
      object Bevel3: TBevel
        Left = 12
        Top = 45
        Width = 344
        Height = 20
        Shape = bsTopLine
      end
      object edSpieleDBV: TLMDLabeledMaskEdit
        Left = 36
        Top = 83
        Width = 50
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 0
        OnChange = edSpieleDBVChange
        OnExit = edSpieleDBVExit
        Alignment = taRightJustify
        CustomButtons = <>
        MaskType = meInteger
        TimeSettings.AMSign = 'am'
        TimeSettings.PMSign = 'pm'
        TimeSettings.MSign = 'm'
        EditLabel.Width = 52
        EditLabel.Height = 15
        EditLabel.Caption = 'Spiele DBV'
        Value = 0
      end
      object edPinsDBV: TLMDLabeledMaskEdit
        Left = 108
        Top = 83
        Width = 50
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 1
        OnChange = edPinsDBVChange
        OnExit = edSpieleDBVExit
        Alignment = taRightJustify
        CustomButtons = <>
        MaskType = meInteger
        TimeSettings.AMSign = 'am'
        TimeSettings.PMSign = 'pm'
        TimeSettings.MSign = 'm'
        EditLabel.Width = 43
        EditLabel.Height = 15
        EditLabel.Caption = 'Pins DBV'
        Value = 0
      end
      object edSchnittDBV: TLMDLabeledMaskEdit
        Left = 188
        Top = 83
        Width = 68
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 2
        TabStop = False
        OnChange = edPinsDBVChange
        OnExit = edSpieleDBVExit
        Validator = LMDFloatRangeValidator1
        Alignment = taRightJustify
        CustomButtons = <>
        Decimals = 2
        MaskType = meFloatFixed
        ReadOnly = True
        TimeSettings.AMSign = 'am'
        TimeSettings.PMSign = 'pm'
        TimeSettings.MSign = 'm'
        EditLabel.Width = 57
        EditLabel.Height = 15
        EditLabel.Caption = 'Schnitt DBV'
        Value = 0.000000000000000000
      end
      object edPinsZus: TLMDLabeledMaskEdit
        Left = 108
        Top = 123
        Width = 50
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 4
        Visible = False
        OnChange = edPinsZusChange
        OnExit = edSpieleDBVExit
        Alignment = taRightJustify
        CustomButtons = <>
        MaskType = meInteger
        TimeSettings.AMSign = 'am'
        TimeSettings.PMSign = 'pm'
        TimeSettings.MSign = 'm'
        EditLabel.Width = 44
        EditLabel.Height = 15
        EditLabel.Visible = False
        EditLabel.Caption = 'zus. Pins'
        Value = 0
      end
      object edSpieleZus: TLMDLabeledMaskEdit
        Left = 36
        Top = 123
        Width = 50
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 3
        Visible = False
        OnChange = edSpieleZusChange
        OnExit = edSpieleDBVExit
        Alignment = taRightJustify
        CustomButtons = <>
        MaskType = meInteger
        TimeSettings.AMSign = 'am'
        TimeSettings.PMSign = 'pm'
        TimeSettings.MSign = 'm'
        EditLabel.Width = 53
        EditLabel.Height = 15
        EditLabel.Visible = False
        EditLabel.Caption = 'zus. Spiele'
        Value = 0
      end
      object edSchnittZus: TLMDLabeledMaskEdit
        Left = 188
        Top = 123
        Width = 68
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 5
        TabStop = False
        Visible = False
        Validator = LMDFloatRangeValidator1
        Alignment = taRightJustify
        CustomButtons = <>
        Decimals = 2
        MaskType = meFloatFixed
        ReadOnly = True
        TimeSettings.AMSign = 'am'
        TimeSettings.PMSign = 'pm'
        TimeSettings.MSign = 'm'
        EditLabel.Width = 35
        EditLabel.Height = 15
        EditLabel.Visible = False
        EditLabel.Caption = 'Schnitt'
        Value = 0.000000000000000000
      end
      object edSchnittFBV: TLMDLabeledMaskEdit
        Left = 36
        Top = 176
        Width = 68
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 6
        OnChange = edSchnittFBVChange
        OnExit = edSpieleDBVExit
        Validator = LMDFloatRangeValidator1
        Alignment = taRightJustify
        CustomButtons = <>
        Decimals = 2
        MaskType = meFloatFixed
        TimeSettings.AMSign = 'am'
        TimeSettings.PMSign = 'pm'
        TimeSettings.MSign = 'm'
        EditLabel.Width = 57
        EditLabel.Height = 15
        EditLabel.Caption = 'FBV-Schnitt'
        Value = 0.000000000000000000
      end
      object edSchnittDBU: TLMDLabeledMaskEdit
        Left = 112
        Top = 176
        Width = 68
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 7
        OnChange = edSchnittDBUChange
        OnExit = edSpieleDBVExit
        Validator = LMDFloatRangeValidator1
        Alignment = taRightJustify
        CustomButtons = <>
        Decimals = 2
        MaskType = meFloatFixed
        TimeSettings.AMSign = 'am'
        TimeSettings.PMSign = 'pm'
        TimeSettings.MSign = 'm'
        EditLabel.Width = 59
        EditLabel.Height = 15
        EditLabel.Caption = 'DBU-Schnitt'
        Value = 0.000000000000000000
      end
      object edSchnittBSV: TLMDLabeledMaskEdit
        Left = 188
        Top = 176
        Width = 68
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 8
        OnChange = edSchnittBSVChange
        OnExit = edSpieleDBVExit
        Validator = LMDFloatRangeValidator1
        Alignment = taRightJustify
        CustomButtons = <>
        Decimals = 2
        MaskType = meFloatFixed
        TimeSettings.AMSign = 'am'
        TimeSettings.PMSign = 'pm'
        TimeSettings.MSign = 'm'
        EditLabel.Width = 57
        EditLabel.Height = 15
        EditLabel.Caption = 'BSV-Schnitt'
        Value = 0.000000000000000000
      end
      object edSchnittEff: TLMDLabeledMaskEdit
        Left = 283
        Top = 176
        Width = 68
        Height = 21
        Bevel.Mode = bmWindows
        Caret.BlinkRate = 530
        TabOrder = 9
        TabStop = False
        Validator = LMDFloatRangeValidator1
        Alignment = taRightJustify
        CustomButtons = <>
        Decimals = 2
        MaskType = meFloatFixed
        ReadOnly = True
        TimeSettings.AMSign = 'am'
        TimeSettings.PMSign = 'pm'
        TimeSettings.MSign = 'm'
        EditLabel.Width = 56
        EditLabel.Height = 15
        EditLabel.Caption = 'eff. Schnitt'
        Value = 0.000000000000000000
      end
    end
  end
  object LMDFloatRangeValidator1: TLMDFloatRangeValidator
    ValidateOnFocus = False
    ValidateOnLostFocus = True
    ErrorProvider = LMDErrorProvider1
    ErrorMessage = 'Ein Schnitt von mehr als 300 ist ja nun wirklich nicht m'#246'glich!!'
    HighLimit = 300.000000000000000000
    Left = 304
    Top = 88
  end
  object LMDErrorProvider1: TLMDErrorProvider
    UseMessageBox = True
    UseErrMsgControl = True
    UseIcon = True
    UseInPlaceIndication = True
    Left = 304
    Top = 56
  end
end
