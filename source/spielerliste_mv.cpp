//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "spielerliste_mv.h"
#include "datamodule.h"
#include "all.h"
#include "DBVRoutines.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "LMDButton"
#pragma link "LMDControl"
#pragma link "LMDCustomBevelPanel"
#pragma link "LMDCustomButton"
#pragma link "LMDCustomControl"
#pragma link "LMDCustomPanel"
#pragma link "LMDSimplePanel"
#pragma link "DbAGrids"
#pragma link "LMDBaseEdit"
#pragma link "LMDCustomEdit"
#pragma link "LMDCustomExtCombo"
#pragma link "LMDCustomListComboBox"
#pragma link "LMDCustomMaskEdit"

#pragma link "LMDListComboBox"
#pragma resource "*.dfm"
TfrmSpielerlisteMV *frmSpielerlisteMV;
//---------------------------------------------------------------------------
__fastcall TfrmSpielerlisteMV::TfrmSpielerlisteMV(TComponent* Owner)
	: TForm(Owner)
{
	FSortDir="DESC";
    FSortField="nachname";
}
//---------------------------------------------------------------------------

int __fastcall TfrmSpielerlisteMV::Search()
{
	if (ShowModal()==mrOk) {
    	return FNummer;
	}
	else {
		return 0;
    }
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpielerlisteMV::FormShow(TObject *Sender)
{
	FNummer=0;
	fillAnlageCombo(dmMain->conMV,cmbAnlage);
	cmbAnlage->ItemIndex=getAnlageComboIdx2(cmbAnlage,dmMain->qryTeams->FieldByName("anlage")->Value);
	qryMitgliederMV->Parameters->Items[0]->Value=dmMain->qryTeams->FieldByName("anlage")->Value;
	DBSort(qryMitgliederMV,DbAltGrid1->Columns->Items[0],FSortDir, FSortField,"Idx");

}
//---------------------------------------------------------------------------

void __fastcall TfrmSpielerlisteMV::FormHide(TObject *Sender)
{
	FNummer=qryMitgliederMV->FieldByName("mnummer")->AsInteger;
	qryMitgliederMV->Close();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpielerlisteMV::DbAltGrid1TitleClick(TColumn *Column)
{
	DBSort(qryMitgliederMV,Column,FSortDir, FSortField,"Idx");
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpielerlisteMV::btnCancelClick(TObject *Sender)
{
	ModalResult=mrCancel;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpielerlisteMV::btnOKClick(TObject *Sender)
{
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpielerlisteMV::cmbAnlageChange(TObject *Sender)
{
	qryMitgliederMV->Close();
	if (cmbAnlage->ItemIndex==-1) {
		qryMitgliederMV->Parameters->Items[0]->Value=0;
	}
	else {
		TDBVCenterObject *a=(TDBVCenterObject*)cmbAnlage->Items->Objects[cmbAnlage->ItemIndex];
		qryMitgliederMV->Parameters->Items[0]->Value=a->Idx;
	}
	qryMitgliederMV->Open();
}
//---------------------------------------------------------------------------

