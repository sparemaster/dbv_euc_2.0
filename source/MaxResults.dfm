object frmMaxResults: TfrmMaxResults
  Left = 0
  Top = 0
  Caption = 'H'#246'chste Spiele'
  ClientHeight = 532
  ClientWidth = 714
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LMDSimplePanel1: TLMDSimplePanel
    Left = 0
    Top = 0
    Width = 714
    Height = 532
    Hint = ''
    Align = alClient
    Bevel.Mode = bmCustom
    TabOrder = 0
    ExplicitLeft = 8
    ExplicitTop = 8
    ExplicitWidth = 457
    ExplicitHeight = 601
    object tbMax: TDbAltGrid
      Left = 0
      Top = 0
      Width = 714
      Height = 483
      Align = alClient
      AltColors.BandLine = 15790320
      AltColors.ColLine = 15790320
      AltColors.FixedLine = 14474460
      AltColors.RowLine = 15790320
      AltColors.Stripe1 = 16776176
      AltOptions = [dagAutoWidth, dagBandLines, dagDiscreteBandHeight, dagStripedBackground, dagStripedRows]
      FixedColor = clWhite
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Gruppe'
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MaxVR'
          Title.Alignment = taCenter
          Width = 40
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MaxF'
          Title.Alignment = taCenter
          Width = 40
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'MaxAll'
          Title.Alignment = taCenter
          Width = 40
        end
        item
          Expanded = False
          FieldName = 'Spielername'
          Width = 120
        end
        item
          Expanded = False
          FieldName = 'Teamname'
          Width = 150
        end>
    end
    object rgFilter: TLMDRadioGroup
      Left = 0
      Top = 483
      Width = 714
      Height = 49
      Hint = ''
      Align = alBottom
      Bevel.Mode = bmWindows
      BtnAlignment.Alignment = agCenterLeft
      BtnAlignment.OffsetX = 20
      BtnAlignment.Spacing = 10
      Caption = 'Filter'
      CaptionFont.Charset = DEFAULT_CHARSET
      CaptionFont.Color = clWindowText
      CaptionFont.Height = -11
      CaptionFont.Name = 'Tahoma'
      CaptionFont.Style = []
      Columns = 3
      Items.Strings = (
        'nur Damen'
        'nur Herren'
        'beide')
      TabOrder = 1
      OnChange = rgFilterChange
      ExplicitLeft = 16
      ExplicitTop = 488
      ExplicitWidth = 681
    end
  end
end
