//----------------------------------------------------------------------------
#ifndef report_teamlisteH
#define report_teamlisteH
//----------------------------------------------------------------------------
#include <ADODB.hpp>
#include <QRCtrls.hpp>
#include <QuickRpt.hpp>
#include <DB.hpp>
#include "DBVTypes.h"
#include "DBVEUCBaseComponent.h"
#include "DBVEUCTeam.h"
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
//----------------------------------------------------------------------------
class TfrmAuswertung_Team : public TForm
{
__published:
        TQuickRep *qrpAuswertung;
	TQRBand *DetailBand1;
	TQRBand *PageFooterBand1;
	TQRSysData *QRSysData2;
        TQRSysData *QRSysData3;
        TQRDBText *QRDBText1;
        TQRDBText *netto;
        TQRDBText *brutto;
        TQRBand *QRBand1;
        TQRSysData *lblCaption;
        TADOTable *tblTarget;
        TADOQuery *qryTarget;
        TQRLabel *lblSubcaption;
        TQRLabel *QRLabel5;
        TQRLabel *lblStand;
        TQRGroup *gruppenkopf;
        TQRBand *gruppenfuss;
        TQRBand *QRBand2;
        TQRLabel *QRLabel1;
        TQRLabel *QRLabel2;
        TQRLabel *QRLabel3;
        TQRLabel *QRLabel4;
        TQRLabel *lblTitel;
        TQRLabel *QRLabel6;
        TQRLabel *QRLabel7;
        TQRDBText *hdc_finale;
        TQRLabel *QRLabel8;
        TQRDBText *schnitt;
	TAutoIncField *tblTargetidx;
	TIntegerField *tblTargetteam;
	TIntegerField *tblTargetNetto;
	TIntegerField *tblTargetBrutto;
	TIntegerField *tblTargetBestDurchgang;
	TBooleanField *tblTargetQualifiziert;
	TFloatField *tblTargetSchnitt;
	TAutoIncField *qryTargetidx;
	TIntegerField *qryTargetteam;
	TIntegerField *qryTargetNetto;
	TIntegerField *qryTargetBrutto;
	TIntegerField *qryTargetBestDurchgang;
	TBooleanField *qryTargetQualifiziert;
	TFloatField *qryTargetSchnitt;
	TWideStringField *qryTargetName;
	TIntegerField *qryTargetNummer;
	TSmallintField *qryTargetHdc;
	TSmallintField *qryTargetHDC_Finale;
	TADOQuery *qryTeams;
	TDBVEUCTeam *eucTeam;
	TADOQuery *qryTarget2;
        void __fastcall DetailBand1BeforePrint(TQRCustomBand *Sender,
          bool &PrintBand);
        void __fastcall qrpAuswertungBeforePrint(TCustomQuickRep *Sender,
          bool &PrintReport);
	void __fastcall qryTeamsAfterScroll(TDataSet *DataSet);
private:
        int counter;
        bool hdc_visible;
		TDBVEUCItemType FItemType;
public:
	virtual __fastcall TfrmAuswertung_Team(TComponent* AOwner);
		void __fastcall PrepareData(TDBVEUCItemType itemtype,bool scratch);
        void __fastcall CloseData();
};
//----------------------------------------------------------------------------
extern PACKAGE TfrmAuswertung_Team *frmAuswertung_Team;
//----------------------------------------------------------------------------
#endif    
