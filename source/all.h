//---------------------------------------------------------------------------

#ifndef allH
#define allH
//---------------------------------------------------------------------------
#include "system.hpp"
#include <ADODB.hpp>
#include <DBGrids.hpp>
#include "LMDListComboBox.hpp"
#include "DBVTypes.h"

const AnsiString regstr="SOFTWARE\\DBV\\EUC_Ergebniserfassung";
const AnsiString regstr_mv="SOFTWARE\\DBV\\Mitgliederverwaltung";

void __fastcall fillAnlageCombo(TADOConnection *con,TLMDLabeledListComboBox *combo);
int __fastcall getAnlageComboIdx2(TLMDLabeledListComboBox *combo, int anlagennummer);
void __fastcall getMVMemberData(TADOConnection *con, TDBVEUCPlayerData &data);

//---------------------------------------------------------------------------
#endif
