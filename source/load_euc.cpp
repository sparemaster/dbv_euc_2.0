//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "load_euc.h"
#include "datamodule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "LMDButton"
#pragma link "LMDControl"
#pragma link "LMDCustomBevelPanel"
#pragma link "LMDCustomButton"
#pragma link "LMDCustomControl"
#pragma link "LMDCustomPanel"
#pragma link "LMDSimplePanel"
#pragma link "LMDBaseEdit"
#pragma link "LMDCustomEdit"
#pragma link "LMDCustomExtCombo"
#pragma link "LMDCustomListComboBox"
#pragma link "LMDCustomMaskEdit"
#pragma link "LMDListComboBox"
#pragma resource "*.dfm"
TfrmLoadEUC *frmLoadEUC;
//---------------------------------------------------------------------------
__fastcall TfrmLoadEUC::TfrmLoadEUC(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmLoadEUC::FormShow(TObject *Sender)
{
	SelectedIdx=0;
	cmbEUC->ItemIndex=-1;
	btnOK->Enabled=false;

	dmMain->tblEUC->Open();
	cmbEUC->Items->Clear();
	while (!dmMain->tblEUC->Eof) {
		cmbEUC->Items->AddObject(dmMain->tblEUC->FieldByName("Name1")->AsString,
								 (TObject*)(dmMain->tblEUC->FieldByName("TurnierIdx")->AsInteger));
		dmMain->tblEUC->Next();
	}
	dmMain->tblEUC->Close();
}
//---------------------------------------------------------------------------

void __fastcall TfrmLoadEUC::btnCancelClick(TObject *Sender)
{
	ModalResult=mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TfrmLoadEUC::btnOKClick(TObject *Sender)
{
	SelectedIdx=(int)(cmbEUC->Items->Objects[cmbEUC->ItemIndex]);
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TfrmLoadEUC::cmbEUCChange(TObject *Sender)
{
	btnOK->Enabled=cmbEUC->ItemIndex!=-1;
}
//---------------------------------------------------------------------------

