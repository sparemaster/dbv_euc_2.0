//----------------------------------------------------------------------------
#ifndef schnittlisteH
#define schnittlisteH
//----------------------------------------------------------------------------
#include <QRCtrls.hpp>
#include <QuickRpt.hpp>
#include <DB.hpp>
#include <ExtCtrls.hpp>
#include "LMDCustomComponent.hpp"
#include "LMDIniCtrl.hpp"
#include <ADODB.hpp>
#include "QRPDFFilt.hpp"
#include "DBVTypes.h"
#include "DBVEUCBaseComponent.h"
#include "DBVEUCTeam.h"
#include "QRWebFilt.hpp"
#include <Classes.hpp>
#include <Controls.hpp>
//----------------------------------------------------------------------------
class TqrpSchnittliste : public TForm
{
__published:
        TLMDIniCtrl *iniControl;
	TQRPDFFilter *QRPDFFilter1;
	TADOQuery *qryData;
	TIntegerField *qryDataTurnier;
	TWideStringField *qryDataTeamname;
	TWideStringField *qryDataNachname;
	TWideStringField *qryDataVorname;
	TWideStringField *qryDataGruppe;
	TIntegerField *qryDataPins;
	TIntegerField *qryDataSpielezahl;
	TFloatField *qryDataSchnitt;
	TIntegerField *qryDataMax;
	TStringField *qryDataSpielername;
	TQRHTMLFilter *QRHTMLFilter1;
	TQuickRep *Report;
	TQRBand *QRBand1;
	TQRSysData *QRSysData1;
	TQRLabel *lblTitel;
	TQRLabel *lblStand;
	TQRLabel *lblSubcaption;
	TQRBand *QRBand4;
	TQRLabel *QRLabel11;
	TQRSysData *QRSysData3;
	TQRLabel *QRLabel12;
	TQRBand *QRBand2;
	TQRLabel *QRLabel1;
	TQRLabel *QRLabel2;
	TQRLabel *QRLabel3;
	TQRLabel *QRLabel4;
	TQRLabel *QRLabel5;
	TQRLabel *QRLabel6;
	TQRLabel *QRLabel7;
	TQRLabel *QRLabel8;
	TQRLabel *QRLabel9;
	TQRLabel *QRLabel10;
	TQRBand *QRBand3;
	TQRDBText *QRDBText1;
	TQRDBText *QRDBText2;
	TQRDBText *QRDBText3;
	TQRDBText *QRDBText4;
	TQRDBText *QRDBText5;
	TQRDBText *QRDBText6;
	TQRSysData *QRSysData2;
        void __fastcall QRBand3BeforePrint(TQRCustomBand *Sender,
          bool &PrintBand);
	void __fastcall qryDataCalcFields(TDataSet *DataSet);
private:
   bool sort_games;
   bool show_empty_row;
   int max_games;
//   int game_count;
public:
   __fastcall TqrpSchnittliste::TqrpSchnittliste(TComponent* Owner);
   void __fastcall PrepareData(TDBVEUCItemType itemtype,TDBVReportType reporttype);
   void __fastcall CloseData();
};
//----------------------------------------------------------------------------
extern TqrpSchnittliste *qrpSchnittliste;
//----------------------------------------------------------------------------
#endif