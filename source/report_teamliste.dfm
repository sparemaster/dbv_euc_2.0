�
 TFRMAUSWERTUNG_TEAM 0-  TPF0TfrmAuswertung_TeamfrmAuswertung_TeamLeft�Top� CaptionfrmAuswertungClientHeightClientWidth5Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledPixelsPerInch`
TextHeight 	TQuickRepqrpAuswertungLeftTopWidthHeightcFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	BeforePrintqrpAuswertungBeforePrintDataSet	qryTargetFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinFirstPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsPixelsZoomdPrevFormStylefsStayOnTopPreviewInitialStatewsMaximizedPrevShowThumbsPrevShowSearchPrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestPDFPreviewLeft 
PreviewTop  TQRBandDetailBand1LeftLTop� Width�HeightAlignToBottomBeforePrintDetailBand1BeforePrintColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values       �@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail 
TQRSysData
QRSysData3LeftTopWidthHeightSize.Values ������@ �������@       �@ �����
�@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataqrsDetailNoText    TransparentExportAsexptTextFontSize
  	TQRDBText	QRDBText1Left:TopWidth#HeightSize.Values�������@UUUUUUu�@UUUUUUU�@UUUUUU5�@ XLColumn 	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataSet	qryTarget	DataFieldNameTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  	TQRDBTextnettoLeft�TopWidth9HeightSize.Valuesk������@      �	@RFUUUUU�@      Ж@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataSet	qryTarget	DataFieldNettoTransparentWordWrapExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  	TQRDBTextbruttoLeftTopWidth=HeightSize.Valuesk������@      ��	@RFUUUUU�@�KUUUUe�@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataSet	qryTarget	DataFieldBruttoTransparentWordWrapExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  	TQRDBText
hdc_finaleLeft$TopWidthHHeightSize.Values2Ъ����@��UUUU=�	@7�TUUUU�@      ��@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataSet	qryTarget	DataField
HDC_FinaleTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  	TQRDBTextschnittLeft�TopWidthLHeightSize.Values2Ъ����@c5�����	@       �@��TUUU�@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataSet	qryTarget	DataFieldSchnittTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
   TQRBandPageFooterBand1LeftLTopWidth�HeightFAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU5�@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageFooter 
TQRSysData
QRSysData2LeftTop3WidthPHeightSize.Values�������@UUUUUUU�@      ��@��������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataqrsPageNumberTextSeite TransparentExportAsexptTextFontSize
  TQRLabelQRLabel5Left�Top3Width� HeightSize.Values�������@      ʙ	@      ��@������z�@ XLColumn 	AlignmenttaRightJustifyAlignToBand	Caption(c) 2003-2013 Michael Kieser  ColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel6Left=TopWidthHeightSize.Values�������@UUUUUUe�@UUUUUU��@��������	@ XLColumn 	AlignmenttaCenterAlignToBand	CaptionPErgebnisse gibt's hier: www.dbv-bowling.de - alle Auswertungen zum herunterladenColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
   TQRBandQRBand1LeftLTop9Width�Height� AlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUUU�@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader 
TQRSysData
lblCaptionLeft� TopWidth� Height&Size.ValuesUUUUUU�@      Ț@UUUUUUU�@��������@ XLColumn 	AlignmenttaCenterAlignToBand	ColorclWhiteDataqrsReportTitleFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFontText    TransparentExportAsexptTextFontSize  TQRLabellblSubcaptionLeftTop*WidthkHeightSize.Values��������@UUUUUUE�@      @�@UUUUUU��@ XLColumn 	AlignmenttaCenterAlignToBand	CaptionlblSubcaptionColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabellblStandLeft"Top`Width?HeightSize.Values������j�@������ҿ@       �@      ��@ XLColumn 	AlignmenttaCenterAlignToBand	CaptionlblStandColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabellblTitelLeft(TopFWidth3HeightSize.Values������j�@��������@UUUUUU5�@      ��@ XLColumn 	AlignmenttaCenterAlignToBand	CaptionlblTitelColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize   TQRGroupgruppenkopfLeftLTop� Width�HeightAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageLinkBandDetailBand1Size.ValuesUUUUUUU�@��������	@ PreCaluculateBandHeightKeepOnOnePage
ExpressionqryTarget.Qualifiziert
FooterBandgruppenfussMasterqrpAuswertungReprintOnNewPage	  TQRBandgruppenfussLeftLTop� Width�HeightAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values       �@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbGroupFooter  TQRBandQRBand2LeftLTop� Width�HeightAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values������j�@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbColumnHeader TQRLabelQRLabel1LeftTopWidth!HeightSize.Values�������@      ��@       �@      ��@ XLColumn 	AlignmenttaLeftJustifyAlignToBandCaptionPlatzColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel2Left9TopWidth%HeightSize.Values�������@      Ж@       �@��������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandCaptionTeamColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel3LeftDTopWidth8HeightSize.Values�������@      P�@       �@������*�@ XLColumn 	AlignmenttaLeftJustifyAlignToBandCaptionRealpinsColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel4Left�TopWidthSHeightSize.Values�������@      ��@       �@��������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandCaptionPins mit HDCColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel7Left$TopWidthHHeightSize.Values�������@UUUUUU=�	@UUUUUUU�@      ��@ XLColumn 	AlignmenttaLeftJustifyAlignToBandCaption
HDC FinaleColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel8Left�TopWidthNHeightSize.Values�������@      ��	@       �@      `�@ XLColumn 	AlignmenttaLeftJustifyAlignToBandCaptionReal-SchnittColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
    	TADOTable	tblTarget
ConnectiondmMain.conMain
CursorTypectStatic	TableNameauswertung_teamLeftTop TAutoIncFieldtblTargetidx	FieldNameidxReadOnly	  TIntegerFieldtblTargetteam	FieldNameteam  TIntegerFieldtblTargetNetto	FieldNameNetto  TIntegerFieldtblTargetBrutto	FieldNameBrutto  TIntegerFieldtblTargetBestDurchgang	FieldNameBestDurchgang  TBooleanFieldtblTargetQualifiziert	FieldNameQualifiziert  TFloatFieldtblTargetSchnitt	FieldNameSchnitt   	TADOQuery	qryTarget
ConnectiondmMain.conMain
CursorTypectStatic
Parameters SQL.Strings3select a.*,t.Name,t.Nummer,t.Hdc,t.HDC_Finale from auswertung_team a,teams twhere (a.Team=t.TeamIdx)8order by a.Brutto DESC,a.Netto DESC,a.BestDurchgang DESC Left!TopE TAutoIncFieldqryTargetidx	FieldNameidxReadOnly	  TIntegerFieldqryTargetteam	FieldNameteam  TIntegerFieldqryTargetNetto	FieldNameNettoDisplayFormat#,##0  TIntegerFieldqryTargetBrutto	FieldNameBruttoDisplayFormat#,##0  TIntegerFieldqryTargetBestDurchgang	FieldNameBestDurchgang  TBooleanFieldqryTargetQualifiziert	FieldNameQualifiziert  TFloatFieldqryTargetSchnitt	FieldNameSchnittDisplayFormat##0.00  TWideStringFieldqryTargetName	FieldNameNameSize(  TIntegerFieldqryTargetNummer	FieldNameNummer  TSmallintFieldqryTargetHdc	FieldNameHdc  TSmallintFieldqryTargetHDC_Finale	FieldName
HDC_Finale   	TADOQueryqryTeams
ConnectiondmMain.conMainAfterScrollqryTeamsAfterScroll
ParametersNameIdx
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings&select * from teams where turnier=:Idx LeftPTop  TDBVEUCTeameucTeam
ConnectiondmMain.conMainIdxFieldTeamIdxTargetetEUCLeftpTop  	TADOQuery
qryTarget2
ConnectiondmMain.conMain
Parameters SQL.Stringsselect * from auswertung_teamorder by brutto DESC Left� Top   