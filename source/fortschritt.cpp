//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "fortschritt.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "LMDBaseControl"
#pragma link "LMDBaseGraphicControl"
#pragma link "LMDBaseMeter"
#pragma link "LMDCustomProgress"
#pragma link "LMDGraphicControl"
#pragma link "LMDProgress"
#pragma link "LMDCustomComponent"
#pragma link "LMDCustomFormFill"
#pragma link "LMDFormFill"
#pragma link "LMDWndProcComponent"
#pragma resource "*.dfm"
TfrmFortschritt *frmFortschritt;
//---------------------------------------------------------------------------
__fastcall TfrmFortschritt::TfrmFortschritt(TComponent* Owner)
        : TForm(Owner)
{

}
//---------------------------------------------------------------------------

void __fastcall TfrmFortschritt::Start(int max, UnicodeString message)
{
  Balken->UserValue=0;
  Balken->MaxValue=max;
  Label1->Caption=message;
  Show();
}
//---------------------------------------------------------------------------

void __fastcall TfrmFortschritt::Step(int size)
{
  Balken->UserValue+=size;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------

void __fastcall TfrmFortschritt::Stop()
{
  Balken->UserValue=0;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------

void __fastcall TfrmFortschritt::Set(int position)
{
	Balken->UserValue=position;
  Application->ProcessMessages();
}
//---------------------------------------------------------------------------



void __fastcall TfrmFortschritt::FormShow(TObject *Sender)
{
//  Left=frmMain->Left+(frmMain->Width-Width)/2;
//  Top=frmMain->Top+(frmMain->Height)/2;
}
//---------------------------------------------------------------------------

