�
 TFRMREPORTSTARTLISTE 0�+  TPF0TfrmReportStartlistefrmReportStartlisteLeftTop� CaptionfrmReportStartlisteClientHeightCClientWidth(Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledPixelsPerInch`
TextHeight 	TQuickRepqrpStartlisteLeftTopWidthHeightcFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetqryTeamsFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinFirstPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsStayOnTopPreviewInitialStatewsMaximizedPrevShowThumbsPrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestPDFPreviewLeft 
PreviewTop  TQRBandQRBand1LeftLTop9Width�Height� Frame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColor��� TransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUUU�@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader 
TQRSysData
QRSysData1Left� TopWidth� Height&Frame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU�@      Ț@UUUUUUU�@��������@ XLColumn 	AlignmenttaCenterAlignToBand	AutoSize	Color��� DataqrsReportTitleFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptTextFontSize  TQRLabellblSubcaptionLeftTop*WidthkHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@UUUUUUE�@      @�@UUUUUU��@ XLColumn 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionlblSubcaptionColor��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabellblStandLeft"Top`Width?HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������j�@������ҿ@       �@      ��@ XLColumn 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionlblStandColor��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabellblTitelLeftTopFWidthDHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������j�@������ֽ@UUUUUU5�@�������@ XLColumn 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaption
StartlisteColor��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize   TQRBand
detailbandLeftLTopWidth�HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColor��� TransparentBandForceNewColumnForceNewPageSize.ValuesVUUUUU��@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail 	TQRDBText	QRDBText1LeftTopWidth+HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@��������@UUUUUUU�@��������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColor��� DataSetqryTeams	DataFieldNameFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize  	TQRDBText	QRDBText4LeftTopWidthEHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU�@ª���ֽ@[sUUUUU�@      ��@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColor��� DataSetqryTeams	DataFieldNummer2Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize   TQRBandPageFooterBand1LeftLTopHWidth�HeightFFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColor��� TransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU5�@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageFooter 
TQRSysData
QRSysData2LeftTop2WidthPHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@������J�@��������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	Color��� DataqrsPageNumberTextSeite TransparentExportAsexptTextFontSize
  TQRLabelQRLabel5Left�Top2Width� HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ʙ	@������J�@������z�@ XLColumn 	AlignmenttaRightJustifyAlignToBand	AutoSize	AutoStretchCaption(c) 2003-2012 Michael Kieser  Color��� TransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel6Left=TopWidthHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUe�@UUUUUU��@��������	@ XLColumn 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionPErgebnisse gibt's hier: www.dbv-bowling.de - alle Auswertungen zum herunterladenColor��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
   TQRGroupgruppenkopfLeftLTop� Width�Height\Frame.DrawTopFrame.DrawBottom	Frame.DrawLeftFrame.DrawRightAlignToBottomColor��� TransparentBandForceNewColumnForceNewPageLinkBand
detailbandSize.Values������j�@��������	@ PreCaluculateBandHeightKeepOnOnePage
ExpressionqryTeams.Gruppe
FooterBandgruppenfussMasterqrpStartlisteReprintOnNewPage	 	TQRDBText	QRDBText2LeftTop$WidthKHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ���j�@2Ъ�����@      ��@      p�@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColor��� DataSetqryTeams	DataFieldGruppeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize  	TQRDBText	QRDBText3LeftwTop$WidthMHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������j�@UUUUUUm�@      ��@��������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColor��� DataSetqryTeams	DataField	StartzeitFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize  TQRLabelQRLabel1LeftTopBWidth4HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values       �@��������@      ��@UUUUUU��@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTeamColor��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabelQRLabel2Left TopBWidthCHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values       �@      ��@      ��@��TUUUE�@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaption(Nr.)Color��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabelQRLabel3Left�Top%WidthLHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@      ��	@��������@UUUUUU�@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
Meldezeit:Color��� Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize  	TQRDBText	QRDBText5Left�Top%Width� HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values �������@ ������	@ �������@ �������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColor��� DataSetqryTeams	DataField	MeldezeitFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize   TQRBandgruppenfussLeftLTop+Width�HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColor��� TransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUUu�@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbGroupFooter   	TADOQueryqryTeams
ConnectiondmMain.conMain
CursorTypectStaticOnCalcFieldsqryTeamsCalcFields
ParametersNameIdx
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value19  SQL.Strings;SELECT t.Name, s.Startzeit, s.Gruppe, t.Nummer, s.Meldezeit?FROM startgruppe AS s INNER JOIN teams AS t ON s.Idx = t.GruppeWHERE (((t.Turnier)=:Idx))ORDER BY s.Gruppe, t.Name     LeftTop  TStringFieldqryTeamsName	FieldNameNameOriginDBV."team.DB".NameSize  TDateTimeFieldqryTeamsStartzeit	FieldName	StartzeitOriginDBV."startgruppe.DB".Startzeit  TStringFieldqryTeamsGruppe	FieldNameGruppeOriginDBV."startgruppe.DB".GruppeSize  TIntegerFieldqryTeamsNummer	FieldNameNummerOriginDBV."team.DB".Nummer  TStringFieldqryTeamsNummer2	FieldKindfkCalculated	FieldNameNummer2Size
Calculated	  TDateTimeFieldqryTeamsMeldezeit	FieldName	Meldezeit   TQRPDFFilterQRPDFFilter1CompressionOnTextEncodingASCIIEncodingCodepage1252Left� Top   