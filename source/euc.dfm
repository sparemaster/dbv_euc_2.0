object frmEUC: TfrmEUC
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = 'EUC bearbeiten'
  ClientHeight = 296
  ClientWidth = 469
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LMDSimplePanel1: TLMDSimplePanel
    Left = 0
    Top = 248
    Width = 469
    Height = 48
    Align = alBottom
    Bevel.StyleInner = bvRaised
    Bevel.StyleOuter = bvFrameLowered
    Bevel.Mode = bmCustom
    TabOrder = 0
    ExplicitTop = 246
    ExplicitWidth = 467
    object btnOK: TLMDButton
      Left = 10
      Top = 13
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      TabOrder = 0
      OnClick = btnOKClick
      ButtonStyle = ubsWin40Ext
    end
    object btnCancel: TLMDButton
      Left = 91
      Top = 13
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Abbrechen'
      TabOrder = 1
      OnClick = btnCancelClick
      ButtonStyle = ubsWin40Ext
    end
  end
  object edName1: TLMDLabeledEdit
    Left = 10
    Top = 32
    Width = 295
    Height = 21
    Bevel.Mode = bmWindows
    Caret.BlinkRate = 530
    TabOrder = 1
    CustomButtons = <>
    PasswordChar = #0
    EditLabel.Width = 35
    EditLabel.Height = 15
    EditLabel.Caption = 'Name1'
  end
  object edName2: TLMDLabeledEdit
    Left = 8
    Top = 80
    Width = 295
    Height = 21
    Bevel.Mode = bmWindows
    Caret.BlinkRate = 530
    TabOrder = 2
    CustomButtons = <>
    PasswordChar = #0
    EditLabel.Width = 35
    EditLabel.Height = 15
    EditLabel.Caption = 'Name2'
  end
end
