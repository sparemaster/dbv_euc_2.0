//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "main.h"
#include "datamodule.h"
#include "load_euc.h"
#include "DBVTypes.h"
#include "DBVRoutines.h"
#include "result.h"
#include "fortschritt.h"
#include "team.h"
#include "euc.h"
#include "schnittliste.h"
#include "spielzettel.h"
#include "report_teamliste.h"
#include "report_teamliste_breitensport.h"
#include "report_startliste.h"
#include "startzeiten.h"
#include "MaxResults.h"
#include "variance.h"
#include "report_max.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DbAGrids"
#pragma link "DBVBaseComponent"
#pragma link "DBVEUCPrepare"
#pragma link "LMDActnList"
#pragma link "LMDCustomComponent"
#pragma link "LMDFormVista"
#pragma link "LMDWndProcComponent"
#pragma link "LMDBaseControl"
#pragma link "LMDBaseGraphicControl"
#pragma link "LMDBaseLabel"
#pragma link "LMDControl"
#pragma link "LMDCustomBevelPanel"
#pragma link "LMDCustomControl"
#pragma link "LMDCustomGroupBox"
#pragma link "LMDCustomPanel"
#pragma link "LMDCustomParentPanel"
#pragma link "LMDCustomSimpleLabel"
#pragma link "LMDCustomStatusBar"
#pragma link "LMDGroupBox"
#pragma link "LMDInformationLabel"
#pragma link "LMDSimpleLabel"
#pragma link "LMDSimplePanel"
#pragma link "LMDStatusBar"
#pragma link "LMDBaseGraphicButton"
#pragma link "LMDCustomSpeedButton"
#pragma link "LMDSpeedButton"
#pragma link "LMDApplicationCtrl"
#pragma link "dagTmplt"
#pragma link "LMDPopupMenu"
#pragma link "LMDSectionBar"
#pragma link "DBVResultFile"
#pragma link "LMDIniCtrl"

#pragma link "LMDBaseEdit"
#pragma link "LMDCustomButton"
#pragma link "LMDCustomEdit"
#pragma link "LMDDockButton"
#pragma link "LMDEdit"
#pragma resource "*.dfm"
TfrmMain *frmMain;
//---------------------------------------------------------------------------
__fastcall TfrmMain::TfrmMain(TComponent* Owner)
	: TForm(Owner)
{
	FSortDir="DESC";
	FSortField="Nummer";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormShow(TObject *Sender)
{
	int idx=iniCtrl->ReadInteger("Common","LastEUC",0);
	int team=iniCtrl->ReadInteger("Common","LastTeam",0);
	dmMain->eucEUC->IdxField="TurnierIdx";
	bool found=dmMain->selectEUC(idx,true);
	if (found) {
		DBSort(dmMain->qryTeams,tbOverview->Columns->Items[1],FSortDir,FSortField,"TeamIdx");
		lblTitle->Caption=dmMain->eucEUC->Name1;
		lblTitle2->Caption=dmMain->eucEUC->Name2;
		if (team>0) {
			found=dmMain->qryTeams->Locate("TeamIdx",team,TLocateOptions());
			if (found) {
				dmMain->eucTeam->readFromDB(team);
			}
		}
		enableMenu(true);
	}
	else {
		enableMenu(false);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::showFooterValues()
{
	((TdagColumn*)(tbOverview->Columns->Items[4]))->Footer->Text=dmMain->qryStartCount->FieldByName("VR1")->AsString;
	((TdagColumn*)(tbOverview->Columns->Items[5]))->Footer->Text=dmMain->qryStartCount->FieldByName("VR2")->AsString;
	((TdagColumn*)(tbOverview->Columns->Items[8]))->Footer->Text=dmMain->qryStartCount->FieldByName("F")->AsString;
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::enableMenu(bool enabled)
{
	acExport->Enabled=enabled;

	TLMDSectionBarSection *sec=(TLMDSectionBarSection*)sebMain->Sections->Items[1];
	sec->ReadOnly=enabled;
	sec=(TLMDSectionBarSection*)sebMain->Sections->Items[2];
	sec->ReadOnly=enabled;

	acReload->Enabled=enabled;
	acEUCEdit->Enabled=enabled;
	acExportResults->Enabled=enabled;
	acStartTime->Enabled=enabled;

	acTeamEdit->Enabled=enabled;
	acTeamResultEdit->Enabled=enabled;
	acTeamResultDelete->Enabled=enabled;
	acTeamNotePrint->Enabled=enabled;

	acReportAverageMale->Enabled=enabled;
	acReportAverageAll->Enabled=enabled;
	acReportAverageFemale->Enabled=enabled;
	acReportTeamListVR1->Enabled=enabled;
	acReportTeamListVR2->Enabled=enabled;
	acReportTeamListVR2int->Enabled=enabled;
	acReportTeamListF->Enabled=enabled;
	acReportTeamListFScratch->Enabled=enabled;
	acReportStart->Enabled=enabled;

}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::tbOverviewTitleClick(TColumn *Column)
{
	if (Column->Index<3) {
		DBSort(dmMain->qryTeams,Column,FSortDir,FSortField,"TeamIdx");
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::dbvPrepareStartReadWrite(TObject *Sender,
	  TDBVFileStatus FileStatus, int Count)
{
	UnicodeString msg;
	if (FileStatus==fsRead) {
		msg="lade Daten...";
	}
	else {
		msg="schreiben";
	}
	frmFortschritt->Start(Count,msg);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::dbvPrepareProgress(TObject *Sender,
	  TDBVFileStatus FileStatus, int Max, int Progress)
{
	frmFortschritt->Step(1);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::dbvPrepareSuccess(TObject *Sender,
	  TDBVFileStatus FileStatus, TDBVFileType FileType)
{
	UnicodeString msg;
	if (FileStatus==fsRead) {
		msg="Daten erfolgreich importiert.";
	}
	else {
		msg="Daten erfolgreich exportiert.";
	}
	frmFortschritt->Close();
	MessageDlg(msg,mtInformation,TMsgDlgButtons()<<mbOK,0);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acEUCLoadExecute(TObject *Sender)
{
	if (frmLoadEUC->ShowModal()==mrOk) {
		dmMain->selectEUC(frmLoadEUC->SelectedIdx, true);
		lblTitle->Caption=dmMain->eucEUC->Name1;
		lblTitle2->Caption=dmMain->eucEUC->Name2;
		enableMenu(true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acCloseExecute(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReloadExecute(TObject *Sender)
{
	dmMain->qryTeams->DisableControls();
	try {
		int idx=dmMain->eucTeam->TeamIdx;
		dmMain->selectEUC(dmMain->eucEUC->Idx,true);
		dmMain->qryTeams->Locate("TeamIdx",idx,TLocateOptions());
	}
	__finally {
		dmMain->qryTeams->EnableControls();
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acImportExecute(TObject *Sender)
{
	if (odOpen->Execute()) {
		FImportTeam=false;
		dbvPrepare->FileName=odOpen->FileName;
		dbvPrepare->Connection=dmMain->conMain;
		int idx=dbvPrepare->readPreparationFile();
		dmMain->selectEUC(idx, true);
		acExport->Enabled=true;
		acTeamEdit->Enabled=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acExportExecute(TObject *Sender)
{
	if (sdSave->Execute()) {
		dbvPrepare->FileName=sdSave->FileName;
		dbvPrepare->Connection=dmMain->conMain;
		dbvPrepare->writePreparationFile(dmMain->eucTeam->TurnierIdx);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acTeamResultEditExecute(TObject *Sender)
{
	frmResult->show(dmMain->eucTeam->TeamIdx);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acTeamEditExecute(TObject *Sender)
{
	if (frmTeam->ShowModal()==mrOk) {
    	acReload->Execute();
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acTeamResultDeleteExecute(TObject *Sender)
{
	UnicodeString msg="Sollen alle Ergebnisse des Teams \""+dmMain->eucTeam->Teamname+"\" gel�scht werden?";
	TModalResult ret=MessageDlg(msg,mtConfirmation,TMsgDlgButtons()<<mbYes<<mbNo,0);
	if (ret==mrYes) {
		dmMain->eucTeam->clearResultList();
		dmMain->eucTeam->writeToDB(dmMain->eucTeam->TeamIdx);
		acReload->Execute();
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acTeamNotePrintExecute(TObject *Sender)
{
    dmMain->eucTeam->Startgruppe;
	qrpSpielzettel->PrepareData(0,dmMain->eucTeam->TeamIdx);
	qrpSpielzettel->Report->PreviewModal();
	qrpSpielzettel->CloseData();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acEUCEditExecute(TObject *Sender)
{
	frmEUC->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReportAverageMaleExecute(TObject *Sender)
{
//	qrpSchnittliste->QRLabel11->Caption="(c) 2003-2012 Michael Kieser  ";
	qrpSchnittliste->PrepareData(dmMain->getCurrentRound(),rtMale);
	qrpSchnittliste->Report->PreviewModal();
	qrpSchnittliste->CloseData();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReportAverageFemaleExecute(TObject *Sender)
{
	qrpSchnittliste->PrepareData(dmMain->getCurrentRound(),rtFemale);
	qrpSchnittliste->Report->PreviewModal();
	qrpSchnittliste->CloseData();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReportAverageAllExecute(TObject *Sender)
{
	qrpSchnittliste->PrepareData(dmMain->getCurrentRound(),rtAll);
	qrpSchnittliste->Report->PreviewModal();
	qrpSchnittliste->CloseData();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReportTeamListVR1Execute(TObject *Sender)
{
	frmAuswertung_Team->PrepareData(itAuswertungVR1,false);
	frmAuswertung_Team->qrpAuswertung->PreviewModal();
	frmAuswertung_Team->CloseData();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReportTeamListVR2Execute(TObject *Sender)
{
	frmAuswertung_Team->PrepareData(itAuswertungVR2,false);
	frmAuswertung_Team->qrpAuswertung->PreviewModal();
	frmAuswertung_Team->CloseData();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReportTeamListVR2intExecute(TObject *Sender)
{
	frmAuswertung_Team->PrepareData(itAuswertungVR2int,true);
	frmAuswertung_Team->qrpAuswertung->PreviewModal();
	frmAuswertung_Team->CloseData();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReportTeamListFExecute(TObject *Sender)
{
	frmAuswertung_Team->PrepareData(itAuswertungF,false);
	frmAuswertung_Team->qrpAuswertung->PreviewModal();
	frmAuswertung_Team->CloseData();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReportTeamListFScratchExecute(TObject *Sender)
{
	frmAuswertung_Team_Breitensport->PrepareData(false);
	frmAuswertung_Team_Breitensport->qrpAuswertung->PreviewModal();
	frmAuswertung_Team_Breitensport->CloseData();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acExportResultsExecute(TObject *Sender)
{
	if (sdResult->Execute()) {
		dbvResultFile->Anlage=302;
		dbvResultFile->LigaName=dmMain->eucEUC->Name1;
		dbvResultFile->SourceIdx=dmMain->eucEUC->Idx;
		dbvResultFile->FileType=ftEUCResult;
		dbvResultFile->FileName=sdResult->FileName;
		dbvResultFile->WriteToFile(dmMain->getProgID(),Sysutils::StringToGUID(emptyGUID/*dmMain->eucEUC->EventGUID*/));
		frmFortschritt->Close();
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::dbvResultFileSuccess(TObject *Sender,
      TDBVFileStatus FileStatus, TDBVFileType FileType)
{
	MessageDlg("Ergebnisse erfolgreich exportiert.",mtInformation,TMsgDlgButtons()<<mbOK,0);	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReportStartExecute(TObject *Sender)
{
	frmReportStartliste->PrepareData();
	frmReportStartliste->qrpStartliste->PreviewModal();
	frmReportStartliste->CloseData();	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acStartTimeExecute(TObject *Sender)
{
	frmStartTime->ShowModal();	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::dbvPrepareItemExists(TObject *Sender, UnicodeString Name,
	  int Idx, bool &CanOverwrite, bool &Cancel)
{
	TModalResult ret=MessageDlg("Ein EUC mit dem Namen \""+Name+"\" existiert bereits. Soll es �berschrieben werden?",mtConfirmation,TMsgDlgButtons()<<mbYes<<mbNo<<mbCancel,0);
	switch (ret) {
		case mrYes:		CanOverwrite=true;break;
		case mrNo:		CanOverwrite=false;break;
		case mrCancel:	Cancel=true;frmFortschritt->Close();break;
	default:
		;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::FormClose(TObject *Sender, TCloseAction &Action)
{
	iniCtrl->WriteInteger("Common","LastEUC",dmMain->eucEUC->Idx);
	iniCtrl->WriteInteger("Common","LastTeam",dmMain->eucTeam->TeamIdx);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acMaxResultsExecute(TObject *Sender)
{
	frmMaxResults->ShowModal();	
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acFinalReportsExecute(TObject *Sender)
{
	frmAuswertung_Team->PrepareData(itAuswertungF,false);
	frmAuswertung_Team_Breitensport->PrepareData(true);
	QRCompositeReport1->Preview();
	frmAuswertung_Team->CloseData();
	frmAuswertung_Team_Breitensport->CloseData();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::QRCompositeReport1AddReports(TObject *Sender)
{
	QRCompositeReport1->Reports->Add(frmAuswertung_Team->qrpAuswertung);
	QRCompositeReport1->Reports->Add(frmAuswertung_Team_Breitensport->qrpAuswertung);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acTeamImportExecute(TObject *Sender)
{
	if (odOpen->Execute()) {
		FImportTeam=true;
		dbvPrepare->FileName=odOpen->FileName;
		dbvPrepare->Connection=dmMain->conMain;
		int idx=dbvPrepare->readPreparationFile();
		acReload->Execute();
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::dbvPrepareReadHeader(TObject *Sender,
      TDBVFileHeader FileHeader, bool &Abort)
{
	if (FImportTeam && (FileHeader.type!=ftEUCPrepareTeam)) {
		MessageDlg("Die angegebene Datei ist keine g�ltige Datei f�r das Einlesen eines einzelnen Teams!",mtError, TMsgDlgButtons()<<mbOK,0);
		Abort=true;
	}
	if (!FImportTeam && (FileHeader.type!=ftEUCPrepare)) {
		MessageDlg("Die angegebene Datei ist keine g�ltige Datei f�r das Einlesen eines gesamten EUC!",mtError, TMsgDlgButtons()<<mbOK,0);
		Abort=true;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::dbvPrepareEUCNotExists(TObject *Sender,
      UnicodeString Name, int Idx, bool &CanOverwrite, bool &Cancel)
{
    MessageDlg("Die angegebene Datei bezieht sich auf das EUC mit dem Namen \""+Name+"\", welches nicht gefunden werden kann.",mtError,TMsgDlgButtons()<<mbOK,0);
	frmFortschritt->Close();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::tbOverviewMouseWheel(TObject *Sender, TShiftState Shift,
          int WheelDelta, TPoint &MousePos, bool &Handled)
{
	if (WheelDelta>0) {
		tbOverview->DataSource->DataSet->MoveBy(-3);
	}
	else {
    	tbOverview->DataSource->DataSet->MoveBy(3);
	}
	Handled=true;
	//tbOverview->ScrollBy(0,WheelDelta/40);
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReportVarianceExecute(TObject *Sender)
{
	frmVariance->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::btnSearchClick(TObject *Sender)
{
	dmMain->qryTeams->Requery();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::acReportMaxExecute(TObject *Sender)
{
	frmReportMax->PrepareData();
	frmReportMax->QuickRep1->Preview();
	frmReportMax->CloseData();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMain::edFilterKeyPress(TObject *Sender, System::WideChar &Key)

{
	if (Key=='\r') {
		btnSearch->Click();
	}
}
//---------------------------------------------------------------------------

