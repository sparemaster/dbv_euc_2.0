//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "report_teamliste_breitensport.h"
#include "main.h"
//#include "math.hpp"
#include "datamodule.h"
#include "DBVRoutines.h"
//---------------------------------------------------------------------

#pragma link "DBVEUCBaseComponent"
#pragma link "DBVEUCTeam"
#pragma link "QRCtrls"
#pragma link "QRPDFFilt"
#pragma link "QuickRpt"
#pragma resource "*.dfm"
TfrmAuswertung_Team_Breitensport *frmAuswertung_Team_Breitensport;
//---------------------------------------------------------------------
__fastcall TfrmAuswertung_Team_Breitensport::TfrmAuswertung_Team_Breitensport(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team_Breitensport::PrepareData(bool Composite)
{
	Screen->Cursor=crHourGlass;
	FComposite=Composite;
	HeaderPrinted=false;
	champion_printed=false;
	lblCaption->Font->Size=20;
	counter=0;
	qrpAuswertung->ReportTitle=dmMain->eucEUC->Name2;
	lblStand->Caption=FormatDateTime("dd/' 'mmmm' 'yyyy'  'hh:nn 'Uhr'",Now());

	deleteTable(dmMain->conMain,"auswertung_team_bs");

	if (createTable(dmMain->conMain,"CREATE TABLE auswertung_team_bs (idx AUTOINCREMENT PRIMARY KEY, team INT, Netto INT, Brutto int, BestDurchgang INT, Qualifiziert BIT, Schnitt FLOAT)")) {
		lblTitel->Caption="Endstand (Realpins der Vorrundenspiele 1 bis 6)";
		netto->Enabled=false;
		QRLabel3->Enabled=false;
		brutto->Enabled=true;
		QRLabel4->Enabled=true;
	}

	//�bertrage jetzt die Daten in die Target-Tabelle
	int gesamt;
	qryTeams->Close();
	qryTeams->Parameters->ParamByName("Idx")->Value=dmMain->eucEUC->Idx;
	qryTeams->Open();
	tblTarget->Open();
	while (!qryTeams->Eof) {
		TDBVEUCResultSumData data=eucTeam->getResultSumData();
		tblTarget->Append();
		tblTargetteam->Value=eucTeam->TeamIdx;
		tblTargetNetto->Value=eucTeam->ErgebnisVRNetto+eucTeam->ErgebnisZRNetto/*+eucTeam->ErgebnisFNetto*/;
		tblTargetBrutto->Value=eucTeam->ErgebnisVRNetto+eucTeam->ErgebnisZRNetto/*eucTeam->ErgebnisFNetto*/;
		tblTargetBestDurchgang->Value=eucTeam->MaxDurchgangNettoVR/*MaxDurchgangNettoF*/;
		tblTargetSchnitt->Value=eucTeam->SchnittBruttoVR/*SchnittBruttoF*/;
		tblTarget->Post();
		qryTeams->Next();
	}
	tblTarget->Close();

	//und nun endlich Auswertung anzeigem
	qryTarget->Close();
	qryTarget->Open();
	Screen->Cursor=crDefault;
}
//---------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team_Breitensport::CloseData()
{
	qryTeams->Close();
	deleteTable(dmMain->conMain,"auswertung_team_bs");
}
//---------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team_Breitensport::DetailBand1BeforePrint(
      TQRCustomBand *Sender, bool &PrintBand)
{
  PrintBand=netto->DataSet->FieldByName(netto->DataField)->AsInteger!=0;
  TFontStyles s;
  if (!champion_printed && PrintBand && !qryTargetAusland->Value) {
	s<<fsBold;
	s<<fsUnderline;
	QRSysData3->Font->Style=s;
	QRDBText1->Font->Style=s;
	brutto->Font->Style=s;
	champion_printed=true;
/*
	if (counter>0) {
	  lblHinweis->Enabled=true;
	}
*/
  }
  else {
	QRSysData3->Font->Style=s;
	QRDBText1->Font->Style=s;
	brutto->Font->Style=s;
  }

  counter++;
}
//---------------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team_Breitensport::qrpAuswertungBeforePrint(
      TCustomQuickRep *Sender, bool &PrintReport)
{
	counter=0;
}
//---------------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team_Breitensport::qryTeamsAfterScroll(
	  TDataSet *DataSet)
{
	eucTeam->readFromDB(qryTeams->FieldByName("TeamIdx")->AsInteger);	
}
//---------------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team_Breitensport::QRBand3BeforePrint(
      TQRCustomBand *Sender, bool &PrintBand)
{
	if (FComposite) {
		qrpAuswertung->NewPage();
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team_Breitensport::PageFooterBand1BeforePrint(
      TQRCustomBand *Sender, bool &PrintBand)
{
	//drucke den Hinweis erst ab dritter Seite
	if (FComposite) {
		lblHinweis->Enabled=qrpAuswertung->PageNumber>=3;
	}
	else {
        lblHinweis->Enabled=true;
    }
}
//---------------------------------------------------------------------------

