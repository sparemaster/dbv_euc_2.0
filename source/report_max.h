//---------------------------------------------------------------------------

#ifndef report_maxH
#define report_maxH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "QuickRpt.hpp"
#include <Vcl.ExtCtrls.hpp>
#include "QRCtrls.hpp"
#include <Data.DB.hpp>
#include <Data.Win.ADODB.hpp>
#include "QRPDFFilt.hpp"
//---------------------------------------------------------------------------
class TfrmReportMax : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TQuickRep *QuickRep1;
	TQRBand *QRBand1;
	TQRSysData *lblCaption;
	TQRLabel *lblSubcaption;
	TQRLabel *lblStand;
	TQRLabel *lblTitel;
	TQRGroup *gruppenkopf;
	TQRBand *DetailBand1;
	TQRDBText *QRDBText1;
	TQRBand *gruppenfuss;
	TQRBand *PageFooterBand1;
	TQRSysData *QRSysData2;
	TQRLabel *QRLabel5;
	TQRLabel *QRLabel6;
	TADOQuery *qryMax;
	TIntegerField *qryMaxTurnier;
	TSmallintField *qryMaxspiel1;
	TSmallintField *qryMaxspiel2;
	TSmallintField *qryMaxspiel3;
	TIntegerField *qryMaxspiel4;
	TIntegerField *qryMaxspiel5;
	TIntegerField *qryMaxspiel6;
	TIntegerField *qryMaxspiel7;
	TIntegerField *qryMaxspiel8;
	TIntegerField *qryMaxspiel9;
	TWideStringField *qryMaxNachname;
	TWideStringField *qryMaxVorname;
	TWideStringField *qryMaxGruppe;
	TWideStringField *qryMaxName;
	TIntegerField *qryMaxNummer;
	TQRDBText *QRDBText2;
	TWideStringField *qryMaxSpielername;
	TQRDBText *QRDBText3;
	TQRDBText *QRDBText4;
	TQRDBText *QRDBText5;
	TQRDBText *QRDBText6;
	TQRDBText *QRDBText7;
	TQRDBText *QRDBText8;
	TQRDBText *QRDBText9;
	TQRDBText *QRDBText10;
	TQRDBText *QRDBText11;
	TQRLabel *QRLabel3;
	TQRLabel *QRLabel1;
	TQRPDFFilter *QRPDFFilter1;
	TQRDBText *QRDBText12;
	TQRLabel *QRLabel2;
	TQRLabel *QRLabel4;
	TQRLabel *QRLabel7;
	void __fastcall DetailBand1BeforePrint(TQRCustomBand *Sender, bool &PrintBand);

private:	// Benutzer-Deklarationen
public:		// Benutzer-Deklarationen
	__fastcall TfrmReportMax(TComponent* Owner);
	void __fastcall PrepareData();
	void __fastcall CloseData();
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmReportMax *frmReportMax;
//---------------------------------------------------------------------------
#endif
