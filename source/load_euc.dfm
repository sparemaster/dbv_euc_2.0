object frmLoadEUC: TfrmLoadEUC
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = 'EUC laden'
  ClientHeight = 112
  ClientWidth = 295
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LMDSimplePanel1: TLMDSimplePanel
    Left = 0
    Top = 64
    Width = 295
    Height = 48
    Align = alBottom
    Bevel.StyleInner = bvRaised
    Bevel.StyleOuter = bvFrameLowered
    Bevel.Mode = bmCustom
    TabOrder = 0
    object btnOK: TLMDButton
      Left = 10
      Top = 13
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      TabOrder = 0
      OnClick = btnOKClick
      ButtonStyle = ubsWin40Ext
    end
    object btnCancel: TLMDButton
      Left = 91
      Top = 13
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Abbrechen'
      TabOrder = 1
      OnClick = btnCancelClick
      ButtonStyle = ubsWin40Ext
    end
  end
  object cmbEUC: TLMDListComboBox
    Left = 10
    Top = 24
    Width = 277
    Height = 21
    Bevel.Mode = bmWindows
    Caret.BlinkRate = 530
    TabOrder = 1
    OnChange = cmbEUCChange
    ItemIndex = -1
    Style = csDropDownList
  end
end
