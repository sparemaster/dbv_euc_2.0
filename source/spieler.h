//---------------------------------------------------------------------------

#ifndef spielerH
#define spielerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "LMDBaseEdit.hpp"
#include "LMDButton.hpp"
#include "LMDControl.hpp"
#include "LMDCustomBevelPanel.hpp"
#include "LMDCustomButton.hpp"
#include "LMDCustomControl.hpp"
#include "LMDCustomEdit.hpp"
#include "LMDCustomPanel.hpp"
#include "LMDEdit.hpp"
#include "LMDSimplePanel.hpp"
#include "LMDBaseControl.hpp"
#include "LMDBaseGraphicControl.hpp"
#include "LMDBaseLabel.hpp"
#include "LMDCustomLabel.hpp"
#include "LMDLabel.hpp"
#include "LMDCustomMaskEdit.hpp"
#include "LMDMaskEdit.hpp"
#include "LMDAssist.hpp"
#include "LMDCustomMemo.hpp"
#include "LMDMemo.hpp"
#include "LMDCustomExtCombo.hpp"
#include "LMDCustomListComboBox.hpp"
#include "LMDListComboBox.hpp"
#include "LMDCustomSimpleLabel.hpp"
#include "LMDSimpleLabel.hpp"
#include <ExtCtrls.hpp>
#include "LMDButtonControl.hpp"
#include "LMDCheckBox.hpp"
#include "LMDCustomCheckBox.hpp"
#include "LMDCustomComponent.hpp"
#include "LMDFloatRangeValidator.hpp"
#include "LMDVldBase.hpp"
#include "LMDErrorProvider.hpp"
#include "LMDDockButton.hpp"
#include "DBVEUCPlayerClass.h"
#include "DBVEUCBaseComponent.h"

//---------------------------------------------------------------------------
class TfrmSpieler : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TLMDAssist *LMDAssist1;
	TLMDAssistPage *aspNummer;
	TLMDLabel *lblSpieler;
	TLMDLabeledMaskEdit *edEDV;
	TLMDAssistPage *aspDaten;
	TLMDLabeledEdit *edNachname;
	TLMDLabeledEdit *edVorname;
	TLMDAssistPage *LMDAssistPage1;
	TLMDLabeledListComboBox *cmbGruppe;
	TLMDSimpleLabel *LMDSimpleLabel1;
	TBevel *Bevel2;
	TBevel *Bevel1;
	TLMDSimpleLabel *lblCaption2;
	TLMDCheckBox *cbAusland;
	TLMDSimpleLabel *lblCaption3;
	TBevel *Bevel3;
	TLMDLabeledMaskEdit *edSpieleDBV;
	TLMDLabeledMaskEdit *edPinsDBV;
	TLMDLabeledMaskEdit *edSchnittDBV;
	TLMDLabeledMaskEdit *edPinsZus;
	TLMDLabeledMaskEdit *edSpieleZus;
	TLMDLabeledMaskEdit *edSchnittZus;
	TLMDLabeledMaskEdit *edSchnittFBV;
	TLMDLabeledMaskEdit *edSchnittDBU;
	TLMDLabeledMaskEdit *edSchnittBSV;
	TLMDLabeledMaskEdit *edSchnittEff;
	TLMDFloatRangeValidator *LMDFloatRangeValidator1;
	TLMDErrorProvider *LMDErrorProvider1;
	TLMDDockButton *btnSearch;
	void __fastcall edEDVChange(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall aspDatenBeforeShowPage(TObject *Sender);
	void __fastcall aspDatenCompleted(TObject *Sender, bool &Cancel);
	void __fastcall edNachnameChange(TObject *Sender);
	void __fastcall LMDAssist1Completed(TObject *Sender, bool &Cancel);
	void __fastcall LMDAssist1CancelClick(TObject *Sender);
	void __fastcall edSpieleDBVExit(TObject *Sender);
	void __fastcall LMDAssist1CustomClick(TObject *Sender);
	void __fastcall btnSearchClick(TObject *Sender);
	void __fastcall LMDAssistPage1BeforeShowPage(TObject *Sender);
	void __fastcall edVornameChange(TObject *Sender);
	void __fastcall cmbGruppeChange(TObject *Sender);
	void __fastcall cbAuslandChange(TObject *Sender);
	void __fastcall edSpieleDBVChange(TObject *Sender);
	void __fastcall edPinsDBVChange(TObject *Sender);
	void __fastcall edSpieleZusChange(TObject *Sender);
	void __fastcall edPinsZusChange(TObject *Sender);
	void __fastcall edSchnittFBVChange(TObject *Sender);
	void __fastcall edSchnittDBUChange(TObject *Sender);
	void __fastcall edSchnittBSVChange(TObject *Sender);
private:	// Benutzer-Deklarationen
//	TDBVEUCPlayerClass *FPlayer;
	TDBVEUCPlayerData FPlayerData;
	void __fastcall Data2Edit();
//	void __fastcall Edit2Data();
	void __fastcall clearEdits();
	void __fastcall enableEdits(bool enabled);
	void __fastcall saveData();
	void __fastcall calcSchnittDBV();
	void __fastcall calcSchnittZus();
	void __fastcall calcSchnittEff();
public:		// Benutzer-Deklarationen
	__fastcall TfrmSpieler(TComponent* Owner);
	//TDBVEUCSpieler SpielerData;
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmSpieler *frmSpieler;
//---------------------------------------------------------------------------
#endif
