�
 TFRMAUSWERTUNG_TEAM_BREITENSPORT 0E*  TPF0 TfrmAuswertung_Team_BreitensportfrmAuswertung_Team_BreitensportLeft;Top� CaptionfrmAuswertungClientHeightClientWidth5Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledPixelsPerInch`
TextHeight 	TQuickRepqrpAuswertungLeftTopWidthHeightcFrame.DrawTop	Frame.DrawBottom	Frame.DrawLeft	Frame.DrawRight	BeforePrintqrpAuswertungBeforePrintDataSet	qryTargetFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinFirstPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsStayOnTopPreviewInitialStatewsMaximizedPrevShowThumbsPrevShowSearchPrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestPDFPreviewLeft 
PreviewTop  TQRBandDetailBand1LeftLTop� Width�HeightAlignToBottomBeforePrintDetailBand1BeforePrintColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values       �@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail 
TQRSysData
QRSysData3LeftTopWidthHeightSize.Values2Ъ����@      ��@7�TUUUU�@痪���
�@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataqrsDetailNoText    TransparentExportAsexptTextFontSize
  	TQRDBText	QRDBText1Left:TopWidthWHeightSize.Values�������@UUUUUUu�@UUUUUUU�@TUUUUU��@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSizeColorclWhiteDataSet	qryTarget	DataFieldNameTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  	TQRDBTextnettoLeft�TopWidth9HeightSize.Values2Ъ����@��UUUUw�	@7�TUUUU�@      Ж@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataSet	qryTarget	DataFieldNettoTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  	TQRDBTextbruttoLeft*TopWidth=HeightSize.Values ������@ XUUUU9�	@ XUUUUU�@ XUUUUe�@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataSet	qryTarget	DataFieldBruttoTransparentExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
   TQRBandPageFooterBand1LeftLTop� Width�HeightFFrame.DrawTop	AlignToBottomBeforePrintPageFooterBand1BeforePrintColorclWhiteTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU5�@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageFooter 
TQRSysData
QRSysData2LeftTop3WidthPHeightSize.Values�������@UUUUUUU�@      ��@��������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandColorclWhiteDataqrsPageNumberTextSeite TransparentExportAsexptTextFontSize
  TQRLabelQRLabel5Left�Top3Width� HeightSize.Values�������@      ʙ	@      ��@������z�@ XLColumn 	AlignmenttaRightJustifyAlignToBand	Caption(c) 2003-2013 Michael Kieser  ColorclWhiteTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel6Left=Top WidthHeightSize.Values�������@UUUUUUe�@UUUUUUU�@��������	@ XLColumn 	AlignmenttaCenterAlignToBand	CaptionPErgebnisse gibt's hier: www.dbv-bowling.de - alle Auswertungen zum herunterladenColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabel
lblHinweisLeftTopWidth]HeightSize.Values�������@UUUUUU�@       �@UUUUUU�	@ XLColumn 	AlignmenttaCenterAlignToBand	Captionj   (Hinweis: Teams aus dem Ausland sind im Wettbewerb Deutscher Breitensport-Meisterschaft nicht aufgeführt)ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize	   TQRBandQRBand1LeftLTop9Width�HeightlAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageLinkBandgruppenkopfSize.Values      ��@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader 
TQRSysData
lblCaptionLeft� TopWidth� Height&Size.ValuesUUUUUU�@      Ț@UUUUUUU�@��������@ XLColumn 	AlignmenttaCenterAlignToBand	ColorclWhiteDataqrsReportTitleFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.Style 
ParentFontText    TransparentExportAsexptTextFontSize  TQRLabellblStandLeft"TopLWidth?HeightSize.Values������j�@������ҿ@UUUUUU�@      ��@ XLColumn 	AlignmenttaCenterAlignToBand	CaptionlblStandColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabellblTitelLeft(Top2Width3HeightSize.Values������j�@��������@������J�@      ��@ XLColumn 	AlignmenttaCenterAlignToBand	CaptionlblTitelColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize   TQRGroupgruppenkopfLeftLTop� Width�HeightAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageLinkBandDetailBand1Size.ValuesUUUUUUU�@��������	@ PreCaluculateBandHeightKeepOnOnePage
ExpressionqryTarget.Qualifiziert
FooterBandgruppenfussMasterqrpAuswertungReprintOnNewPage	  TQRBandgruppenfussLeftLTop� Width�HeightAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values       �@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbGroupFooter  TQRBandQRBand2LeftLTop� Width�HeightAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values������j�@��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbColumnHeader TQRLabelQRLabel1LeftTopWidth!HeightSize.Values�������@      ��@       �@      ��@ XLColumn 	AlignmenttaLeftJustifyAlignToBandCaptionPlatzColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel2Left9TopWidth%HeightSize.Values�������@      Ж@       �@��������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandCaptionTeamColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel3Left�TopWidth8HeightSize.Values�������@UUUUUUw�	@       �@������*�@ XLColumn 	AlignmenttaLeftJustifyAlignToBandCaptionRealpinsColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel4Left*TopWidth?HeightSize.Values�������@ XUUUU9�	@       �@      ��@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeCaptionRealpinsColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentExportAsexptText	WrapStyleBreakOnSpacesFontSize
   TQRBandQRBand3LeftLTop� Width�HeightAlignToBottomBeforePrintQRBand3BeforePrintColorclWhiteTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUUU� @��������	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbTitle   	TADOQueryqryTeams
ConnectiondmMain.conMainAfterScrollqryTeamsAfterScroll
ParametersNameIdx
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.StringsSELECT teams.Ausland, *
FROM teams;WHERE (((teams.Turnier)=:Idx) AND ((teams.Ausland)=False));     LeftPTop  TDBVEUCTeameucTeam
ConnectiondmMain.conMainIdxFieldTeamIdxTargetetEUCLeftpTop  	TADOTable	tblTarget
ConnectiondmMain.conMain
CursorTypectStatic	TableNameauswertung_team_bsLeftTop TAutoIncFieldtblTargetidx	FieldNameidxReadOnly	  TIntegerFieldtblTargetteam	FieldNameteam  TIntegerFieldtblTargetNetto	FieldNameNetto  TIntegerFieldtblTargetBrutto	FieldNameBrutto  TIntegerFieldtblTargetBestDurchgang	FieldNameBestDurchgang  TBooleanFieldtblTargetQualifiziert	FieldNameQualifiziert  TFloatFieldtblTargetSchnitt	FieldNameSchnitt   	TADOQuery	qryTarget
ConnectiondmMain.conMain
CursorTypectStatic
Parameters SQL.Strings>select a.*,t.Name,t.Nummer,t.Hdc,t.HDC_Finale, t.Ausland from auswertung_team_bs a,teams twhere (a.Team=t.TeamIdx)8order by a.Brutto DESC,a.Netto DESC,a.BestDurchgang DESC Left!TopE TAutoIncFieldqryTargetidx	FieldNameidxReadOnly	  TIntegerFieldqryTargetteam	FieldNameteam  TIntegerFieldqryTargetNetto	FieldNameNettoDisplayFormat#,##0  TIntegerFieldqryTargetBrutto	FieldNameBruttoDisplayFormat#,##0  TIntegerFieldqryTargetBestDurchgang	FieldNameBestDurchgang  TBooleanFieldqryTargetQualifiziert	FieldNameQualifiziert  TFloatFieldqryTargetSchnitt	FieldNameSchnittDisplayFormat##0.00  TWideStringFieldqryTargetName	FieldNameNameSize(  TIntegerFieldqryTargetNummer	FieldNameNummer  TSmallintFieldqryTargetHdc	FieldNameHdc  TSmallintFieldqryTargetHDC_Finale	FieldName
HDC_Finale  TBooleanFieldqryTargetAusland	FieldNameAusland   	TADOQuery
qryTarget2
ConnectiondmMain.conMain
Parameters SQL.Strings select * from auswertung_team_bsorder by brutto DESC Left� Top  TQRPDFFilterQRPDFFilter1CompressionOnTextEncodingASCIIEncodingCodepage1252LeftXTop8   