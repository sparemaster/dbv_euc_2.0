//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "spieler.h"
#include "datamodule.h"
#include "spielerliste_mv.h"
#include "all.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "LMDBaseEdit"
#pragma link "LMDButton"
#pragma link "LMDControl"
#pragma link "LMDCustomBevelPanel"
#pragma link "LMDCustomButton"
#pragma link "LMDCustomControl"
#pragma link "LMDCustomEdit"
#pragma link "LMDCustomPanel"
#pragma link "LMDEdit"
#pragma link "LMDSimplePanel"
#pragma link "LMDBaseControl"
#pragma link "LMDBaseGraphicControl"
#pragma link "LMDBaseLabel"
#pragma link "LMDCustomLabel"
#pragma link "LMDLabel"
#pragma link "LMDCustomMaskEdit"
#pragma link "LMDMaskEdit"
#pragma link "LMDAssist"
#pragma link "LMDCustomMemo"
#pragma link "LMDMemo"
#pragma link "LMDCustomExtCombo"
#pragma link "LMDCustomListComboBox"
#pragma link "LMDListComboBox"
#pragma link "LMDCustomSimpleLabel"
#pragma link "LMDSimpleLabel"
#pragma link "LMDButtonControl"
#pragma link "LMDCheckBox"
#pragma link "LMDCustomCheckBox"
#pragma link "LMDCustomComponent"
#pragma link "LMDFloatRangeValidator"
#pragma link "LMDVldBase"
#pragma link "LMDErrorProvider"
#pragma link "LMDDockButton"
//#pragma link "DBVEUCPlayer"
#pragma link "DBVEUCBaseComponent"
#pragma resource "*.dfm"
TfrmSpieler *frmSpieler;
//---------------------------------------------------------------------------
__fastcall TfrmSpieler::TfrmSpieler(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::edEDVChange(TObject *Sender)
{
	if ((!edEDV->Text.IsEmpty()) && (edEDV->Text!="0")) {
		int mnummer=edEDV->Text.ToInt();
		//TDBVEUCPlayerData SpielerData;
		memset(&FPlayerData,NULL,sizeof(FPlayerData));
		FPlayerData.mnummer=mnummer;
		getMVMemberData(dmMain->conMV,FPlayerData);
		if (FPlayerData.idx!=0) {
			lblSpieler->Caption=UnicodeString(FPlayerData.nachname)+", "+UnicodeString(FPlayerData.vorname);
		}
		else {
			lblSpieler->Caption="";
		}
	}
	else {
		lblSpieler->Caption="";
	}
	LMDAssist1->VerifyNext();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::FormShow(TObject *Sender)
{
	LMDAssist1->Completed=false;
	LMDAssist1->ActivePage=aspNummer;
	edEDV->Text="";
	edEDV->SetFocus();
	LMDAssist1->Buttons >> abCustom;

//	FPlayer=new TDBVEUCPlayerClass();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::Data2Edit()
{
	edNachname->Text=UnicodeString(FPlayerData.nachname);
	edVorname->Text=UnicodeString(FPlayerData.vorname);
	cmbGruppe->ItemIndex=cmbGruppe->Items->IndexOf(UnicodeString(FPlayerData.gruppe));
	cbAusland->Checked=FPlayerData.ausland;
	edSpieleDBV->Text=IntToStr(FPlayerData.spiele_dbv);
	edPinsDBV->Text=IntToStr(FPlayerData.pins_dbv);
	double base=10.0;
	edSchnittDBV->Text=FormatFloat("##0.00",RoundTo(((float)FPlayerData.schnitt_dbv)/IntPower(base,2),-2));
	edSchnittFBV->Text=FormatFloat("##0.00",RoundTo(((float)FPlayerData.schnitt_fbv)/IntPower(base,2),-2));
	edSchnittDBU->Text=FormatFloat("##0.00",RoundTo(((float)FPlayerData.schnitt_dbu)/IntPower(base,2),-2));
	edSchnittBSV->Text=FormatFloat("##0.00",RoundTo(((float)FPlayerData.schnitt_bsv)/IntPower(base,2),-2));
	edPinsZus->Text=IntToStr(FPlayerData.pins_zus);
	edSpieleZus->Text=IntToStr(FPlayerData.spiele_zus);
	edSchnittZus->Text=FormatFloat("##0.00",RoundTo(((float)FPlayerData.schnitt_zus)/IntPower(base,2),-2));
	edSchnittEff->Text=FormatFloat("##0.00",RoundTo(((float)FPlayerData.schnitt_eff)/IntPower(base,2),-2));
}
//---------------------------------------------------------------------------

/*
void __fastcall TfrmSpieler::Edit2Data()
{
	eucPlayer->Nachname=edNachname->Text;
	eucPlayer->Vorname=edVorname->Text;
	eucPlayer->Gruppe=cmbGruppe->Text;
	eucPlayer->Ausland=cbAusland->Checked;
	eucPlayer->SpieleDBV=edSpieleDBV->Text.ToInt();
	eucPlayer->PinsDBV=edPinsDBV->Text.ToInt();
	eucPlayer->SpieleZus=edSpieleZus->Text.ToInt();
	eucPlayer->PinsZus=edPinsZus->Text.ToInt();
	eucPlayer->SchnittDBV=edSchnittDBV->Text.ToDouble();
	eucPlayer->SchnittDBU=edSchnittDBU->Text.ToDouble();
	eucPlayer->SchnittFBV=edSchnittFBV->Text.ToDouble();
	eucPlayer->SchnittBSV=edSchnittBSV->Text.ToDouble();
//	eucPlayer->SchnittZus=edSchnittZus->Text.ToDouble();
//	SpielerData.schnitt_eff=edSchnittEff->Text.ToDouble();
}
//---------------------------------------------------------------------------
*/


void __fastcall TfrmSpieler::clearEdits()
{
	edNachname->Text="";
	edVorname->Text="";
	cmbGruppe->ItemIndex=-1;
	cbAusland->Checked=false;
	edSpieleDBV->Text=""/*IntToStr(eucPlayer->SpieleDBV)*/;
	edPinsDBV->Text=""/*IntToStr(eucPlayer->PinsDBV)*/;
	edSchnittDBV->Text="";
	edSchnittFBV->Text="";
	edSchnittDBU->Text="";
	edSchnittBSV->Text="";
	edPinsZus->Text="";
	edSpieleZus->Text="";
	edSchnittZus->Text="";
	edSchnittBSV->Text="";
	edSchnittEff->Text="";
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::enableEdits(bool enabled)
{

}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::aspDatenBeforeShowPage(TObject *Sender)
{
	if (FPlayerData.mnummer==0) {
		lblCaption2->Caption="Bitte die Daten des Spielers eingeben und auf \"Weiter >\" klicken";
		clearEdits();
		enableEdits(true);
	}
	else {
		lblCaption2->Caption="Bitte die Daten überprüfen und auf \"Weiter >\" klicken";
		Data2Edit();
		enableEdits(false);
	}
	edNachname->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::aspDatenCompleted(TObject *Sender, bool &Cancel)
{
	Cancel=(UnicodeString(FPlayerData.nachname).IsEmpty()) || (UnicodeString(FPlayerData.vorname).IsEmpty()) || (UnicodeString(FPlayerData.gruppe).IsEmpty());
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::edNachnameChange(TObject *Sender)
{
	strncpy(FPlayerData.nachname,((AnsiString)(edNachname->Text)).c_str(),DBV_MEMBERNACHNAME_Size);
	LMDAssist1->VerifyNext();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::saveData()
{
	TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass(FPlayerData);
	dmMain->eucTeam->addPlayer(p);
	//dmMain->eucTeam->writeToDB(dmMain->eucTeam->TeamIdx);
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::LMDAssist1Completed(TObject *Sender, bool &Cancel)
{
	//speichere jetzt die Daten
//	Edit2Data();
	saveData();
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::LMDAssist1CancelClick(TObject *Sender)
{
	ModalResult=mrCancel;	
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::edSpieleDBVExit(TObject *Sender)
{
	LMDAssist1->VerifyNext();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::LMDAssist1CustomClick(TObject *Sender)
{
	//Button Neu geklickt	
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::btnSearchClick(TObject *Sender)
{
	int ret=frmSpielerlisteMV->Search();
	if (ret!=0) {
        edEDV->Text=IntToStr(ret);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::LMDAssistPage1BeforeShowPage(TObject *Sender)
{
	int i=0;
/*
	edSpieleDBV->Text=IntToStr(eucPlayer->SpieleDBV);
	edPinsDBV->Text=IntToStr(eucPlayer->PinsDBV);
	edSchnittDBV->Text=FormatFloat("##0.00",eucPlayer->SchnittDBV);
*/
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::edVornameChange(TObject *Sender)
{
	strncpy(FPlayerData.vorname,((AnsiString)edVorname->Text).c_str(),DBV_MEMBERVORNAME_Size);
	LMDAssist1->VerifyNext();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::cmbGruppeChange(TObject *Sender)
{
	strncpy(FPlayerData.gruppe,((AnsiString)cmbGruppe->Text).c_str(),DBV_MEMBERGROUP_Size);
	LMDAssist1->VerifyNext();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::cbAuslandChange(TObject *Sender)
{
	FPlayerData.ausland=cbAusland->Checked;
	LMDAssist1->VerifyNext();
}
//---------------------------------------------------------------------------


void __fastcall TfrmSpieler::edSpieleDBVChange(TObject *Sender)
{
	FPlayerData.spiele_dbv=edSpieleDBV->Text.ToInt();
	calcSchnittDBV();
	Data2Edit();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::edPinsDBVChange(TObject *Sender)
{
	FPlayerData.pins_dbv=edPinsDBV->Text.ToInt();
	calcSchnittDBV();
	Data2Edit();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::edSpieleZusChange(TObject *Sender)
{
/*
	if (edSpieleZus->Text.IsEmpty() || (edSpieleZus->Text=="0")) {
		eucPlayer->SpieleZus=0;
	}
	else {
		eucPlayer->SpieleZus=edSpieleZus->Text.ToInt();
	}
*/
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::edPinsZusChange(TObject *Sender)
{
/*
	if (edPinsZus->Text.IsEmpty() || (edPinsZus->Text=="0")) {
		eucPlayer->PinsZus=0;
	}
	else {
		eucPlayer->PinsZus=edPinsZus->Text.ToInt();
	}
*/
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::edSchnittFBVChange(TObject *Sender)
{
	FPlayerData.schnitt_fbv=RoundTo(edSchnittFBV->Text.ToDouble()*IntPower((float)10,2),0);
	calcSchnittEff();
	Data2Edit();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::edSchnittDBUChange(TObject *Sender)
{
	FPlayerData.schnitt_dbu=RoundTo(edSchnittDBU->Text.ToDouble()*IntPower((float)10,2),0);
	calcSchnittEff();
	Data2Edit();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::edSchnittBSVChange(TObject *Sender)
{
	FPlayerData.schnitt_bsv=RoundTo(edSchnittBSV->Text.ToDouble()*IntPower((float)10,2),0);
	calcSchnittEff();
	Data2Edit();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::calcSchnittDBV()
{
	float a=(float)(FPlayerData.pins_dbv);
	float b=(float)(FPlayerData.spiele_dbv);
	if (b>0) {
		FPlayerData.schnitt_dbv=(int)(RoundTo(a/b,-2)*IntPower((float)10,2));
	}
	else {
		FPlayerData.schnitt_dbv=0;
	}
	calcSchnittZus();
	calcSchnittEff();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::calcSchnittZus()
{
	float a=(float)(FPlayerData.pins_dbv+FPlayerData.pins_zus);
	float b=(float)(FPlayerData.spiele_dbv+FPlayerData.spiele_zus);
	if (b>0) {
		FPlayerData.schnitt_zus=(int)(RoundTo(a/b,-2)*IntPower((float)10,2));
	}
	else {
		FPlayerData.schnitt_zus=0;
	}
	calcSchnittEff();
}
//---------------------------------------------------------------------------

void __fastcall TfrmSpieler::calcSchnittEff()
{
	int a,b,c,d;

	a=FPlayerData.schnitt_bsv;
	b=FPlayerData.schnitt_dbu;
	if (FPlayerData.schnitt_zus>0) {
		c=FPlayerData.schnitt_zus;
	}
	else {
		c=FPlayerData.schnitt_dbv;
	}
	d=FPlayerData.schnitt_fbv;
	if ((a>=b) && (a>=c) && (a>=d)) {
		FPlayerData.schnitt_eff=a;
	}
	if ((b>=a) && (b>=c) && (b>=d)) {
		FPlayerData.schnitt_eff=b;
	}
	if ((c>=a) && (c>=b) && (c>=d)) {
		FPlayerData.schnitt_eff=c;
	}
	if ((d>=a) && (d>=b) && (d>=c)) {
		FPlayerData.schnitt_eff=d;
	}
}
//---------------------------------------------------------------------------

