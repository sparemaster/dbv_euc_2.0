�
 TQRPSCHNITTLISTE 0:  TPF0TqrpSchnittlisteqrpSchnittlisteLeft Top ClientHeightjClientWidthFColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style OldCreateOrder	ScaledPixelsPerInch`
TextHeight 	TQuickRepReportLeftTopWidthHeightcFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetqryDataFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.ContinuousPage.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.OutputBinAutoPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage "PrinterSettings.UseStandardprinter PrinterSettings.UseCustomBinCodePrinterSettings.CustomBinCode PrinterSettings.ExtendedDuplex "PrinterSettings.UseCustomPaperCodePrinterSettings.CustomPaperCode PrinterSettings.PrintMetaFilePrinterSettings.PrintQuality PrinterSettings.Collate PrinterSettings.ColorOption PrintIfEmpty	
SnapToGrid	UnitsMMZoomdPrevFormStylefsStayOnTopPreviewInitialStatewsMaximizedPrevShowThumbsPrevShowSearchPrevInitialZoomqrZoomToFitPreviewDefaultSaveTypestPDFPreviewLeft 
PreviewTop  TQRBandQRBand1Left&Top&Width�Height� Frame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUUU�@������v�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageHeader 
TQRSysData
QRSysData1LeftTopWidth� Height%Frame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@UUUUUUA�@       �@��������@ XLColumn 	AlignmenttaCenterAlignToBand	AutoSize	ColorclWhiteDataqrsReportTitleFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentExportAsexptTextFontSize  TQRLabellblTitelLeftMTopFWidth3HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������j�@      D�@UUUUUU5�@      ��@ XLColumn 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionlblTitelColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabellblStandLeftGTop`Width?HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������j�@      L�@       �@      ��@ XLColumn 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionlblStandColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize  TQRLabellblSubcaptionLeft1Top*WidthkHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values��������@��������@      @�@UUUUUU��@ XLColumn 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionlblSubcaptionColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize   TQRBandQRBand4Left&Top� Width�HeightFFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.ValuesUUUUUU5�@������v�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbPageFooter TQRLabel	QRLabel11LeftTop4Width� HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@UUUUUU��@������z�@ XLColumn 	AlignmenttaRightJustifyAlignToBand	AutoSize	AutoStretchCaption(c) 2003-2013 Michael Kieser  ColorclWhiteTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
  
TQRSysData
QRSysData3LeftTop4WidthPHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUU��@��������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	ColorclWhiteDataqrsPageNumberTextSeite TransparentExportAsexptTextFontSize
  TQRLabel	QRLabel12LeftcTopWidthHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@UUUUUU��@��������	@ XLColumn 	AlignmenttaCenterAlignToBand	AutoSize	AutoStretchCaptionPErgebnisse gibt's hier: www.dbv-bowling.de - alle Auswertungen zum herunterladenColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsItalic 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
   TQRBandQRBand2Left&Top� Width�Height(Frame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values��������@������v�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbColumnHeader TQRLabelQRLabel1Left7TopWidth&HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��@������*�@UUUUUU�@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionNameColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel2Left� TopWidth%HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@�������@������*�@��������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTeamColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel3Left�TopWidth*HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������2�	@       �@      @�@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionSpieleColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel4LeftBTopWidthHeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU)�	@UUUUUU�@������*�@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionPinsColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel5Left�TopWidth-HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      T�	@UUUUUU�@       �@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionSchnittColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel6Left@TopWidth"HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@       �@�������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionReal-ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel7Left�TopWidth"HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��	@UUUUUUU�@�������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionReal-ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel8Left�TopWidth"HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUY�	@UUUUUU�@�������@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionSpielColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabelQRLabel9Left�TopWidth9HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUa�	@       �@      Ж@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption	   HöchstesColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
  TQRLabel	QRLabel10LeftTopWidth!HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@������*�@      ��@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionPlatzColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFontSize
   TQRBandQRBand3Left&Top� Width�HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomBeforePrintQRBand3BeforePrintColorclWhiteTransparentBandForceNewColumnForceNewPageSize.Values������j�@������v�	@ PreCaluculateBandHeightKeepOnOnePageBandTyperbDetail 	TQRDBText	QRDBText1Left7TopWidth� HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU��@       �@      (�@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryData	DataFieldSpielernameTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  	TQRDBText	QRDBText2Left� TopWidth� HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUq�@       �@������N�@ XLColumn 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryData	DataFieldTeamnameTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  	TQRDBText	QRDBText3Left�TopWidth%HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ܐ	@       �@��������@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryData	DataField
SpielezahlTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  	TQRDBText	QRDBText4Left7TopWidth3HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��	@       �@      ��@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryData	DataFieldPinsTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  	TQRDBText	QRDBText5LeftxTopWidth?HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU�	@       �@      ��@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryData	DataFieldSchnittTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  	TQRDBText	QRDBText6Left�TopWidth9HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUa�	@       �@      Ж@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryData	DataFieldMaxTransparentWordWrap	ExportAsexptText	WrapStyleBreakOnSpacesFullJustifyMaxBreakChars FontSize
  
TQRSysData
QRSysData2LeftTopWidth HeightFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@       �@UUUUUUU�@ XLColumn 	AlignmenttaCenterAlignToBandAutoSizeColorclWhiteDataqrsDetailNoTransparentExportAsexptTextFontSize
    TLMDIniCtrl
iniControlRegPathSOFTWARE\DBV\EUCRegRootrpLocalMachineLeftsTop  TQRPDFFilterQRPDFFilter1CompressionOnTextEncodingASCIIEncodingCodepage1252Left� Top  	TADOQueryqryData
ConnectiondmMain.conMain
CursorTypectStaticOnCalcFieldsqryDataCalcFields
ParametersName
MaxSpiele1
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value  NameIdx
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value16 Name
MaxSpiele2
Attributes
paNullable DataTypeftWideStringNumericScale� 	Precision� Size�Value   SQL.Strings,SELECT (Spielezahl=:MaxSpiele1) AS InList, *FROM Schnittliste%WHERE (((Schnittliste.Turnier)=:Idx))UORDER BY (Spielezahl=:MaxSpiele2), Schnittliste.Schnitt DESC , Schnittliste.Max DESC; Left@Top8 TIntegerFieldqryDataTurnier	FieldNameTurnier  TWideStringFieldqryDataTeamname	FieldNameTeamnameSize(  TWideStringFieldqryDataNachname	FieldNameNachname  TWideStringFieldqryDataVorname	FieldNameVorname  TWideStringFieldqryDataGruppe	FieldNameGruppeSize
  TIntegerFieldqryDataPins	FieldNamePins  TIntegerFieldqryDataSpielezahl	FieldName
Spielezahl  TFloatFieldqryDataSchnitt	FieldNameSchnittDisplayFormat##0.00  TIntegerField
qryDataMax	FieldNameMax  TStringFieldqryDataSpielername	FieldKindfkCalculated	FieldNameSpielernameSize
Calculated	   TQRHTMLFilterQRHTMLFilter1	MultiPage	PageLinks	FinalPage FirstLastLinksConcatConcatCountLinkFontSizeLinkFontNameArialTextEncodingASCIIEncodingLeftTop(   