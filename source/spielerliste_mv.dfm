object frmSpielerlisteMV: TfrmSpielerlisteMV
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsDialog
  Caption = 'DBV Mitglied suchen'
  ClientHeight = 387
  ClientWidth = 463
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnHide = FormHide
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LMDSimplePanel1: TLMDSimplePanel
    Left = 0
    Top = 347
    Width = 463
    Height = 40
    Align = alBottom
    Bevel.Mode = bmStandard
    Bevel.StandardStyle = lsFrameInset
    TabOrder = 0
    object btnOK: TLMDButton
      Left = 10
      Top = 9
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      TabOrder = 0
      OnClick = btnOKClick
      ButtonStyle = ubsWin40Ext
    end
    object btnCancel: TLMDButton
      Left = 91
      Top = 9
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Abbrechen'
      TabOrder = 1
      OnClick = btnCancelClick
      ButtonStyle = ubsWin40Ext
    end
  end
  object DbAltGrid1: TDbAltGrid
    Left = 8
    Top = 74
    Width = 449
    Height = 267
    AltColors.FixedLine = clBlack
    AltColors.Stripe1 = 16776176
    AltOptions = [dagAutoWidth, dagBandLines, dagDiscreteBandHeight, dagScaleColumns, dagStripedBackground, dagStripedRows]
    DataSource = dtsMitgliederMV
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    OnTitleClick = DbAltGrid1TitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'nachname'
        Title.Caption = 'Nachname'
        Width = 129
      end
      item
        Expanded = False
        FieldName = 'vorname'
        Title.Caption = 'Vorname'
        Width = 129
      end
      item
        Expanded = False
        FieldName = 'mnummer'
        Title.Caption = 'EDV-Nr'
        Width = 50
      end
      item
        Expanded = False
        FieldName = 'anlagenname'
        Title.Caption = 'Anlage'
        Width = 90
      end>
  end
  object cmbAnlage: TLMDLabeledListComboBox
    Left = 16
    Top = 24
    Width = 267
    Height = 21
    Bevel.Mode = bmWindows
    Caret.BlinkRate = 530
    Caret.CanEnable = False
    TabOrder = 2
    OnChange = cmbAnlageChange
    ItemIndex = -1
    Style = csDropDownList
    EditLabel.Width = 35
    EditLabel.Height = 15
    EditLabel.Caption = 'Anlage'
  end
  object qryMitgliederMV: TADOQuery
    Connection = dmMain.conMV
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'anummer'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = Null
      end>
    SQL.Strings = (
      'SELECT m.*, a.anlagenname'
      'FROM anlage AS a INNER JOIN mitglied AS m ON a.idx = m.anlage'
      'WHERE (((a.anlagennummer)=:anummer))'
      'order by'
      'nachname'
      'asc')
    Left = 136
    Top = 32
  end
  object dtsMitgliederMV: TDataSource
    DataSet = qryMitgliederMV
    Left = 216
    Top = 32
  end
end
