//---------------------------------------------------------------------------
#ifndef fortschrittH
#define fortschrittH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CGAUGES.h"
#include <ExtCtrls.hpp>
#include "cgauges.h"
#include "LMDBaseControl.hpp"
#include "LMDBaseGraphicControl.hpp"
#include "LMDBaseMeter.hpp"
#include "LMDCustomProgress.hpp"
#include "LMDGraphicControl.hpp"
#include "LMDProgress.hpp"
#include "LMDCustomComponent.hpp"
#include "LMDCustomFormFill.hpp"
#include "LMDFormFill.hpp"
#include "LMDWndProcComponent.hpp"
//---------------------------------------------------------------------------
class TfrmFortschritt : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
        TLabel *Label1;
	TLMDProgress *Balken;
	TLMDFormFill *LMDFormFill1;
        void __fastcall FormShow(TObject *Sender);
private:	// Anwenderdeklarationen
public:		// Anwenderdeklarationen
        __fastcall TfrmFortschritt(TComponent* Owner);
        void __fastcall Start(int max, UnicodeString message);
        void __fastcall Step(int size);
		void __fastcall Stop();
		void __fastcall Set(int position);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmFortschritt *frmFortschritt;
//---------------------------------------------------------------------------
#endif
