//---------------------------------------------------------------------------

#ifndef teamH
#define teamH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "LMDButton.hpp"
#include "LMDControl.hpp"
#include "LMDCustomBevelPanel.hpp"
#include "LMDCustomButton.hpp"
#include "LMDCustomControl.hpp"
#include "LMDCustomPanel.hpp"
#include "LMDSimplePanel.hpp"
#include "LMDBaseEdit.hpp"
#include "LMDCustomEdit.hpp"
#include "LMDCustomGroupBox.hpp"
#include "LMDCustomMaskEdit.hpp"
#include "LMDCustomParentPanel.hpp"
#include "LMDGroupBox.hpp"
#include "LMDMaskEdit.hpp"
#include "LMDCustomExtCombo.hpp"
#include "LMDCustomListComboBox.hpp"
#include "LMDEdit.hpp"
#include "LMDListComboBox.hpp"
#include "LMDButtonControl.hpp"
#include "LMDCheckBox.hpp"
#include "LMDCustomCheckBox.hpp"
#include "LMDBaseControl.hpp"
#include "LMDBaseGraphicControl.hpp"
#include "LMDBaseLabel.hpp"
#include "LMDCustomSimpleLabel.hpp"
#include "LMDSimpleLabel.hpp"
#include "LMDBaseGraphicButton.hpp"
#include "LMDCustomSpeedButton.hpp"
#include "LMDSpeedButton.hpp"
#include "DBVEUCPlayerClass.h"
//---------------------------------------------------------------------------
class TfrmTeam : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TLMDSimplePanel *LMDSimplePanel1;
	TLMDButton *btnOK;
	TLMDButton *btnCancel;
	TLMDGroupBox *LMDGroupBox1;
	TLMDLabeledMaskEdit *edNumber;
	TLMDLabeledListComboBox *cmbGroup;
	TLMDLabeledEdit *edName;
	TLMDLabeledListComboBox *cmbCenter;
	TLMDCheckBox *cbForeigner;
	TLMDGroupBox *LMDGroupBox2;
	TLMDMaskEdit *edNumber1;
	TLMDEdit *edNachname1;
	TLMDSimpleLabel *LMDSimpleLabel1;
	TLMDSimpleLabel *LMDSimpleLabel2;
	TLMDEdit *edVorname1;
	TLMDSimpleLabel *LMDSimpleLabel3;
	TLMDMaskEdit *edDBV1;
	TLMDSimpleLabel *LMDSimpleLabel4;
	TLMDMaskEdit *edFBV1;
	TLMDSimpleLabel *LMDSimpleLabel5;
	TLMDMaskEdit *edDBU1;
	TLMDSimpleLabel *LMDSimpleLabel6;
	TLMDMaskEdit *edBSV1;
	TLMDSimpleLabel *LMDSimpleLabel7;
	TLMDMaskEdit *edEff1;
	TLMDSimpleLabel *LMDSimpleLabel8;
	TLMDMaskEdit *edNumber2;
	TLMDEdit *edNachname2;
	TLMDEdit *edVorname2;
	TLMDMaskEdit *edDBV2;
	TLMDMaskEdit *edFBV2;
	TLMDMaskEdit *edDBU2;
	TLMDMaskEdit *edBSV2;
	TLMDMaskEdit *edEff2;
	TLMDMaskEdit *edNumber3;
	TLMDEdit *edNachname3;
	TLMDEdit *edVorname3;
	TLMDMaskEdit *edDBV3;
	TLMDMaskEdit *edFBV3;
	TLMDMaskEdit *edDBU3;
	TLMDMaskEdit *edBSV3;
	TLMDMaskEdit *edEff3;
	TLMDMaskEdit *edNumber4;
	TLMDEdit *edNachname4;
	TLMDEdit *edVorname4;
	TLMDMaskEdit *edDBV4;
	TLMDMaskEdit *edFBV4;
	TLMDMaskEdit *edDBU4;
	TLMDMaskEdit *edBSV4;
	TLMDMaskEdit *edEff4;
	TLMDMaskEdit *edNumber5;
	TLMDEdit *edNachname5;
	TLMDEdit *edVorname5;
	TLMDMaskEdit *edDBV5;
	TLMDMaskEdit *edFBV5;
	TLMDMaskEdit *edDBU5;
	TLMDMaskEdit *edBSV5;
	TLMDMaskEdit *edEff5;
	TLMDMaskEdit *edNumber6;
	TLMDEdit *edNachname6;
	TLMDEdit *edVorname6;
	TLMDMaskEdit *edDBV6;
	TLMDMaskEdit *edFBV6;
	TLMDMaskEdit *edDBU6;
	TLMDMaskEdit *edBSV6;
	TLMDMaskEdit *edEff6;
	TLMDSimpleLabel *lblIdx1;
	TLMDSimpleLabel *lblIdx2;
	TLMDSimpleLabel *lblIdx3;
	TLMDSimpleLabel *lblIdx4;
	TLMDSimpleLabel *lblIdx5;
	TLMDSimpleLabel *lblIdx6;
	TLMDButton *btnClose;
	TLMDCheckBox *cbHerren1;
	TLMDCheckBox *cbHerren2;
	TLMDCheckBox *cbHerren3;
	TLMDCheckBox *cbHerren4;
	TLMDCheckBox *cbHerren5;
	TLMDCheckBox *cbHerren6;
	TLMDSimpleLabel *LMDSimpleLabel9;
	TLMDSpeedButton *btnAdd1;
	TLMDSpeedButton *btnDelete1;
	TLMDSpeedButton *btnAdd2;
	TLMDSpeedButton *btnDelete2;
	TLMDSpeedButton *btnAdd3;
	TLMDSpeedButton *btnDelete3;
	TLMDSpeedButton *btnAdd4;
	TLMDSpeedButton *btnDelete4;
	TLMDSpeedButton *btnAdd5;
	TLMDSpeedButton *btnDelete5;
	TLMDSpeedButton *btnAdd6;
	TLMDSpeedButton *btnDelete6;
	TLMDLabeledMaskEdit *edHDC;
	TLMDSpeedButton *btnHDC;
	void __fastcall btnOKClick(TObject *Sender);
	void __fastcall btnCancelClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall btnCloseClick(TObject *Sender);
	void __fastcall btnDelete1Click(TObject *Sender);
	void __fastcall edNachname1Enter(TObject *Sender);
	void __fastcall edNachname1Change(TObject *Sender);
	void __fastcall edVorname1Change(TObject *Sender);
	void __fastcall edDBV1Change(TObject *Sender);
	void __fastcall edFBV1Change(TObject *Sender);
	void __fastcall edDBU1Change(TObject *Sender);
	void __fastcall edBSV1Change(TObject *Sender);
	void __fastcall edHDCChange(TObject *Sender);
	void __fastcall btnHDCClick(TObject *Sender);
	void __fastcall btnAdd1Click(TObject *Sender);
	void __fastcall cbHerren1Click(TObject *Sender);
	void __fastcall edNameChange(TObject *Sender);
	void __fastcall cbForeignerChange(TObject *Sender);
private:	// Benutzer-Deklarationen
	bool changed;
	bool FLoading;
	TDBVEUCPlayerClass *CurrentPlayer;
	void __fastcall Data2Edit();
	void __fastcall Edit2Data();
	void __fastcall clearEdits();
	void __fastcall setChanged(bool isChanged);
	void __fastcall selectPlayer(int idx);
	TLMDMaskEdit* __fastcall getEffField(int idx);
public:		// Benutzer-Deklarationen
	__fastcall TfrmTeam(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmTeam *frmTeam;
//---------------------------------------------------------------------------
#endif
