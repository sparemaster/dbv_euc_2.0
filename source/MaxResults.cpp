//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "MaxResults.h"
#include "datamodule.h"
#include "DBVRoutines.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DbAGrids"
#pragma link "LMDControl"
#pragma link "LMDCustomBevelPanel"
#pragma link "LMDCustomButtonGroup"
#pragma link "LMDCustomControl"
#pragma link "LMDCustomGroupBox"
#pragma link "LMDCustomPanel"
#pragma link "LMDCustomParentPanel"
#pragma link "LMDCustomRadioGroup"
#pragma link "LMDRadioGroup"
#pragma link "LMDSimplePanel"
#pragma resource "*.dfm"
TfrmMaxResults *frmMaxResults;
//---------------------------------------------------------------------------
__fastcall TfrmMaxResults::TfrmMaxResults(TComponent* Owner)
	: TForm(Owner)
{
	FSortField="MaxAll";
	FSortDir="DESC";
}
//---------------------------------------------------------------------------

void __fastcall TfrmMaxResults::FormShow(TObject *Sender)
{
	tbMax->DataSource=dmMain->dtsMaxResults;
	rgFilter->ItemIndex=2;
	dmMain->qryMaxResults->Open();
}
//---------------------------------------------------------------------------

void __fastcall TfrmMaxResults::FormClose(TObject *Sender, TCloseAction &Action)
{
	dmMain->qryMaxResults->Close();
}
//---------------------------------------------------------------------------
void __fastcall TfrmMaxResults::rgFilterChange(TObject *Sender, int ButtonIndex)
{
	dmMain->qryMaxResults->Close();
	dmMain->qryMaxResults->Open();
}
//---------------------------------------------------------------------------

