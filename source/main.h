//---------------------------------------------------------------------------

#ifndef mainH
#define mainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "DbAGrids.hpp"
#include <DBGrids.hpp>
#include <Grids.hpp>
#include "DBVBaseComponent.h"
#include "DBVEUCPrepare.h"
#include <Menus.hpp>
#include "LMDActnList.hpp"
#include "LMDCustomComponent.hpp"
#include "LMDFormVista.hpp"
#include "LMDWndProcComponent.hpp"
#include <ActnList.hpp>
#include "LMDBaseControl.hpp"
#include "LMDBaseGraphicControl.hpp"
#include "LMDBaseLabel.hpp"
#include "LMDControl.hpp"
#include "LMDCustomBevelPanel.hpp"
#include "LMDCustomControl.hpp"
#include "LMDCustomGroupBox.hpp"
#include "LMDCustomPanel.hpp"
#include "LMDCustomParentPanel.hpp"
#include "LMDCustomSimpleLabel.hpp"
#include "LMDCustomStatusBar.hpp"
#include "LMDGroupBox.hpp"
#include "LMDInformationLabel.hpp"
#include "LMDSimpleLabel.hpp"
#include "LMDSimplePanel.hpp"
#include "LMDStatusBar.hpp"
#include "LMDBaseGraphicButton.hpp"
#include "LMDCustomSpeedButton.hpp"
#include "LMDSpeedButton.hpp"
#include "LMDApplicationCtrl.hpp"
#include <ImgList.hpp>
#include "dagTmplt.hpp"
#include "LMDVistaDialogs.hpp"
#include "LMDPopupMenu.hpp"
#include "LMDSectionBar.hpp"
#include "DBVResultFile.h"
#include "LMDIniCtrl.hpp"
#include <QuickRpt.hpp>
#include <DB.hpp>
#include "LMDBaseEdit.hpp"
#include "LMDCustomButton.hpp"
#include "LMDCustomEdit.hpp"
#include "LMDDockButton.hpp"
#include "LMDEdit.hpp"
#include <System.Actions.hpp>
#include <Vcl.ExtCtrls.hpp>

//---------------------------------------------------------------------------
class TfrmMain : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TDBVEUCPrepare *dbvPrepare;
	TLMDFormVistaExtension *LMDFormVistaExtension1;
	TLMDFormGlass *LMDFormGlass1;
	TLMDActionList *aclMain;
	TAction *acEUCLoad;
	TAction *acClose;
	TLMDStatusBar *LMDStatusBar1;
	TLMDSimplePanel *pnlClient;
	TDbAltGrid *tbOverview;
	TLMDSimpleLabel *LMDSimpleLabel1;
	TLMDInformationLabel *LMDInformationLabel1;
	TLMDInformationLabel *LMDInformationLabel2;
	TLMDInformationLabel *LMDInformationLabel3;
	TLMDInformationLabel *LMDInformationLabel4;
	TLMDApplicationCtrl *LMDApplicationCtrl1;
	TImageList *imlMain;
	TAction *acReload;
	TdagLookupList *dagLookupList1;
	TAction *acImport;
	TLMDFileOpenDialog *odOpen;
	TLMDFileSaveDialog *sdSave;
	TAction *acExport;
	TLMDPopupMenu *popMain;
	TAction *acTeamResultEdit;
	TMenuItem *Ergebnisseerfassen1;
	TAction *acTeamEdit;
	TMenuItem *eamdatenbearbeiten1;
	TMenuItem *N1;
	TAction *acTeamResultDelete;
	TMenuItem *Ergebnisselschen1;
	TAction *acTeamNotePrint;
	TMenuItem *Spielzetteldrucken1;
	TMenuItem *N2;
	TAction *acEUCEdit;
	TAction *acReportAverageMale;
	TAction *acReportAverageFemale;
	TAction *acReportAverageAll;
	TAction *acReportMax;
	TLMDSimplePanel *pnlCaption;
	TLMDSimpleLabel *lblTitle;
	TLMDSimpleLabel *lblTitle2;
	TAction *acReportTeamListVR1;
	TAction *acReportTeamListVR2;
	TAction *acReportTeamListF;
	TAction *acReportTeamListFScratch;
	TDBVResultFile *dbvResultFile;
	TLMDFileSaveDialog *sdResult;
	TAction *acExportResults;
	TAction *acReportTeamListVR2int;
	TAction *acReportStart;
	TAction *acStartTime;
	TAction *acTeamAddPlayer;
	TAction *acTeamDeletePlayer;
	TLMDIniCtrl *iniCtrl;
	TAction *acMaxResults;
	TQRCompositeReport *QRCompositeReport1;
	TAction *acFinalReports;
	TLMDSimplePanel *LMDSimplePanel1;
	TLMDSectionBar *sebMain;
	TAction *acTeamImport;
	TAction *acReportVariance;
	TLMDSimplePanel *LMDSimplePanel2;
	TLMDLabeledEdit *edFilter;
	TLMDDockButton *btnSearch;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall acEUCLoadExecute(TObject *Sender);
	void __fastcall tbOverviewTitleClick(TColumn *Column);
	void __fastcall acCloseExecute(TObject *Sender);
	void __fastcall acReloadExecute(TObject *Sender);
	void __fastcall acImportExecute(TObject *Sender);
	void __fastcall dbvPrepareStartReadWrite(TObject *Sender,
          TDBVFileStatus FileStatus, int Count);
	void __fastcall dbvPrepareProgress(TObject *Sender, TDBVFileStatus FileStatus,
          int Max, int Progress);
	void __fastcall dbvPrepareSuccess(TObject *Sender, TDBVFileStatus FileStatus,
          TDBVFileType FileType);
	void __fastcall acExportExecute(TObject *Sender);
	void __fastcall acTeamResultEditExecute(TObject *Sender);
	void __fastcall acTeamEditExecute(TObject *Sender);
	void __fastcall acTeamResultDeleteExecute(TObject *Sender);
	void __fastcall acTeamNotePrintExecute(TObject *Sender);
	void __fastcall acEUCEditExecute(TObject *Sender);
	void __fastcall acReportAverageMaleExecute(TObject *Sender);
	void __fastcall acReportAverageFemaleExecute(TObject *Sender);
	void __fastcall acReportAverageAllExecute(TObject *Sender);
	void __fastcall acReportTeamListVR1Execute(TObject *Sender);
	void __fastcall acReportTeamListVR2Execute(TObject *Sender);
	void __fastcall acReportTeamListFExecute(TObject *Sender);
	void __fastcall acReportTeamListFScratchExecute(TObject *Sender);
	void __fastcall acExportResultsExecute(TObject *Sender);
	void __fastcall dbvResultFileSuccess(TObject *Sender,
          TDBVFileStatus FileStatus, TDBVFileType FileType);
	void __fastcall acReportTeamListVR2intExecute(TObject *Sender);
	void __fastcall acReportStartExecute(TObject *Sender);
	void __fastcall dbvPrepareItemExists(TObject *Sender, UnicodeString Name, int Idx,
          bool &CanOverwrite, bool &Cancel);
	void __fastcall acStartTimeExecute(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall acMaxResultsExecute(TObject *Sender);
	void __fastcall acFinalReportsExecute(TObject *Sender);
	void __fastcall QRCompositeReport1AddReports(TObject *Sender);
	void __fastcall acTeamImportExecute(TObject *Sender);
	void __fastcall dbvPrepareReadHeader(TObject *Sender,
          TDBVFileHeader FileHeader, bool &Abort);
	void __fastcall dbvPrepareEUCNotExists(TObject *Sender, UnicodeString Name,
          int Idx, bool &CanOverwrite, bool &Cancel);
	void __fastcall tbOverviewMouseWheel(TObject *Sender, TShiftState Shift, int WheelDelta,
          TPoint &MousePos, bool &Handled);
	void __fastcall acReportVarianceExecute(TObject *Sender);
	void __fastcall btnSearchClick(TObject *Sender);
	void __fastcall acReportMaxExecute(TObject *Sender);
	void __fastcall edFilterKeyPress(TObject *Sender, System::WideChar &Key);
private:	// Benutzer-Deklarationen
	UnicodeString FSortField,FSortDir;
	bool FImportTeam;
	void __fastcall enableMenu(bool enabled);
public:		// Benutzer-Deklarationen
	__fastcall TfrmMain(TComponent* Owner);
	void __fastcall showFooterValues();
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmMain *frmMain;
//---------------------------------------------------------------------------
#endif
