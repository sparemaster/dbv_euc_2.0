//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("schnittliste.cpp", qrpSchnittliste); /* TQuickRep: File Type */
USEFORM("spieler.cpp", frmSpieler);
USEFORM("result.cpp", frmResult);
USEFORM("report_teamliste.cpp", frmAuswertung_Team);
USEFORM("report_teamliste_breitensport.cpp", frmAuswertung_Team_Breitensport);
USEFORM("team.cpp", frmTeam);
USEFORM("variance.cpp", frmVariance);
USEFORM("startzeiten.cpp", frmStartTime);
USEFORM("spielerliste_mv.cpp", frmSpielerlisteMV);
USEFORM("spielzettel.cpp", qrpSpielzettel); /* TQuickRep: File Type */
USEFORM("euc.cpp", frmEUC);
USEFORM("datamodule.cpp", dmMain); /* TDataModule: File Type */
USEFORM("MaxResults.cpp", frmMaxResults);
USEFORM("report_startliste.cpp", frmReportStartliste);
USEFORM("main.cpp", frmMain);
USEFORM("fortschritt.cpp", frmFortschritt);
USEFORM("load_euc.cpp", frmLoadEUC);
USEFORM("report_max.cpp", frmReportMax);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	try
	{
		Application->Initialize();
		Application->Title = "DBV EUC Ergebniserfassung";
		Application->CreateForm(__classid(TfrmMain), &frmMain);
		Application->CreateForm(__classid(TdmMain), &dmMain);
		Application->CreateForm(__classid(TfrmLoadEUC), &frmLoadEUC);
		Application->CreateForm(__classid(TfrmResult), &frmResult);
		Application->CreateForm(__classid(TfrmFortschritt), &frmFortschritt);
		Application->CreateForm(__classid(TfrmTeam), &frmTeam);
		Application->CreateForm(__classid(TfrmEUC), &frmEUC);
		Application->CreateForm(__classid(TqrpSchnittliste), &qrpSchnittliste);
		Application->CreateForm(__classid(TqrpSpielzettel), &qrpSpielzettel);
		Application->CreateForm(__classid(TfrmAuswertung_Team), &frmAuswertung_Team);
		Application->CreateForm(__classid(TfrmAuswertung_Team_Breitensport), &frmAuswertung_Team_Breitensport);
		Application->CreateForm(__classid(TfrmReportStartliste), &frmReportStartliste);
		Application->CreateForm(__classid(TfrmStartTime), &frmStartTime);
		Application->CreateForm(__classid(TfrmSpieler), &frmSpieler);
		Application->CreateForm(__classid(TfrmSpielerlisteMV), &frmSpielerlisteMV);
		Application->CreateForm(__classid(TfrmVariance), &frmVariance);
		Application->CreateForm(__classid(TfrmMaxResults), &frmMaxResults);
		Application->CreateForm(__classid(TfrmReportMax), &frmReportMax);
		Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
