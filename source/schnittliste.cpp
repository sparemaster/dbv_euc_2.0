//----------------------------------------------------------------------------
#pragma hdrstop

#include "schnittliste.h"
#include "datamodule.h"
#include "DBVRoutines.h"
//----------------------------------------------------------------------------
#pragma link "LMDCustomComponent"
#pragma link "LMDIniCtrl"
#pragma link "QRPDFFilt"
#pragma link "DBVEUCBaseComponent"
#pragma link "DBVEUCTeam"
#pragma link "QRWebFilt"
#pragma resource "*.dfm"
TqrpSchnittliste *qrpSchnittliste;
//----------------------------------------------------------------------------
__fastcall TqrpSchnittliste::TqrpSchnittliste(TComponent* Owner)
	: TForm(Owner)
{
}
//----------------------------------------------------------------------------

void __fastcall TqrpSchnittliste::PrepareData(TDBVEUCItemType itemtype,TDBVReportType reporttype)
{
	int gamecount;
	switch (itemtype) {
		case itAuswertungVR1: gamecount=dmMain->eucEUC->GamesVR1 ;break;
		case itAuswertungVR2: gamecount=dmMain->eucEUC->GamesVR1+dmMain->eucEUC->GamesVR2;break;
		case itAuswertungVR2int: gamecount=dmMain->eucEUC->GamesVR1+dmMain->eucEUC->GamesVR2;break;
		case itAuswertungF: gamecount=dmMain->eucEUC->GamesVR1+dmMain->eucEUC->GamesVR2+dmMain->eucEUC->GamesF;break;
	default:
		;
	}

	qryData->Parameters->ParamByName("Idx")->Value=dmMain->eucEUC->Idx;
	qryData->Parameters->ParamByName("MaxSpiele1")->Value=gamecount;
	qryData->Parameters->ParamByName("MaxSpiele2")->Value=gamecount;
	Report->ReportTitle=dmMain->eucEUC->Name1;
	lblSubcaption->Caption=dmMain->eucEUC->Name2;

	switch (reporttype) {
		case rtMale:
			lblTitel->Caption="Einzelwertung der Herren";
			qryData->Filtered=true;
			qryData->Filter="Gruppe='Herren'";
			break;
		case rtFemale:
			lblTitel->Caption="Einzelwertung der Damen";
			qryData->Filtered=true;
			qryData->Filter="Gruppe='Damen'";
			break;
	default:
			lblTitel->Caption="Einzelwertung Gesamt";
			qryData->Filtered=false;
	}
    lblStand->Caption=FormatDateTime("dd/mm/yyyy' 'hh:nn' Uhr'",Now());

	qryData->Open();

	show_empty_row=false;
	sort_games=true;
	max_games=qryData->FieldByName("Spielezahl")->AsInteger;
}
//----------------------------------------------------------------------------

void __fastcall TqrpSchnittliste::CloseData()
{
	qryData->Close();
}
//----------------------------------------------------------------------------


void __fastcall TqrpSchnittliste::QRBand3BeforePrint(TQRCustomBand *Sender,
      bool &PrintBand)
{
  QRBand3->Height=23;
  int top=3;
  if (sort_games && !show_empty_row && qryDataSpielezahl->Value<max_games) {
	QRBand3->Height=QRBand3->Height+23;
	top=top+23;
	show_empty_row=true;
  }
  QRSysData2->Top=top;
  QRDBText1->Top=top;
  QRDBText2->Top=top;
  QRDBText3->Top=top;
  QRDBText4->Top=top;
  QRDBText5->Top=top;
  QRDBText6->Top=top;
}
//---------------------------------------------------------------------------



void __fastcall TqrpSchnittliste::qryDataCalcFields(TDataSet *DataSet)
{
	DataSet->FieldByName("Spielername")->AsString=DataSet->FieldByName("Nachname")->AsString+", "+DataSet->FieldByName("Vorname")->AsString;
}
//---------------------------------------------------------------------------

