//---------------------------------------------------------------------------

#ifndef datamoduleH
#define datamoduleH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include "DBVEUCTeam.h"
#include "DBVEUCPlayer.h"
#include "DBVEUCResult.h"
#include "DBVEUCBaseComponent.h"
#include "DBVEUCResultClass.h"
#include "DBVEUCTeamClass.h"
#include "DBVEUC.h"

//---------------------------------------------------------------------------
class TdmMain : public TDataModule
{
__published:	// Von der IDE verwaltete Komponenten
	TADOConnection *conMain;
	TADOQuery *qryTeams;
	TDataSource *dtsTeams;
	TADOTable *tblEUC;
	TAutoIncField *qryTeamsTeamIdx;
	TIntegerField *qryTeamsTurnier;
	TWideStringField *qryTeamsName;
	TIntegerField *qryTeamsNummer;
	TSmallintField *qryTeamsHdc;
	TSmallintField *qryTeamsHDC_Finale;
	TBooleanField *qryTeamsAusland;
	TIntegerField *qryTeamsBahn;
	TIntegerField *qryTeamsBahn_ZR;
	TIntegerField *qryTeamsBahn_F;
	TIntegerField *qryTeamsAnlage;
	TIntegerField *qryTeamsStatus;
	TIntegerField *qryTeamsVorrunde;
	TIntegerField *qryTeamsZwischenrunde;
	TIntegerField *qryTeamsFinale;
	TIntegerField *qryTeamsGruppe;
	TADOQuery *qryCenter;
	TADOQuery *qryGroup;
	TIntegerField *qryTeamsVR1_D1;
	TIntegerField *qryTeamsVR1_D2;
	TIntegerField *qryTeamsVR1_D3;
	TIntegerField *qryTeamsVR2_D1;
	TIntegerField *qryTeamsVR2_D2;
	TIntegerField *qryTeamsVR2_D3;
	TIntegerField *qryTeamsF_D1;
	TIntegerField *qryTeamsF_D2;
	TIntegerField *qryTeamsF_D3;
	TIntegerField *qryTeamsVorrundeGesamt;
	TADOTable *tblInit;
	TDataSource *dtsGroup;
	TADOConnection *conMV;
	TADOQuery *qryMaxSpiel;
	TADOQuery *qryAnlage;
	TADOQuery *qryMaxResults;
	TDataSource *dtsMaxResults;
	TADOQuery *qryStartCount;
	TADOQuery *qryVariance;
	TWideStringField *qryVarianceNachname;
	TWideStringField *qryVarianceVorname;
	TWideStringField *qryVarianceName;
	TWideStringField *qryVarianceGruppe;
	TIntegerField *qryVariancemnummer;
	TIntegerField *qryVarianceTeam;
	TIntegerField *qryVarianceanlage;
	TIntegerField *qryVarianceturnier;
	TSmallintField *qryVariancespiel1;
	TSmallintField *qryVariancespiel2;
	TSmallintField *qryVariancespiel3;
	TIntegerField *qryVariancespiel4;
	TIntegerField *qryVariancespiel5;
	TIntegerField *qryVariancespiel6;
	TIntegerField *qryVariancespiel7;
	TIntegerField *qryVariancespiel8;
	TIntegerField *qryVariancespiel9;
	TIntegerField *qryVarianceVarianz;
	TDataSource *dtsVariance;
	TADOQuery *qryVarianceTarget;
	TAutoIncField *qryVarianceSpielerIdx;
	TADOTable *tblSpieler;
	TAutoIncField *qryVarianceTargetIdx;
	TIntegerField *qryVarianceTargetSpielerIdx;
	TIntegerField *qryVarianceTargetMinimum;
	TIntegerField *qryVarianceTargetMaximum;
	TStringField *qryVarianceTargetSpielername;
	TIntegerField *qryVarianceMinimum;
	TIntegerField *qryVarianceMaximum;
	TIntegerField *qryVarianceTargetVarianzzz;
	TADOCommand *cmdUpdate;
	TDBVEUC *eucEUC;
	TDBVEUCTeam *eucTeam;
	void __fastcall qryTeamsAfterScroll(TDataSet *DataSet);
	void __fastcall qryTeamsCalcFields(TDataSet *DataSet);
	void __fastcall qryGroupBeforeOpen(TDataSet *DataSet);
	void __fastcall qryTeamsBeforeOpen(TDataSet *DataSet);
	void __fastcall qryTeamsAfterOpen(TDataSet *DataSet);
	void __fastcall qryMaxResultsBeforeOpen(TDataSet *DataSet);
	void __fastcall DataModuleCreate(TObject *Sender);
	void __fastcall qryStartCountBeforeOpen(TDataSet *DataSet);
	void __fastcall qryStartCountAfterOpen(TDataSet *DataSet);
	void __fastcall qryMaxResultsFilterRecord(TDataSet *DataSet, bool &Accept);
	void __fastcall qryMaxSpielCalcFields(TDataSet *DataSet);
	void __fastcall qryVarianceBeforeOpen(TDataSet *DataSet);
	void __fastcall qryVarianceCalcFields(TDataSet *DataSet);
	void __fastcall conMainConnectComplete(TADOConnection *Connection, const Error *Error,
          TEventStatus &EventStatus);
	void __fastcall tblEUCBeforePost(TDataSet *DataSet);
	void __fastcall qryTeamsFilterRecord(TDataSet *DataSet, bool &Accept);
private:	// Benutzer-Deklarationen
	UnicodeString db_path,db_path_mv,db_name;
public:		// Benutzer-Deklarationen
	__fastcall TdmMain(TComponent* Owner);
	bool __fastcall selectEUC(int EUCIdx, bool reload);
	_GUID __fastcall getProgID();
	TDBVEUCItemType __fastcall getCurrentRound();
};
//---------------------------------------------------------------------------
extern PACKAGE TdmMain *dmMain;
//---------------------------------------------------------------------------
#endif
