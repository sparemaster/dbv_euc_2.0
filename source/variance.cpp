//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "variance.h"
#include "datamodule.h"
#include "DBVRoutines.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DbAGrids"
#pragma link "LMDControl"
#pragma link "LMDCustomBevelPanel"
#pragma link "LMDCustomButtonGroup"
#pragma link "LMDCustomControl"
#pragma link "LMDCustomGroupBox"
#pragma link "LMDCustomPanel"
#pragma link "LMDCustomParentPanel"
#pragma link "LMDCustomRadioGroup"
#pragma link "LMDRadioGroup"
#pragma link "LMDSimplePanel"
#pragma resource "*.dfm"
TfrmVariance *frmVariance;
//---------------------------------------------------------------------------
__fastcall TfrmVariance::TfrmVariance(TComponent* Owner)
	: TForm(Owner)
{
	FSortField="Vorname";
	FSortDir="DESC";
}
//---------------------------------------------------------------------------

void __fastcall TfrmVariance::FormShow(TObject *Sender)
{
	Screen->Cursor=crHourGlass;
	try {
		tbMax->DataSource=dmMain->dtsVariance;
		if (TableExists(dmMain->conMain,"Variance")) {
			deleteTable(dmMain->conMain,"Variance");
		}
		createTable(dmMain->conMain,"create table variance (Idx AUTOINCREMENT,SpielerIdx INTEGER,Minimum INTEGER, Maximum INTEGER, Varianzzz INTEGER)");
		dmMain->qryVariance->Open();
		int reccount=dmMain->qryVariance->RecordCount;
		dmMain->qryVarianceTarget->Open();
		while (!dmMain->qryVariance->Eof) {
			dmMain->qryVarianceTarget->Append();
			dmMain->qryVarianceTarget->FieldByName("SpielerIdx")->AsInteger=dmMain->qryVariance->FieldByName("SpielerIdx")->AsInteger;
			dmMain->qryVarianceTarget->FieldByName("Minimum")->AsInteger=dmMain->qryVariance->FieldByName("Minimum")->AsInteger;
			dmMain->qryVarianceTarget->FieldByName("Maximum")->AsInteger=dmMain->qryVariance->FieldByName("Maximum")->AsInteger;
			dmMain->qryVarianceTarget->FieldByName("Varianzzz")->AsInteger=dmMain->qryVariance->FieldByName("Maximum")->AsInteger-dmMain->qryVariance->FieldByName("Minimum")->AsInteger;
			dmMain->qryVarianceTarget->Post();
			dmMain->qryVariance->Next();
		}
		dmMain->qryVariance->Close();
		dmMain->qryVarianceTarget->Close();
		dmMain->qryVarianceTarget->Open();
	}
	__finally {
		Screen->Cursor=crDefault;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmVariance::FormClose(TObject *Sender, TCloseAction &Action)
{
	dmMain->qryVarianceTarget->Close();
}
//---------------------------------------------------------------------------
void __fastcall TfrmVariance::rgFilterChange(TObject *Sender, int ButtonIndex)
{
	dmMain->qryVariance->Close();
	dmMain->qryVariance->Open();
}
//---------------------------------------------------------------------------

