//---------------------------------------------------------------------------

#ifndef resultH
#define resultH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <DB.hpp>
#include "DBVTypes.h"
#include "LMDControl.hpp"
#include "LMDCustomBevelPanel.hpp"
#include "LMDCustomControl.hpp"
#include "LMDCustomPanel.hpp"
#include "LMDSimplePanel.hpp"
#include "LMDBaseEdit.hpp"
#include "LMDCustomEdit.hpp"
#include "LMDCustomExtCombo.hpp"
#include "LMDCustomListComboBox.hpp"
#include "LMDCustomMaskEdit.hpp"
#include "LMDListComboBox.hpp"
#include "LMDMaskEdit.hpp"
#include "LMDBaseControl.hpp"
#include "LMDBaseGraphicControl.hpp"
#include "LMDBaseLabel.hpp"
#include "LMDCustomSimpleLabel.hpp"
#include "LMDSimpleLabel.hpp"
#include "LMDGraphicControl.hpp"
#include "LMDTechnicalLine.hpp"
#include "LMDButton.hpp"
#include "LMDCustomButton.hpp"
#include "LMDCustomLabel.hpp"
#include "LMDLabel.hpp"
#include "LMDCustomButtonGroup.hpp"
#include "LMDCustomGroupBox.hpp"
#include "LMDCustomParentPanel.hpp"
#include "LMDCustomRadioGroup.hpp"
#include "LMDRadioGroup.hpp"
//---------------------------------------------------------------------------
class TfrmResult : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TLMDSimplePanel *pnlTeam;
	TLMDSimplePanel *pnlButtons;
	TLMDSimplePanel *pnlClient;
	TLMDLabeledMaskEdit *edTeamnumber;
	TLMDLabeledListComboBox *cmbTeam;
	TLMDSimpleLabel *lblChanged;
	TLMDSimpleLabel *LMDSimpleLabel1;
	TLMDSimpleLabel *lblPlayer1;
	TLMDMaskEdit *edGameVR1_1_1;
	TLMDMaskEdit *edGameVR1_1_2;
	TLMDMaskEdit *edGameVR1_1_3;
	TLMDSimpleLabel *lblVorrunde1_1;
	TLMDSimpleLabel *LMDSimpleLabel3;
	TLMDMaskEdit *edGameVR2_1_1;
	TLMDMaskEdit *edGameVR2_1_2;
	TLMDMaskEdit *edGameVR2_1_3;
	TLMDSimpleLabel *lblVorrunde2_1;
	TLMDSimpleLabel *LMDSimpleLabel5;
	TLMDMaskEdit *edGameF_1_1;
	TLMDMaskEdit *edGameF_1_2;
	TLMDMaskEdit *edGameF_1_3;
	TLMDSimpleLabel *lblFinale_1;
	TLMDSimpleLabel *LMDSimpleLabel7;
	TLMDSimpleLabel *lblVorrunde_1;
	TLMDSimpleLabel *lblPlayer2;
	TLMDSimpleLabel *lblVorrunde1_2;
	TLMDSimpleLabel *lblVorrunde2_2;
	TLMDSimpleLabel *lblFinale_2;
	TLMDSimpleLabel *lblVorrunde_2;
	TLMDMaskEdit *edGameVR1_2_1;
	TLMDMaskEdit *edGameVR1_2_2;
	TLMDMaskEdit *edGameVR1_2_3;
	TLMDMaskEdit *edGameVR2_2_1;
	TLMDMaskEdit *edGameVR2_2_2;
	TLMDMaskEdit *edGameVR2_2_3;
	TLMDMaskEdit *edGameF_2_1;
	TLMDMaskEdit *edGameF_2_2;
	TLMDMaskEdit *edGameF_2_3;
	TLMDSimpleLabel *lblPlayer3;
	TLMDSimpleLabel *lblVorrunde1_3;
	TLMDSimpleLabel *lblVorrunde2_3;
	TLMDSimpleLabel *lblFinale_3;
	TLMDSimpleLabel *lblVorrunde_3;
	TLMDMaskEdit *edGameVR1_3_1;
	TLMDMaskEdit *edGameVR1_3_2;
	TLMDMaskEdit *edGameVR1_3_3;
	TLMDMaskEdit *edGameVR2_3_1;
	TLMDMaskEdit *edGameVR2_3_2;
	TLMDMaskEdit *edGameVR2_3_3;
	TLMDMaskEdit *edGameF_3_1;
	TLMDMaskEdit *edGameF_3_2;
	TLMDMaskEdit *edGameF_3_3;
	TLMDSimpleLabel *lblPlayer4;
	TLMDSimpleLabel *lblVorrunde1_4;
	TLMDSimpleLabel *lblVorrunde2_4;
	TLMDSimpleLabel *lblFinale_4;
	TLMDSimpleLabel *lblVorrunde_4;
	TLMDMaskEdit *edGameVR1_4_1;
	TLMDMaskEdit *edGameVR1_4_2;
	TLMDMaskEdit *edGameVR1_4_3;
	TLMDMaskEdit *edGameVR2_4_1;
	TLMDMaskEdit *edGameVR2_4_2;
	TLMDMaskEdit *edGameVR2_4_3;
	TLMDMaskEdit *edGameF_4_1;
	TLMDMaskEdit *edGameF_4_2;
	TLMDMaskEdit *edGameF_4_3;
	TLMDSimpleLabel *lblPlayer5;
	TLMDSimpleLabel *lblVorrunde1_5;
	TLMDSimpleLabel *lblVorrunde2_5;
	TLMDSimpleLabel *lblFinale_5;
	TLMDSimpleLabel *lblVorrunde_5;
	TLMDMaskEdit *edGameVR1_5_1;
	TLMDMaskEdit *edGameVR1_5_2;
	TLMDMaskEdit *edGameVR1_5_3;
	TLMDMaskEdit *edGameVR2_5_1;
	TLMDMaskEdit *edGameVR2_5_2;
	TLMDMaskEdit *edGameVR2_5_3;
	TLMDMaskEdit *edGameF_5_1;
	TLMDMaskEdit *edGameF_5_2;
	TLMDMaskEdit *edGameF_5_3;
	TLMDSimpleLabel *lblPlayer6;
	TLMDSimpleLabel *lblVorrunde1_6;
	TLMDSimpleLabel *lblVorrunde2_6;
	TLMDSimpleLabel *lblFinale_6;
	TLMDSimpleLabel *lblVorrunde_6;
	TLMDMaskEdit *edGameVR1_6_1;
	TLMDMaskEdit *edGameVR1_6_2;
	TLMDMaskEdit *edGameVR1_6_3;
	TLMDMaskEdit *edGameVR2_6_1;
	TLMDMaskEdit *edGameVR2_6_2;
	TLMDMaskEdit *edGameVR2_6_3;
	TLMDMaskEdit *edGameF_6_1;
	TLMDMaskEdit *edGameF_6_2;
	TLMDMaskEdit *edGameF_6_3;
	TLMDSimpleLabel *LMDSimpleLabel2;
	TLMDSimpleLabel *LMDSimpleLabel4;
	TLMDSimpleLabel *LMDSimpleLabel6;
	TLMDSimpleLabel *lblVR1_D1;
	TLMDSimpleLabel *lblVR1_D2;
	TLMDSimpleLabel *lblVR1_D3;
	TLMDSimpleLabel *lblVR1;
	TLMDSimpleLabel *lblVR2_D1;
	TLMDSimpleLabel *lblVR2_D2;
	TLMDSimpleLabel *lblVR2_D3;
	TLMDSimpleLabel *lblVR2;
	TLMDSimpleLabel *lblF_D1;
	TLMDSimpleLabel *lblF_D2;
	TLMDSimpleLabel *lblF_D3;
	TLMDSimpleLabel *lblF;
	TLMDSimpleLabel *lblVR;
	TLMDSimpleLabel *lblHDC1;
	TLMDSimpleLabel *lblHDC2;
	TLMDSimpleLabel *lblHDC3;
	TLMDSimpleLabel *lblHdcVR1;
	TLMDSimpleLabel *lblHDC4;
	TLMDSimpleLabel *lblHDC5;
	TLMDSimpleLabel *lblHDC6;
	TLMDSimpleLabel *lblHdcVR2;
	TLMDSimpleLabel *lblHdcF1;
	TLMDSimpleLabel *lblHdcF2;
	TLMDSimpleLabel *lblHdcF3;
	TLMDSimpleLabel *lblHdcF;
	TLMDSimpleLabel *lblHdcVR;
	TLMDSimpleLabel *lblAllVR1_D1;
	TLMDSimpleLabel *lblAllVR1_D2;
	TLMDSimpleLabel *lblAllVR1_D3;
	TLMDSimpleLabel *lblAllVR1;
	TLMDSimpleLabel *lblAllVR2_D1;
	TLMDSimpleLabel *lblAllVR2_D2;
	TLMDSimpleLabel *lblAllVR2_D3;
	TLMDSimpleLabel *lblAllVR2;
	TLMDSimpleLabel *lblAllF_D1;
	TLMDSimpleLabel *lblAllF_D2;
	TLMDSimpleLabel *lblAllF_D3;
	TLMDSimpleLabel *lblAllF;
	TLMDSimpleLabel *lblAllVR;
	TLMDTechnicalLine *LMDTechnicalLine1;
	TLMDTechnicalLine *LMDTechnicalLine2;
	TLMDTechnicalLine *LMDTechnicalLine3;
	TLMDTechnicalLine *LMDTechnicalLine4;
	TLMDButton *btnOK;
	TLMDButton *btnCancel;
	TLMDLabel *lblPlayerIdx1;
	TLMDLabel *lblPlayerIdx2;
	TLMDLabel *lblPlayerIdx3;
	TLMDLabel *lblPlayerIdx4;
	TLMDLabel *lblPlayerIdx5;
	TLMDLabel *lblPlayerIdx6;
	TLMDButton *btnClose;
	TLMDRadioGroup *rgInput;
	TLMDTechnicalLine *LMDTechnicalLine5;
	void __fastcall cmbTeamChange(TObject *Sender);
	void __fastcall edTeamnumberChange(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall btnOKClick(TObject *Sender);
	void __fastcall btnCancelClick(TObject *Sender);
	void __fastcall edGameVR1_1_1Enter(TObject *Sender);
	void __fastcall edGameVR1_2_1Enter(TObject *Sender);
	void __fastcall edGameVR1_3_1Enter(TObject *Sender);
	void __fastcall edGameVR1_4_1Enter(TObject *Sender);
	void __fastcall edGameVR1_5_1Enter(TObject *Sender);
	void __fastcall edGameVR1_6_1Enter(TObject *Sender);
	void __fastcall btnCloseClick(TObject *Sender);
	void __fastcall edGameVR1_1_1Exit(TObject *Sender);
	void __fastcall edGameVR1_1_2Exit(TObject *Sender);
	void __fastcall edGameVR1_1_3Exit(TObject *Sender);
	void __fastcall edGameVR2_1_1Exit(TObject *Sender);
	void __fastcall edGameVR2_1_2Exit(TObject *Sender);
	void __fastcall edGameVR2_1_3Exit(TObject *Sender);
	void __fastcall edGameF_1_1Exit(TObject *Sender);
	void __fastcall edGameF_1_2Exit(TObject *Sender);
	void __fastcall edGameF_1_3Exit(TObject *Sender);
	void __fastcall rgInputChange(TObject *Sender, int ButtonIndex);
	void __fastcall LMDSimpleLabel1Click(TObject *Sender);
private:	// Benutzer-Deklarationen
	bool isLoading;
	int FCurrentIdx;
	bool FModified;
//	TDBVEUCResultSumData ResultData;
	void __fastcall fillCombo();
	void __fastcall fillResult(int idx);
	void __fastcall fillPlayer(int idx);
	void __fastcall fillSum();
	void __fastcall fillPlayerSum(int idx);
	void __fastcall fillEdits();
	void __fastcall clearEdits();
	void __fastcall setChanged(TObject *Sender,bool Value);
public:		// Benutzer-Deklarationen
	__fastcall TfrmResult(TComponent* Owner);
	TModalResult __fastcall show(int team);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmResult *frmResult;
//---------------------------------------------------------------------------
#endif
