//---------------------------------------------------------------------------

#ifndef load_eucH
#define load_eucH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "LMDButton.hpp"
#include "LMDControl.hpp"
#include "LMDCustomBevelPanel.hpp"
#include "LMDCustomButton.hpp"
#include "LMDCustomControl.hpp"
#include "LMDCustomPanel.hpp"
#include "LMDSimplePanel.hpp"
#include "LMDBaseEdit.hpp"
#include "LMDCustomEdit.hpp"
#include "LMDCustomExtCombo.hpp"
#include "LMDCustomListComboBox.hpp"
#include "LMDCustomMaskEdit.hpp"
#include "LMDListComboBox.hpp"
//---------------------------------------------------------------------------
class TfrmLoadEUC : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TLMDSimplePanel *LMDSimplePanel1;
	TLMDButton *btnOK;
	TLMDButton *btnCancel;
	TLMDListComboBox *cmbEUC;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall btnCancelClick(TObject *Sender);
	void __fastcall btnOKClick(TObject *Sender);
	void __fastcall cmbEUCChange(TObject *Sender);
private:	// Benutzer-Deklarationen
public:		// Benutzer-Deklarationen
	__fastcall TfrmLoadEUC(TComponent* Owner);
	int SelectedIdx;
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmLoadEUC *frmLoadEUC;
//---------------------------------------------------------------------------
#endif
