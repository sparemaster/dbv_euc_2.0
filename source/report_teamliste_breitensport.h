//----------------------------------------------------------------------------
#ifndef report_teamliste_breitensportH
#define report_teamliste_breitensportH
//----------------------------------------------------------------------------
#include <DB.hpp>
#include <ADODB.hpp>
#include "DBVTypes.h"
#include <Classes.hpp>
#include <Controls.hpp>
#include <ExtCtrls.hpp>
#include "DBVEUCBaseComponent.h"
#include "DBVEUCTeam.h"
#include "QRCtrls.hpp"
#include "QRPDFFilt.hpp"
#include "QuickRpt.hpp"
//----------------------------------------------------------------------------
class TfrmAuswertung_Team_Breitensport : public TForm
{
__published:
        TQuickRep *qrpAuswertung;
	TQRBand *DetailBand1;
	TQRBand *PageFooterBand1;
	TQRSysData *QRSysData2;
        TQRSysData *QRSysData3;
        TQRDBText *QRDBText1;
        TQRDBText *netto;
        TQRDBText *brutto;
		TQRBand *QRBand1;
        TQRSysData *lblCaption;
        TQRLabel *QRLabel5;
        TQRLabel *lblStand;
        TQRGroup *gruppenkopf;
        TQRBand *gruppenfuss;
        TQRBand *QRBand2;
        TQRLabel *QRLabel1;
        TQRLabel *QRLabel2;
        TQRLabel *QRLabel3;
        TQRLabel *QRLabel4;
        TQRLabel *lblTitel;
        TQRLabel *QRLabel6;
        TQRLabel *lblHinweis;
	TADOQuery *qryTeams;
	TDBVEUCTeam *eucTeam;
	TADOTable *tblTarget;
	TAutoIncField *tblTargetidx;
	TIntegerField *tblTargetteam;
	TIntegerField *tblTargetNetto;
	TIntegerField *tblTargetBrutto;
	TIntegerField *tblTargetBestDurchgang;
	TBooleanField *tblTargetQualifiziert;
	TFloatField *tblTargetSchnitt;
	TADOQuery *qryTarget;
	TAutoIncField *qryTargetidx;
	TIntegerField *qryTargetteam;
	TIntegerField *qryTargetNetto;
	TIntegerField *qryTargetBrutto;
	TIntegerField *qryTargetBestDurchgang;
	TBooleanField *qryTargetQualifiziert;
	TFloatField *qryTargetSchnitt;
	TWideStringField *qryTargetName;
	TIntegerField *qryTargetNummer;
	TSmallintField *qryTargetHdc;
	TSmallintField *qryTargetHDC_Finale;
	TADOQuery *qryTarget2;
	TBooleanField *qryTargetAusland;
	TQRPDFFilter *QRPDFFilter1;
	TQRBand *QRBand3;
        void __fastcall DetailBand1BeforePrint(TQRCustomBand *Sender,
          bool &PrintBand);
        void __fastcall qrpAuswertungBeforePrint(TCustomQuickRep *Sender,
          bool &PrintReport);
	void __fastcall qryTeamsAfterScroll(TDataSet *DataSet);
	void __fastcall QRBand3BeforePrint(TQRCustomBand *Sender, bool &PrintBand);
	void __fastcall PageFooterBand1BeforePrint(TQRCustomBand *Sender,
          bool &PrintBand);
private:
	bool FComposite;
	bool HeaderPrinted;
	int counter;
	bool hdc_visible;
	bool champion_printed;
public:
	virtual __fastcall TfrmAuswertung_Team_Breitensport(TComponent* AOwner);
	void __fastcall PrepareData(bool Composite);
	void __fastcall CloseData();
};
//----------------------------------------------------------------------------
extern PACKAGE TfrmAuswertung_Team_Breitensport *frmAuswertung_Team_Breitensport;
//----------------------------------------------------------------------------
#endif    
