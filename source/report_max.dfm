object frmReportMax: TfrmReportMax
  Left = 0
  Top = 0
  Caption = 'frmReportMax'
  ClientHeight = 739
  ClientWidth = 833
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object QuickRep1: TQuickRep
    Left = 8
    Top = 8
    Width = 794
    Height = 1123
    DataSet = qryMax
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Continuous = False
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.OutputBin = Auto
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.UseStandardprinter = False
    PrinterSettings.UseCustomBinCode = False
    PrinterSettings.CustomBinCode = 0
    PrinterSettings.ExtendedDuplex = 0
    PrinterSettings.UseCustomPaperCode = False
    PrinterSettings.CustomPaperCode = 0
    PrinterSettings.PrintMetaFile = False
    PrinterSettings.PrintQuality = 0
    PrinterSettings.Collate = 0
    PrinterSettings.ColorOption = 0
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 100
    PrevFormStyle = fsNormal
    PreviewInitialState = wsMaximized
    PrevShowThumbs = False
    PrevShowSearch = False
    PrevInitialZoom = qrZoomToWidth
    PreviewDefaultSaveType = stPDF
    PreviewLeft = 0
    PreviewTop = 0
    object QRBand1: TQRBand
      Left = 38
      Top = 38
      Width = 718
      Height = 128
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        338.666666666666700000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageHeader
      object lblCaption: TQRSysData
        Left = 271
        Top = 4
        Width = 175
        Height = 38
        Size.Values = (
          100.541666666666700000
          717.020833333333300000
          10.583333333333330000
          463.020833333333300000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = True
        Color = clWhite
        Data = qrsReportTitle
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -32
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Text = ''
        Transparent = False
        ExportAs = exptText
        FontSize = 24
      end
      object lblSubcaption: TQRLabel
        Left = 305
        Top = 42
        Width = 107
        Height = 20
        Size.Values = (
          52.916666666666670000
          806.979166666666700000
          111.125000000000000000
          283.104166666666700000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = True
        Caption = 'lblSubcaption'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 12
      end
      object lblStand: TQRLabel
        Left = 327
        Top = 96
        Width = 63
        Height = 23
        Size.Values = (
          60.854166666666670000
          865.187500000000000000
          254.000000000000000000
          166.687500000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = True
        Caption = 'lblStand'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 12
      end
      object lblTitel: TQRLabel
        Left = 314
        Top = 70
        Width = 90
        Height = 23
        Size.Values = (
          60.854166666666670000
          830.791666666666700000
          185.208333333333300000
          238.125000000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = True
        Caption = 'hohe Spiele'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 12
      end
    end
    object gruppenkopf: TQRGroup
      Left = 38
      Top = 166
      Width = 718
      Height = 45
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      LinkBand = DetailBand1
      Size.Values = (
        119.062500000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      Expression = 'qryMax.Gruppe'
      FooterBand = gruppenfuss
      Master = QuickRep1
      ReprintOnNewPage = True
      object QRLabel3: TQRLabel
        Left = 463
        Top = 22
        Width = 37
        Height = 17
        Size.Values = (
          44.979166666666670000
          1225.020833333333000000
          58.208333333333330000
          97.895833333333330000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Team'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel1: TQRLabel
        Left = 18
        Top = 24
        Width = 38
        Height = 17
        Size.Values = (
          44.979166666666670000
          47.625000000000000000
          63.500000000000000000
          100.541666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        Caption = 'Name'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRDBText12: TQRDBText
        Left = 18
        Top = 3
        Width = 58
        Height = 20
        Size.Values = (
          52.916666666666670000
          47.625000000000000000
          7.937500000000000000
          153.458333333333300000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'Gruppe'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 12
      end
      object QRLabel2: TQRLabel
        Left = 150
        Top = 22
        Width = 103
        Height = 17
        Frame.DrawLeft = True
        Frame.DrawRight = True
        Size.Values = (
          44.979166666666670000
          396.875000000000000000
          58.208333333333330000
          272.520833333333300000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Vorrunde 1'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel4: TQRLabel
        Left = 254
        Top = 22
        Width = 99
        Height = 17
        Size.Values = (
          44.979166666666670000
          672.041666666666700000
          58.208333333333330000
          261.937500000000000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Vorrunde 2'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel7: TQRLabel
        Left = 354
        Top = 22
        Width = 103
        Height = 17
        Frame.DrawLeft = True
        Frame.DrawRight = True
        Size.Values = (
          44.979166666666670000
          936.625000000000000000
          58.208333333333330000
          272.520833333333300000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        AutoSize = False
        Caption = 'Finale'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
    end
    object DetailBand1: TQRBand
      Left = 38
      Top = 211
      Width = 718
      Height = 24
      AlignToBottom = False
      BeforePrint = DetailBand1BeforePrint
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        63.500000000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbDetail
      object QRDBText1: TQRDBText
        Left = 18
        Top = 4
        Width = 119
        Height = 17
        Size.Values = (
          44.979166666666670000
          47.625000000000000000
          10.583333333333330000
          314.854166666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'Spielername'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRDBText2: TQRDBText
        Left = 150
        Top = 4
        Width = 35
        Height = 17
        Size.Values = (
          44.979166666666670000
          396.875000000000000000
          10.583333333333330000
          92.604166666666670000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'spiel1'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRDBText3: TQRDBText
        Left = 184
        Top = 4
        Width = 35
        Height = 17
        Size.Values = (
          44.979166666666670000
          486.833333333333300000
          10.583333333333330000
          92.604166666666670000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'spiel2'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRDBText4: TQRDBText
        Left = 218
        Top = 4
        Width = 35
        Height = 17
        Size.Values = (
          44.979166666666670000
          576.791666666666700000
          10.583333333333330000
          92.604166666666670000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'spiel3'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRDBText5: TQRDBText
        Left = 252
        Top = 4
        Width = 35
        Height = 17
        Size.Values = (
          44.979166666666670000
          666.750000000000000000
          10.583333333333330000
          92.604166666666670000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'spiel4'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRDBText6: TQRDBText
        Left = 286
        Top = 4
        Width = 35
        Height = 17
        Size.Values = (
          44.979166666666670000
          756.708333333333300000
          10.583333333333330000
          92.604166666666670000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'spiel5'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRDBText7: TQRDBText
        Left = 320
        Top = 4
        Width = 35
        Height = 17
        Size.Values = (
          44.979166666666670000
          846.666666666666700000
          10.583333333333330000
          92.604166666666670000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'spiel6'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRDBText8: TQRDBText
        Left = 354
        Top = 4
        Width = 35
        Height = 17
        Size.Values = (
          44.979166666666670000
          936.625000000000000000
          10.583333333333330000
          92.604166666666670000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'spiel7'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRDBText9: TQRDBText
        Left = 388
        Top = 4
        Width = 35
        Height = 17
        Size.Values = (
          44.979166666666670000
          1026.583333333333000000
          10.583333333333330000
          92.604166666666670000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'spiel8'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRDBText10: TQRDBText
        Left = 422
        Top = 4
        Width = 35
        Height = 17
        Size.Values = (
          44.979166666666670000
          1116.541666666667000000
          10.583333333333330000
          92.604166666666670000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'spiel9'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
      object QRDBText11: TQRDBText
        Left = 463
        Top = 4
        Width = 170
        Height = 17
        Size.Values = (
          44.979166666666670000
          1225.020833333333000000
          10.583333333333330000
          449.791666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        AutoSize = False
        Color = clWhite
        DataSet = qryMax
        DataField = 'Name'
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FullJustify = False
        MaxBreakChars = 0
        FontSize = 10
      end
    end
    object gruppenfuss: TQRBand
      Left = 38
      Top = 235
      Width = 718
      Height = 24
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        63.500000000000000000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbGroupFooter
    end
    object PageFooterBand1: TQRBand
      Left = 38
      Top = 259
      Width = 718
      Height = 70
      AlignToBottom = False
      Color = clWhite
      TransparentBand = False
      ForceNewColumn = False
      ForceNewPage = False
      Size.Values = (
        185.208333333333300000
        1899.708333333333000000)
      PreCaluculateBandHeight = False
      KeepOnOnePage = False
      BandType = rbPageFooter
      object QRSysData2: TQRSysData
        Left = 8
        Top = 51
        Width = 80
        Height = 17
        Size.Values = (
          44.979166666666670000
          21.166666666666670000
          134.937500000000000000
          211.666666666666700000)
        XLColumn = 0
        Alignment = taLeftJustify
        AlignToBand = False
        Color = clWhite
        Data = qrsPageNumber
        Text = 'Seite '
        Transparent = False
        ExportAs = exptText
        FontSize = 10
      end
      object QRLabel5: TQRLabel
        Left = 540
        Top = 51
        Width = 178
        Height = 17
        Size.Values = (
          44.979166666666670000
          1428.750000000000000000
          134.937500000000000000
          470.958333333333300000)
        XLColumn = 0
        Alignment = taRightJustify
        AlignToBand = True
        Caption = '(c) 2003-2014 Michael Kieser  '
        Color = clWhite
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
      object QRLabel6: TQRLabel
        Left = 99
        Top = 26
        Width = 520
        Height = 17
        Size.Values = (
          44.979166666666670000
          261.937500000000000000
          68.791666666666670000
          1375.833333333333000000)
        XLColumn = 0
        Alignment = taCenter
        AlignToBand = True
        Caption = 
          'Ergebnisse gibt'#39's hier: www.dbv-bowling.de - alle Auswertungen z' +
          'um herunterladen'
        Color = clWhite
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold, fsItalic]
        ParentFont = False
        Transparent = False
        ExportAs = exptText
        WrapStyle = BreakOnSpaces
        FontSize = 10
      end
    end
  end
  object qryMax: TADOQuery
    AutoCalcFields = False
    Connection = dmMain.conMain
    CursorType = ctStatic
    Filtered = True
    Parameters = <
      item
        Name = 'TurnierIdx'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 510
        Value = '23'
      end>
    SQL.Strings = (
      
        'SELECT ergebnis.Turnier, ergebnis.spiel1, ergebnis.spiel2, ergeb' +
        'nis.spiel3, ergebnis.spiel4, ergebnis.spiel5, ergebnis.spiel6, e' +
        'rgebnis.spiel7, ergebnis.spiel8, ergebnis.spiel9, spieler.Nachna' +
        'me, spieler.Vorname, spieler.Gruppe, teams.Name, teams.Nummer, (' +
        'spieler.Vorname+" "+spieler.Nachname) as Spielername'
      
        'FROM teams INNER JOIN (spieler INNER JOIN ergebnis ON spieler.Sp' +
        'ielerIdx = ergebnis.Spieler) ON teams.TeamIdx = spieler.Team'
      'WHERE (ergebnis.Turnier=:TurnierIdx) AND (('
      '      ((ergebnis.spiel1>=230) AND (spieler.Gruppe="Damen"))'
      '   OR ((ergebnis.spiel2>=230) AND (spieler.Gruppe="Damen"))'
      '   OR ((ergebnis.spiel3>=230) AND (spieler.Gruppe="Damen"))'
      '   OR ((ergebnis.spiel4>=230) AND (spieler.Gruppe="Damen"))'
      '   OR ((ergebnis.spiel5>=230) AND (spieler.Gruppe="Damen"))'
      '   OR ((ergebnis.spiel6>=230) AND (spieler.Gruppe="Damen"))'
      '   OR ((ergebnis.spiel7>=230) AND (spieler.Gruppe="Damen"))'
      '   OR ((ergebnis.spiel8>=230) AND (spieler.Gruppe="Damen"))'
      '   OR ((ergebnis.spiel9>=230) AND (spieler.Gruppe="Damen")))'
      '   OR ('
      ''
      '      ((ergebnis.spiel1>=260) AND (spieler.Gruppe="Herren"))'
      '   OR ((ergebnis.spiel2>=260) AND (spieler.Gruppe="Herren"))'
      '   OR ((ergebnis.spiel3>=260) AND (spieler.Gruppe="Herren"))'
      '   OR ((ergebnis.spiel4>=260) AND (spieler.Gruppe="Herren"))'
      '   OR ((ergebnis.spiel5>=260) AND (spieler.Gruppe="Herren"))'
      '   OR ((ergebnis.spiel6>=260) AND (spieler.Gruppe="Herren"))'
      '   OR ((ergebnis.spiel7>=260) AND (spieler.Gruppe="Herren"))'
      '   OR ((ergebnis.spiel8>=260) AND (spieler.Gruppe="Herren"))'
      '   OR ((ergebnis.spiel9>=260) AND (spieler.Gruppe="Herren"))))'
      ''
      ''
      'ORDER BY spieler.Gruppe ASC')
    Left = 32
    Top = 40
    object qryMaxTurnier: TIntegerField
      FieldName = 'Turnier'
    end
    object qryMaxspiel1: TSmallintField
      FieldName = 'spiel1'
    end
    object qryMaxspiel2: TSmallintField
      FieldName = 'spiel2'
    end
    object qryMaxspiel3: TSmallintField
      FieldName = 'spiel3'
    end
    object qryMaxspiel4: TIntegerField
      FieldName = 'spiel4'
    end
    object qryMaxspiel5: TIntegerField
      FieldName = 'spiel5'
    end
    object qryMaxspiel6: TIntegerField
      FieldName = 'spiel6'
    end
    object qryMaxspiel7: TIntegerField
      FieldName = 'spiel7'
    end
    object qryMaxspiel8: TIntegerField
      FieldName = 'spiel8'
    end
    object qryMaxspiel9: TIntegerField
      FieldName = 'spiel9'
    end
    object qryMaxNachname: TWideStringField
      FieldName = 'Nachname'
    end
    object qryMaxVorname: TWideStringField
      FieldName = 'Vorname'
    end
    object qryMaxGruppe: TWideStringField
      FieldName = 'Gruppe'
      Size = 10
    end
    object qryMaxName: TWideStringField
      FieldName = 'Name'
      Size = 50
    end
    object qryMaxNummer: TIntegerField
      FieldName = 'Nummer'
    end
    object qryMaxSpielername: TWideStringField
      FieldName = 'Spielername'
      ReadOnly = True
      Size = 255
    end
  end
  object QRPDFFilter1: TQRPDFFilter
    CompressionOn = False
    TextEncoding = AnsiEncoding
    Codepage = '1252'
    Left = 94
    Top = 46
  end
end
