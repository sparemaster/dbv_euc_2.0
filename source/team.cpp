//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "team.h"
#include "datamodule.h"
#include "DBVRoutines.h"
#include "spieler.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "LMDButton"
#pragma link "LMDControl"
#pragma link "LMDCustomBevelPanel"
#pragma link "LMDCustomButton"
#pragma link "LMDCustomControl"
#pragma link "LMDCustomPanel"
#pragma link "LMDSimplePanel"
#pragma link "LMDBaseEdit"
#pragma link "LMDCustomEdit"
#pragma link "LMDCustomGroupBox"
#pragma link "LMDCustomMaskEdit"
#pragma link "LMDCustomParentPanel"
#pragma link "LMDGroupBox"
#pragma link "LMDMaskEdit"
#pragma link "LMDCustomExtCombo"
#pragma link "LMDCustomListComboBox"
#pragma link "LMDEdit"
#pragma link "LMDListComboBox"
#pragma link "LMDButtonControl"
#pragma link "LMDCheckBox"
#pragma link "LMDCustomCheckBox"
#pragma link "LMDBaseControl"
#pragma link "LMDBaseGraphicControl"
#pragma link "LMDBaseLabel"
#pragma link "LMDCustomSimpleLabel"
#pragma link "LMDSimpleLabel"
#pragma link "LMDBaseGraphicButton"
#pragma link "LMDCustomSpeedButton"
#pragma link "LMDSpeedButton"
#pragma resource "*.dfm"
TfrmTeam *frmTeam;
//---------------------------------------------------------------------------
__fastcall TfrmTeam::TfrmTeam(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::btnOKClick(TObject *Sender)
{
	if (changed) {
//		Edit2Data();
		dmMain->eucTeam->PlayerSort=psAverage;
		dmMain->eucTeam->writeToDB(dmMain->eucTeam->TeamIdx);
		dmMain->eucTeam->PlayerSort=psName;
		setChanged(false);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::btnCancelClick(TObject *Sender)
{
    dmMain->eucTeam->readFromDB(dmMain->eucTeam->TeamIdx);
	Data2Edit();
	setChanged(false);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::Data2Edit()
{
	btnAdd1->Enabled=true;btnDelete1->Enabled=false;
	btnAdd2->Enabled=true;btnDelete2->Enabled=false;
	btnAdd3->Enabled=true;btnDelete3->Enabled=false;
	btnAdd4->Enabled=true;btnDelete4->Enabled=false;
	btnAdd5->Enabled=true;btnDelete5->Enabled=false;
	btnAdd6->Enabled=true;btnDelete6->Enabled=false;

	edNumber->Text=IntToStr(dmMain->eucTeam->Teamnummer);
	edName->Text=dmMain->eucTeam->Teamname;
	cbForeigner->Checked=dmMain->eucTeam->Ausland;
	cmbCenter->ItemIndex=getCenterItemIndex(cmbCenter->Items,dmMain->eucTeam->Anlage);
	cmbGroup->ItemIndex=cmbGroup->Items->IndexOfObject((TObject*)dmMain->eucTeam->Startgruppe.ToInt());
	edHDC->Text=dmMain->eucTeam->TeamHdc;

	//Spielerdaten
	TLMDMaskEdit *nr,*dbv,*dbu,*bsv,*fbv,*eff;
	TLMDEdit *vn, *nn;
	TLMDSimpleLabel *lbl;
	TLMDSpeedButton *btnDel,*btnAdd;
	TLMDCheckBox *gender;
	dmMain->eucTeam->FirstPlayer();
	int i=1;
	bool next=true;
	while (dmMain->eucTeam->CurrentPlayer!=NULL && next) {
		switch (i) {
			case 1: nr=edNumber1;dbv=edDBV1;dbu=edDBU1;bsv=edBSV1;fbv=edFBV1;eff=edEff1;vn=edVorname1;nn=edNachname1;lbl=lblIdx1;btnDel=btnDelete1;btnAdd=btnAdd1;gender=cbHerren1;break;
			case 2: nr=edNumber2;dbv=edDBV2;dbu=edDBU2;bsv=edBSV2;fbv=edFBV2;eff=edEff2;vn=edVorname2;nn=edNachname2;lbl=lblIdx2;btnDel=btnDelete2;btnAdd=btnAdd2;gender=cbHerren2;break;
			case 3: nr=edNumber3;dbv=edDBV3;dbu=edDBU3;bsv=edBSV3;fbv=edFBV3;eff=edEff3;vn=edVorname3;nn=edNachname3;lbl=lblIdx3;btnDel=btnDelete3;btnAdd=btnAdd3;gender=cbHerren3;break;
			case 4: nr=edNumber4;dbv=edDBV4;dbu=edDBU4;bsv=edBSV4;fbv=edFBV4;eff=edEff4;vn=edVorname4;nn=edNachname4;lbl=lblIdx4;btnDel=btnDelete4;btnAdd=btnAdd4;gender=cbHerren4;break;
			case 5: nr=edNumber5;dbv=edDBV5;dbu=edDBU5;bsv=edBSV5;fbv=edFBV5;eff=edEff5;vn=edVorname5;nn=edNachname5;lbl=lblIdx5;btnDel=btnDelete5;btnAdd=btnAdd5;gender=cbHerren5;break;
			case 6: nr=edNumber6;dbv=edDBV6;dbu=edDBU6;bsv=edBSV6;fbv=edFBV6;eff=edEff6;vn=edVorname6;nn=edNachname6;lbl=lblIdx6;btnDel=btnDelete6;btnAdd=btnAdd6;gender=cbHerren6;break;
		default:
			;
		}
		nr->Text=IntToStr(dmMain->eucTeam->CurrentPlayer->mnummer);
		dbv->Text=FormatFloat("##0.00",dmMain->eucTeam->CurrentPlayer->schnitt_dbv);
		fbv->Text=FormatFloat("##0.00",dmMain->eucTeam->CurrentPlayer->schnitt_fbv);
		dbu->Text=FormatFloat("##0.00",dmMain->eucTeam->CurrentPlayer->schnitt_dbu);
		bsv->Text=FormatFloat("##0.00",dmMain->eucTeam->CurrentPlayer->schnitt_bsv);
		eff->Text=FormatFloat("##0.00",dmMain->eucTeam->CurrentPlayer->schnitt_eff);
		vn->Text=dmMain->eucTeam->CurrentPlayer->vorname;
		nn->Text=dmMain->eucTeam->CurrentPlayer->nachname;
		lbl->Caption=IntToStr(dmMain->eucTeam->CurrentPlayer->idx);
		btnDel->Enabled=(nr->Text!="") && (nr->Text!="0");
		btnAdd->Enabled=!btnDel->Enabled;
		gender->Checked=dmMain->eucTeam->CurrentPlayer->gruppe=="Herren";

		i++;
		next=dmMain->eucTeam->NextPlayer();
    }
	setChanged(false);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::Edit2Data()
{
	TDBVEUCTeam *team=dmMain->eucTeam;
	TDBVEUCPlayer *player;

	//team->Teamnummer=edNumber->Text.ToInt();
	team->Teamname=edName->Text;
	team->Startgruppe=(int)cmbGroup->Items->Objects[cmbGroup->ItemIndex];
	team->Ausland=cbForeigner->Checked;
	TDBVCenterObject *a=(TDBVCenterObject*)cmbCenter->Items->Objects[cmbCenter->ItemIndex];
	team->Anlage=a->Anlagennummer;

	//Spielerdaten
	team->clearPlayerList();
	if ((edNumber1->Text!="0") || (cbForeigner->Checked && !edNachname1->Text.IsEmpty())) {
		if (!team->existPlayer(edNumber1->Text.ToInt())) {
			if (team->readPlayerDataFromMV(edNumber1->Text.ToInt())) {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber1->Text.ToInt();
				p->schnitt_bsv=edBSV1->Text.ToDouble();
				p->schnitt_dbu=edDBU1->Text.ToDouble();
				p->schnitt_dbv=edDBV1->Text.ToDouble();
				p->schnitt_fbv=edFBV1->Text.ToDouble();
				team->addPlayer(p);
			}
			else {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber1->Text.ToInt();
				p->vorname=edVorname1->Text;
				p->nachname=edNachname1->Text;
				if (cbHerren1->Checked) {
					p->gruppe="Herren";
				}
				else {
					p->gruppe="Damen";
				}
				p->idx=lblIdx1->Caption.ToInt();
				p->schnitt_bsv=edBSV1->Text.ToDouble();
				p->schnitt_dbu=edDBU1->Text.ToDouble();
				p->schnitt_dbv=edDBV1->Text.ToDouble();
				p->schnitt_fbv=edFBV1->Text.ToDouble();
				team->addPlayer(p);
			}
		}
	}
	//Spieler2
	if ((edNumber2->Text!="0") || (cbForeigner->Checked && !edNachname2->Text.IsEmpty())) {
		if (!team->existPlayer(edNumber2->Text.ToInt())) {
			if (team->readPlayerDataFromMV(edNumber2->Text.ToInt())) {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber2->Text.ToInt();
				p->schnitt_bsv=edBSV2->Text.ToDouble();
				p->schnitt_dbu=edDBU2->Text.ToDouble();
				p->schnitt_dbv=edDBV2->Text.ToDouble();
				p->schnitt_fbv=edFBV2->Text.ToDouble();
				team->addPlayer(p);
			}
			else {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber2->Text.ToInt();
				p->vorname=edVorname2->Text;
				p->nachname=edNachname2->Text;
				if (cbHerren2->Checked) {
					p->gruppe="Herren";
				}
				else {
					p->gruppe="Damen";
				}
				p->idx=lblIdx2->Caption.ToInt();
				p->schnitt_bsv=edBSV2->Text.ToDouble();
				p->schnitt_dbu=edDBU2->Text.ToDouble();
				p->schnitt_dbv=edDBV2->Text.ToDouble();
				p->schnitt_fbv=edFBV2->Text.ToDouble();
				team->addPlayer(p);
			}
		}
	}
	//Spieler3
	if ((edNumber3->Text!="0") || (cbForeigner->Checked && !edNachname3->Text.IsEmpty())) {
		if (!team->existPlayer(edNumber3->Text.ToInt())) {
			if (team->readPlayerDataFromMV(edNumber3->Text.ToInt())) {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber3->Text.ToInt();
				p->schnitt_bsv=edBSV3->Text.ToDouble();
				p->schnitt_dbu=edDBU3->Text.ToDouble();
				p->schnitt_dbv=edDBV3->Text.ToDouble();
				p->schnitt_fbv=edFBV3->Text.ToDouble();
				team->addPlayer(p);
			}
			else {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber3->Text.ToInt();
				p->vorname=edVorname3->Text;
				p->nachname=edNachname3->Text;
				if (cbHerren3->Checked) {
					p->gruppe="Herren";
				}
				else {
					p->gruppe="Damen";
				}
				p->idx=lblIdx3->Caption.ToInt();
				p->schnitt_bsv=edBSV3->Text.ToDouble();
				p->schnitt_dbu=edDBU3->Text.ToDouble();
				p->schnitt_dbv=edDBV3->Text.ToDouble();
				p->schnitt_fbv=edFBV3->Text.ToDouble();
				team->addPlayer(p);
			}
		}
	}
	//Spieler4
	if ((edNumber4->Text!="0") || (cbForeigner->Checked && !edNachname4->Text.IsEmpty())) {
		if (!team->existPlayer(edNumber4->Text.ToInt())) {
			if (team->readPlayerDataFromMV(edNumber4->Text.ToInt())) {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber4->Text.ToInt();
				p->schnitt_bsv=edBSV4->Text.ToDouble();
				p->schnitt_dbu=edDBU4->Text.ToDouble();
				p->schnitt_dbv=edDBV4->Text.ToDouble();
				p->schnitt_fbv=edFBV4->Text.ToDouble();
				team->addPlayer(p);
			}
			else {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber4->Text.ToInt();
				p->vorname=edVorname4->Text;
				p->nachname=edNachname4->Text;
				if (cbHerren4->Checked) {
					p->gruppe="Herren";
				}
				else {
					p->gruppe="Damen";
				}
				p->idx=lblIdx4->Caption.ToInt();
				p->schnitt_bsv=edBSV4->Text.ToDouble();
				p->schnitt_dbu=edDBU4->Text.ToDouble();
				p->schnitt_dbv=edDBV4->Text.ToDouble();
				p->schnitt_fbv=edFBV4->Text.ToDouble();
				team->addPlayer(p);
			}
		}
	}
	//Spieler5
	if ((edNumber5->Text!="0") || (cbForeigner->Checked && !edNachname5->Text.IsEmpty())) {
		if (!team->existPlayer(edNumber5->Text.ToInt())) {
			if (team->readPlayerDataFromMV(edNumber5->Text.ToInt())) {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber5->Text.ToInt();
				p->schnitt_bsv=edBSV5->Text.ToDouble();
				p->schnitt_dbu=edDBU5->Text.ToDouble();
				p->schnitt_dbv=edDBV5->Text.ToDouble();
				p->schnitt_fbv=edFBV5->Text.ToDouble();
				team->addPlayer(p);
			}
			else {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber5->Text.ToInt();
				p->vorname=edVorname5->Text;
				p->nachname=edNachname5->Text;
				if (cbHerren5->Checked) {
					p->gruppe="Herren";
				}
				else {
					p->gruppe="Damen";
				}
				p->idx=lblIdx5->Caption.ToInt();
				p->schnitt_bsv=edBSV5->Text.ToDouble();
				p->schnitt_dbu=edDBU5->Text.ToDouble();
				p->schnitt_dbv=edDBV5->Text.ToDouble();
				p->schnitt_fbv=edFBV5->Text.ToDouble();
				team->addPlayer(p);
			}
		}
	}
	//Spieler6
	if ((edNumber6->Text!="0") || (cbForeigner->Checked && !edNachname6->Text.IsEmpty())) {
		if (!team->existPlayer(edNumber6->Text.ToInt())) {
			if (team->readPlayerDataFromMV(edNumber6->Text.ToInt())) {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber6->Text.ToInt();
				p->schnitt_bsv=edBSV6->Text.ToDouble();
				p->schnitt_dbu=edDBU6->Text.ToDouble();
				p->schnitt_dbv=edDBV6->Text.ToDouble();
				p->schnitt_fbv=edFBV6->Text.ToDouble();
				team->addPlayer(p);
			}
			else {
				TDBVEUCPlayerClass *p=new TDBVEUCPlayerClass();
				p->mnummer=edNumber6->Text.ToInt();
				p->vorname=edVorname6->Text;
				p->nachname=edNachname6->Text;
				if (cbHerren6->Checked) {
					p->gruppe="Herren";
				}
				else {
					p->gruppe="Damen";
				}
				p->idx=lblIdx6->Caption.ToInt();
				p->schnitt_bsv=edBSV6->Text.ToDouble();
				p->schnitt_dbu=edDBU6->Text.ToDouble();
				p->schnitt_dbv=edDBV6->Text.ToDouble();
				p->schnitt_fbv=edFBV6->Text.ToDouble();
				team->addPlayer(p);
			}
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::FormShow(TObject *Sender)
{
	fillCenterList(dmMain->qryCenter,cmbCenter->Items,"","");

	cmbGroup->Items->Clear();
	dmMain->qryGroup->Open();
	while (!dmMain->qryGroup->Eof) {
		cmbGroup->Items->AddObject(dmMain->qryGroup->FieldByName("Gruppe")->AsString,
								   (TObject*)dmMain->qryGroup->FieldByName("Idx")->AsInteger);
		dmMain->qryGroup->Next();
	}
	dmMain->qryGroup->Close();

	changed=false;
	FLoading=true;
	clearEdits();
	Data2Edit();
	FLoading=false;
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::clearEdits()
{
	edNumber->Text="";
	edName->Text="";
	cmbGroup->ItemIndex=-1;
	cmbCenter->ItemIndex=-1;
	cbForeigner->Checked=false;

	edNumber1->Text="";edNachname1->Text="";edVorname1->Text="";edDBV1->Text="";
	edFBV1->Text="";edDBU1->Text="";edBSV1->Text="";edEff1->Text="";
	edNumber2->Text="";edNachname2->Text="";edVorname2->Text="";edDBV2->Text="";
	edFBV2->Text="";edDBU2->Text="";edBSV2->Text="";edEff2->Text="";
	edNumber3->Text="";edNachname3->Text="";edVorname3->Text="";edDBV3->Text="";
	edFBV3->Text="";edDBU3->Text="";edBSV3->Text="";edEff3->Text="";
	edNumber4->Text="";edNachname4->Text="";edVorname4->Text="";edDBV4->Text="";
	edFBV4->Text="";edDBU4->Text="";edBSV4->Text="";edEff4->Text="";
	edNumber5->Text="";edNachname5->Text="";edVorname5->Text="";edDBV5->Text="";
	edFBV5->Text="";edDBU5->Text="";edBSV5->Text="";edEff5->Text="";
	edNumber6->Text="";edNachname6->Text="";edVorname6->Text="";edDBV6->Text="";
	edFBV6->Text="";edDBU6->Text="";edBSV6->Text="";edEff6->Text="";

	lblIdx1->Caption="0";
	lblIdx2->Caption="0";
	lblIdx3->Caption="0";
	lblIdx4->Caption="0";
	lblIdx5->Caption="0";
	lblIdx6->Caption="0";
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::setChanged(bool isChanged)
{
	changed=isChanged;
	btnOK->Enabled=isChanged;
	btnCancel->Enabled=isChanged;
	btnClose->Enabled=!isChanged;
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::btnCloseClick(TObject *Sender)
{
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::selectPlayer(int idx)
{
	TLMDMaskEdit *edNr=NULL;
	switch (idx) {
		case 1:edNr=edNumber1;break;
		case 2:edNr=edNumber2;break;
		case 3:edNr=edNumber3;break;
		case 4:edNr=edNumber4;break;
		case 5:edNr=edNumber5;break;
		case 6:edNr=edNumber6;break;
	default:
		;
	}
	if (edNr!=NULL) {
		CurrentPlayer=dmMain->eucTeam->getPlayer(edNr->Text.ToInt());
	}
	else {
        CurrentPlayer=NULL;
    }
}
//---------------------------------------------------------------------------

TLMDMaskEdit* __fastcall TfrmTeam::getEffField(int idx)
{
	TLMDMaskEdit *edEff;
	switch (idx) {
		case 1:edEff=edEff1;break;
		case 2:edEff=edEff2;break;
		case 3:edEff=edEff3;break;
		case 4:edEff=edEff4;break;
		case 5:edEff=edEff5;break;
		case 6:edEff=edEff6;break;
	default:
		;
	}
	return edEff;
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::btnDelete1Click(TObject *Sender)
{
	if (MessageDlg("Soll der Spieler wirklich gel�scht werden?",mtConfirmation,TMsgDlgButtons()<<mbYes<<mbNo,0)==mrYes) {
		TLMDSpeedButton *b=(TLMDSpeedButton*)Sender;
		selectPlayer(b->Tag);
		dmMain->eucTeam->deletePlayer(CurrentPlayer->idx);
		FLoading=true;
		clearEdits();
		Data2Edit();
		FLoading=false;
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::edNachname1Enter(TObject *Sender)
{
	selectPlayer(((TControl*)Sender)->Tag);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::edNachname1Change(TObject *Sender)
{
	if (FLoading) {
		return;
	}
	CurrentPlayer->nachname=((TLMDEdit*)Sender)->Text;
	setChanged(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::edVorname1Change(TObject *Sender)
{
	if (FLoading) {
		return;
	}
	CurrentPlayer->vorname=((TLMDEdit*)Sender)->Text;
	setChanged(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::edDBV1Change(TObject *Sender)
{
	if (FLoading) {
		return;
	}
	CurrentPlayer->schnitt_dbv=((TLMDMaskEdit*)Sender)->Text.ToDouble();
	getEffField(((TControl*)Sender)->Tag)->Text=CurrentPlayer->schnitt_eff;
	setChanged(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::edFBV1Change(TObject *Sender)
{
	if (FLoading) {
		return;
	}
	CurrentPlayer->schnitt_fbv=((TLMDMaskEdit*)Sender)->Text.ToDouble();
	getEffField(((TControl*)Sender)->Tag)->Text=CurrentPlayer->schnitt_eff;
	setChanged(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::edDBU1Change(TObject *Sender)
{
	if (FLoading) {
		return;
	}
	CurrentPlayer->schnitt_dbu=((TLMDMaskEdit*)Sender)->Text.ToDouble();
	getEffField(((TControl*)Sender)->Tag)->Text=CurrentPlayer->schnitt_eff;
	setChanged(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::edBSV1Change(TObject *Sender)
{
	if (FLoading) {
		return;
	}
	CurrentPlayer->schnitt_bsv=((TLMDMaskEdit*)Sender)->Text.ToDouble();
	getEffField(((TControl*)Sender)->Tag)->Text=CurrentPlayer->schnitt_eff;
	setChanged(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::edHDCChange(TObject *Sender)
{
	if (FLoading) {
		return;
	}
	dmMain->eucTeam->TeamHdc=edHDC->Text.ToInt();
	setChanged(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::btnHDCClick(TObject *Sender)
{
	dmMain->eucTeam->AllowHDCCalc=true;
	dmMain->eucTeam->calcHdc();
	dmMain->eucTeam->AllowHDCCalc=false;
	edHDC->Text=dmMain->eucTeam->TeamHdc;
	setChanged(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::btnAdd1Click(TObject *Sender)
{
	TModalResult ret=frmSpieler->ShowModal();
	if (ret==mrOk) {
		FLoading=true;
		clearEdits();
		Data2Edit();
		FLoading=false;
	}
	setChanged(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::cbHerren1Click(TObject *Sender)
{
	TLMDCheckBox* cb=(TLMDCheckBox*)Sender;
	selectPlayer(cb->Tag);
	if (cb->Checked) {
		CurrentPlayer->gruppe="Herren";
	}
	else {
		CurrentPlayer->gruppe="Damen";
	}
	setChanged(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::edNameChange(TObject *Sender)
{
	if (FLoading) {
		return;
	}
	dmMain->eucTeam->Teamname=edName->Text;
	setChanged(true);
}
//---------------------------------------------------------------------------

void __fastcall TfrmTeam::cbForeignerChange(TObject *Sender)
{
	if (FLoading) {
		return;
	}
	dmMain->eucTeam->Ausland=cbForeigner->Checked;
	setChanged(true);
}
//---------------------------------------------------------------------------

