//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "euc.h"
#include "datamodule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "LMDButton"
#pragma link "LMDControl"
#pragma link "LMDCustomBevelPanel"
#pragma link "LMDCustomButton"
#pragma link "LMDCustomControl"
#pragma link "LMDCustomPanel"
#pragma link "LMDSimplePanel"
#pragma link "LMDBaseEdit"
#pragma link "LMDCustomEdit"
#pragma link "LMDEdit"
#pragma resource "*.dfm"
TfrmEUC *frmEUC;
//---------------------------------------------------------------------------
__fastcall TfrmEUC::TfrmEUC(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmEUC::FormShow(TObject *Sender)
{
	Data2Edit();
}
//---------------------------------------------------------------------------

void __fastcall TfrmEUC::btnOKClick(TObject *Sender)
{
	ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TfrmEUC::btnCancelClick(TObject *Sender)
{
	ModalResult=mrCancel;
}
//---------------------------------------------------------------------------

void __fastcall TfrmEUC::Edit2Data()
{

}
//---------------------------------------------------------------------------

void __fastcall TfrmEUC::Data2Edit()
{
	edName1->Text=dmMain->eucEUC->Name1;
	edName2->Text=dmMain->eucEUC->Name2;
}
//---------------------------------------------------------------------------

void __fastcall TfrmEUC::clearEdits()
{

}
//---------------------------------------------------------------------------



