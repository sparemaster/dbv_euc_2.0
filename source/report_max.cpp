//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "report_max.h"
#include "datamodule.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "QuickRpt"
#pragma link "QRCtrls"
#pragma link "QRPDFFilt"
#pragma resource "*.dfm"
TfrmReportMax *frmReportMax;
//---------------------------------------------------------------------------
__fastcall TfrmReportMax::TfrmReportMax(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmReportMax::PrepareData()
{
	Screen->Cursor=crHourGlass;
	lblCaption->Font->Size=24;
//	FItemType=itemtype;
	lblSubcaption->Enabled=true;
//	counter=0;
	QuickRep1->ReportTitle=dmMain->eucEUC->Name1;
	lblStand->Caption=FormatDateTime("dd/' 'mmmm' 'yyyy'  'hh:nn 'Uhr'",Now());
	lblSubcaption->Caption=dmMain->eucEUC->Name2;
	qryMax->Open();

	Screen->Cursor=crDefault;
}
//---------------------------------------------------------------------------

void __fastcall TfrmReportMax::CloseData()
{
	qryMax->Close();
}
//---------------------------------------------------------------------------


void __fastcall TfrmReportMax::DetailBand1BeforePrint(TQRCustomBand *Sender, bool &PrintBand)
{
	int max=230;
	if (qryMax->FieldByName("Gruppe")->AsString=="Damen") {
		if (qryMax->FieldByName("Spiel1")->AsInteger>=max) {
//			QRDBText2->Font->Color=clHotLight;
			QRDBText2->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText2->Font->Color=clWindowText;
			QRDBText2->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel2")->AsInteger>=max) {
//			QRDBText3->Font->Color=clHotLight;
			QRDBText3->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText3->Font->Color=clWindowText;
			QRDBText3->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel3")->AsInteger>=max) {
//			QRDBText4->Font->Color=clHotLight;
			QRDBText4->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText4->Font->Color=clWindowText;
			QRDBText4->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel4")->AsInteger>=max) {
//			QRDBText5->Font->Color=clHotLight;
			QRDBText5->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText5->Font->Color=clWindowText;
			QRDBText5->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel5")->AsInteger>=max) {
//			QRDBText6->Font->Color=clHotLight;
			QRDBText6->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText6->Font->Color=clWindowText;
			QRDBText6->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel6")->AsInteger>=max) {
//			QRDBText7->Font->Color=clHotLight;
			QRDBText7->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText7->Font->Color=clWindowText;
			QRDBText7->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel7")->AsInteger>=max) {
//			QRDBText8->Font->Color=clHotLight;
			QRDBText8->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText8->Font->Color=clWindowText;
			QRDBText8->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel8")->AsInteger>=max) {
//			QRDBText9->Font->Color=clHotLight;
			QRDBText9->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText9->Font->Color=clWindowText;
			QRDBText9->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel9")->AsInteger>=max) {
//			QRDBText10->Font->Color=clHotLight;
			QRDBText10->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText10->Font->Color=clWindowText;
			QRDBText10->Font->Style=TFontStyles();
		}
	}
	else {
		max=260;
		if (qryMax->FieldByName("Spiel1")->AsInteger>=max) {
//			QRDBText2->Font->Color=clHotLight;
			QRDBText2->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText2->Font->Color=clWindowText;
			QRDBText2->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel2")->AsInteger>=max) {
//			QRDBText3->Font->Color=clHotLight;
			QRDBText3->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText3->Font->Color=clWindowText;
			QRDBText3->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel3")->AsInteger>=max) {
//			QRDBText4->Font->Color=clHotLight;
			QRDBText4->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText4->Font->Color=clWindowText;
			QRDBText4->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel4")->AsInteger>=max) {
//			QRDBText5->Font->Color=clHotLight;
			QRDBText5->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText5->Font->Color=clWindowText;
			QRDBText5->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel5")->AsInteger>=max) {
//			QRDBText6->Font->Color=clHotLight;
			QRDBText6->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText6->Font->Color=clWindowText;
			QRDBText6->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel6")->AsInteger>=max) {
//			QRDBText7->Font->Color=clHotLight;
			QRDBText7->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText7->Font->Color=clWindowText;
			QRDBText7->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel7")->AsInteger>=max) {
//			QRDBText8->Font->Color=clHotLight;
			QRDBText8->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText8->Font->Color=clWindowText;
			QRDBText8->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel8")->AsInteger>=max) {
//			QRDBText9->Font->Color=clHotLight;
			QRDBText9->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText9->Font->Color=clWindowText;
			QRDBText9->Font->Style=TFontStyles();
		}
		if (qryMax->FieldByName("Spiel9")->AsInteger>=max) {
//			QRDBText10->Font->Color=clHotLight;
			QRDBText10->Font->Style=TFontStyles()<<fsBold;
		}
		else {
//			QRDBText10->Font->Color=clWindowText;
			QRDBText10->Font->Style=TFontStyles();
		}
	}
}
//---------------------------------------------------------------------------
