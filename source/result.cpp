//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "result.h"
#include "datamodule.h"
#include "team.h"
#include "main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "LMDControl"
#pragma link "LMDCustomBevelPanel"
#pragma link "LMDCustomControl"
#pragma link "LMDCustomPanel"
#pragma link "LMDSimplePanel"
#pragma link "LMDBaseEdit"
#pragma link "LMDCustomEdit"
#pragma link "LMDCustomExtCombo"
#pragma link "LMDCustomListComboBox"
#pragma link "LMDCustomMaskEdit"
#pragma link "LMDListComboBox"
#pragma link "LMDMaskEdit"
#pragma link "LMDBaseControl"
#pragma link "LMDBaseGraphicControl"
#pragma link "LMDBaseLabel"
#pragma link "LMDCustomSimpleLabel"
#pragma link "LMDSimpleLabel"
#pragma link "LMDGraphicControl"
#pragma link "LMDTechnicalLine"
#pragma link "LMDButton"
#pragma link "LMDCustomButton"
#pragma link "LMDCustomLabel"
#pragma link "LMDLabel"
#pragma link "LMDCustomButtonGroup"
#pragma link "LMDCustomGroupBox"
#pragma link "LMDCustomParentPanel"
#pragma link "LMDCustomRadioGroup"
#pragma link "LMDRadioGroup"
#pragma resource "*.dfm"
TfrmResult *frmResult;
//---------------------------------------------------------------------------
__fastcall TfrmResult::TfrmResult(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::setChanged(TObject *Sender,bool Value)
{
	if (isLoading) {
		return;
	}
	
	btnOK->Enabled=Value;
	btnCancel->Enabled=Value;
	lblChanged->Visible=Value;
	btnClose->Enabled=!Value;
	if ((Sender!=cmbTeam) && (Sender!=edTeamnumber)) {
		edTeamnumber->Enabled=!Value;
		cmbTeam->Enabled=!Value;
	}
	rgInput->Enabled=!Value;
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::fillCombo()
{
	cmbTeam->Items->Clear();
	TADOQuery *q=new TADOQuery(this);
	q->Connection=dmMain->conMain;
	q->SQL->Add("select * from teams order by name");
	try {
		q->Open();
		while (!q->Eof) {
			cmbTeam->Items->AddObject(q->FieldByName("Name")->AsString,
									  (TObject*)(q->FieldByName("TeamIdx")->AsInteger));
			q->Next();
		}
	}
	__finally {
		q->Close();
		delete q;
		cmbTeam->ItemIndex=-1;
    }
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::fillPlayer(int idx)
{
	TLMDSimpleLabel *lbl;
	TLMDLabel *lblIdx;
	TLMDMaskEdit *s1,*s2,*s3,*s4,*s5,*s6,*s7,*s8,*s9;
	switch (idx) {
		case 1:	lbl=lblPlayer1;lblIdx=lblPlayerIdx1;
			s1=edGameVR1_1_1;s2=edGameVR1_1_2;s3=edGameVR1_1_3;
			s4=edGameVR2_1_1;s5=edGameVR2_1_2;s6=edGameVR2_1_3;
			s7=edGameF_1_1;s8=edGameF_1_2;s9=edGameF_1_3;
			break;
		case 2:	lbl=lblPlayer2;lblIdx=lblPlayerIdx2;
			s1=edGameVR1_2_1;s2=edGameVR1_2_2;s3=edGameVR1_2_3;
			s4=edGameVR2_2_1;s5=edGameVR2_2_2;s6=edGameVR2_2_3;
			s7=edGameF_2_1;s8=edGameF_2_2;s9=edGameF_2_3;
			break;
		case 3:	lbl=lblPlayer3;lblIdx=lblPlayerIdx3;
			s1=edGameVR1_3_1;s2=edGameVR1_3_2;s3=edGameVR1_3_3;
			s4=edGameVR2_3_1;s5=edGameVR2_3_2;s6=edGameVR2_3_3;
			s7=edGameF_3_1;s8=edGameF_3_2;s9=edGameF_3_3;
			break;
		case 4:	lbl=lblPlayer4;lblIdx=lblPlayerIdx4;
			s1=edGameVR1_4_1;s2=edGameVR1_4_2;s3=edGameVR1_4_3;
			s4=edGameVR2_4_1;s5=edGameVR2_4_2;s6=edGameVR2_4_3;
			s7=edGameF_4_1;s8=edGameF_4_2;s9=edGameF_4_3;
			break;
		case 5:	lbl=lblPlayer5;lblIdx=lblPlayerIdx5;
			s1=edGameVR1_5_1;s2=edGameVR1_5_2;s3=edGameVR1_5_3;
			s4=edGameVR2_5_1;s5=edGameVR2_5_2;s6=edGameVR2_5_3;
			s7=edGameF_5_1;s8=edGameF_5_2;s9=edGameF_5_3;
			break;
		case 6:	lbl=lblPlayer6;lblIdx=lblPlayerIdx6;
			s1=edGameVR1_6_1;s2=edGameVR1_6_2;s3=edGameVR1_6_3;
			s4=edGameVR2_6_1;s5=edGameVR2_6_2;s6=edGameVR2_6_3;
			s7=edGameF_6_1;s8=edGameF_6_2;s9=edGameF_6_3;
			break;
	default:
		;
	}
	lbl->Caption=dmMain->eucTeam->CurrentPlayer->nachname+", "+dmMain->eucTeam->CurrentPlayer->vorname;
	lblIdx->Caption=IntToStr(dmMain->eucTeam->CurrentPlayer->idx);

	s1->Visible=true;s2->Visible=true;s3->Visible=true;
	s4->Visible=true;s5->Visible=true;s6->Visible=true;
	s7->Visible=true;s8->Visible=true;s9->Visible=true;
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::fillResult(int idx)
{
	TDBVEUCResultClass *result=dmMain->eucTeam->CurrentResult;

	TLMDMaskEdit *s1,*s2,*s3,*s4,*s5,*s6,*s7,*s8,*s9;
	switch (idx) {
		case 1:
			s1=edGameVR1_1_1;s2=edGameVR1_1_2;s3=edGameVR1_1_3;
			s4=edGameVR2_1_1;s5=edGameVR2_1_2;s6=edGameVR2_1_3;
			s7=edGameF_1_1;s8=edGameF_1_2;s9=edGameF_1_3;
			break;
		case 2:
			s1=edGameVR1_2_1;s2=edGameVR1_2_2;s3=edGameVR1_2_3;
			s4=edGameVR2_2_1;s5=edGameVR2_2_2;s6=edGameVR2_2_3;
			s7=edGameF_2_1;s8=edGameF_2_2;s9=edGameF_2_3;
			break;
		case 3:
			s1=edGameVR1_3_1;s2=edGameVR1_3_2;s3=edGameVR1_3_3;
			s4=edGameVR2_3_1;s5=edGameVR2_3_2;s6=edGameVR2_3_3;
			s7=edGameF_3_1;s8=edGameF_3_2;s9=edGameF_3_3;
			break;
		case 4:
			s1=edGameVR1_4_1;s2=edGameVR1_4_2;s3=edGameVR1_4_3;
			s4=edGameVR2_4_1;s5=edGameVR2_4_2;s6=edGameVR2_4_3;
			s7=edGameF_4_1;s8=edGameF_4_2;s9=edGameF_4_3;
			break;
		case 5:
			s1=edGameVR1_5_1;s2=edGameVR1_5_2;s3=edGameVR1_5_3;
			s4=edGameVR2_5_1;s5=edGameVR2_5_2;s6=edGameVR2_5_3;
			s7=edGameF_5_1;s8=edGameF_5_2;s9=edGameF_5_3;
			break;
		case 6:
			s1=edGameVR1_6_1;s2=edGameVR1_6_2;s3=edGameVR1_6_3;
			s4=edGameVR2_6_1;s5=edGameVR2_6_2;s6=edGameVR2_6_3;
			s7=edGameF_6_1;s8=edGameF_6_2;s9=edGameF_6_3;
			break;
	}

	s1->Text=IntToStr(result->spiel1);
	s2->Text=IntToStr(result->spiel2);
	s3->Text=IntToStr(result->spiel3);
	s4->Text=IntToStr(result->spiel4);
	s5->Text=IntToStr(result->spiel5);
	s6->Text=IntToStr(result->spiel6);
	s7->Text=IntToStr(result->spiel7);
	s8->Text=IntToStr(result->spiel8);
	s9->Text=IntToStr(result->spiel9);
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::fillSum()
{
	TDBVEUCTeam *p=dmMain->eucTeam;
	TDBVEUCResultSumData data=p->getResultSumData();

	//Handicap
	lblHDC1->Caption=IntToStr(p->TeamHdc);
	lblHDC2->Caption=IntToStr(p->TeamHdc);
	lblHDC3->Caption=IntToStr(p->TeamHdc);
	lblHDC4->Caption=IntToStr(p->TeamHdc);
	lblHDC5->Caption=IntToStr(p->TeamHdc);
	lblHDC6->Caption=IntToStr(p->TeamHdc);
	lblHdcVR1->Caption=IntToStr(3*p->TeamHdc);
	lblHdcVR2->Caption=IntToStr(3*p->TeamHdc);
	lblHdcVR->Caption=IntToStr(6*p->TeamHdc);
	lblHdcF1->Caption=IntToStr(p->TeamHdcFinal);
	lblHdcF2->Caption=IntToStr(p->TeamHdcFinal);
	lblHdcF3->Caption=IntToStr(p->TeamHdcFinal);
	lblHdcF->Caption=IntToStr(3*p->TeamHdcFinal);
	//Durchgänge
	lblVR1_D1->Caption=IntToStr(data.DVR1_1);
	lblVR1_D2->Caption=IntToStr(data.DVR1_2);
	lblVR1_D3->Caption=IntToStr(data.DVR1_3);
	lblVR2_D1->Caption=IntToStr(data.DVR2_1);
	lblVR2_D2->Caption=IntToStr(data.DVR2_2);
	lblVR2_D3->Caption=IntToStr(data.DVR2_3);
	lblF_D1->Caption=IntToStr(data.DF_1);
	lblF_D2->Caption=IntToStr(data.DF_2);
	lblF_D3->Caption=IntToStr(data.DF_3);
	lblVR1->Caption=IntToStr(data.DVR1_1+data.DVR1_2+data.DVR1_3);
	lblVR2->Caption=IntToStr(data.DVR2_1+data.DVR2_2+data.DVR2_3);
	lblF->Caption=IntToStr(data.DF_1+data.DF_2+data.DF_3);
	lblVR->Caption=IntToStr(data.DVR1_1+data.DVR1_2+data.DVR1_3+data.DVR2_1+data.DVR2_2+data.DVR2_3);
	//Gesamt
	lblAllVR1_D1->Caption=IntToStr(data.AllDVR1_1);
	lblAllVR1_D2->Caption=IntToStr(data.AllDVR1_2);
	lblAllVR1_D3->Caption=IntToStr(data.AllDVR1_3);
	lblAllVR2_D1->Caption=IntToStr(data.AllDVR2_1);
	lblAllVR2_D2->Caption=IntToStr(data.AllDVR2_2);
	lblAllVR2_D3->Caption=IntToStr(data.AllDVR2_3);
	lblAllF_D1->Caption=IntToStr(data.AllDF_1);
	lblAllF_D2->Caption=IntToStr(data.AllDF_2);
	lblAllF_D3->Caption=IntToStr(data.AllDF_3);
	lblAllVR1->Caption=IntToStr(data.AllDVR1_1+data.AllDVR1_2+data.AllDVR1_3);
	lblAllVR2->Caption=IntToStr(data.AllDVR2_1+data.AllDVR2_2+data.AllDVR2_3);
	lblAllF->Caption=IntToStr(data.AllDF_1+data.AllDF_2+data.AllDF_3);
	lblAllVR->Caption=IntToStr(data.AllDVR1_1+data.AllDVR1_2+data.AllDVR1_3+data.AllDVR2_1+data.AllDVR2_2+data.AllDVR2_3);
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::fillPlayerSum(int idx)
{
	TDBVEUCResultClass *result=dmMain->eucTeam->CurrentResult;

	TLMDSimpleLabel *lblSumVR1,*lblSumVR2,*lblSumVR,*lblSumF;
	switch (idx) {
		case 1:
			lblSumVR1=lblVorrunde1_1;lblSumVR2=lblVorrunde2_1;
			lblSumVR=lblVorrunde_1;lblSumF=lblFinale_1;
			break;
		case 2:
			lblSumVR1=lblVorrunde1_2;lblSumVR2=lblVorrunde2_2;
			lblSumVR=lblVorrunde_2;lblSumF=lblFinale_2;
			break;
		case 3:
			lblSumVR1=lblVorrunde1_3;lblSumVR2=lblVorrunde2_3;
			lblSumVR=lblVorrunde_3;lblSumF=lblFinale_3;
			break;
		case 4:
			lblSumVR1=lblVorrunde1_4;lblSumVR2=lblVorrunde2_4;
			lblSumVR=lblVorrunde_4;lblSumF=lblFinale_4;
			break;
		case 5:
			lblSumVR1=lblVorrunde1_5;lblSumVR2=lblVorrunde2_5;
			lblSumVR=lblVorrunde_5;lblSumF=lblFinale_5;
			break;
		case 6:
			lblSumVR1=lblVorrunde1_6;lblSumVR2=lblVorrunde2_6;
			lblSumVR=lblVorrunde_6;lblSumF=lblFinale_6;
			break;
	}

	lblSumVR->Caption=IntToStr(result->serieVR1+result->serieVR2);
	lblSumVR1->Caption=IntToStr(result->serieVR1);
	lblSumVR2->Caption=IntToStr(result->serieVR2);
	lblSumF->Caption=IntToStr(result->serieF);
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::fillEdits()
{

	edTeamnumber->Text=IntToStr(dmMain->eucTeam->Teamnummer);
	clearEdits();

	dmMain->eucTeam->PlayerSort=psName;
	dmMain->eucTeam->FirstPlayer();
	for (int j=1; j<=dmMain->eucTeam->PlayerCount; j++) {
		fillPlayer(j);
		if (dmMain->eucTeam->locateResult(dmMain->eucTeam->CurrentPlayer->idx)) {
			fillResult(j);
			fillPlayerSum(j);
		}
		dmMain->eucTeam->NextPlayer();
	}
	fillSum();

	setChanged(this,false);
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::clearEdits()
{
	lblPlayer1->Caption="";lblPlayer2->Caption="";lblPlayer3->Caption="";
	lblPlayer4->Caption="";lblPlayer5->Caption="";lblPlayer6->Caption="";

	edGameVR1_1_1->Text="";edGameVR1_1_2->Text="";edGameVR1_1_3->Text="";
	edGameVR2_1_1->Text="";edGameVR2_1_2->Text="";edGameVR2_1_3->Text="";
	edGameF_1_1->Text="";edGameF_1_2->Text="";edGameF_1_3->Text="";

	edGameVR1_2_1->Text="";edGameVR1_2_2->Text="";edGameVR1_2_3->Text="";
	edGameVR2_2_1->Text="";edGameVR2_2_2->Text="";edGameVR2_2_3->Text="";
	edGameF_2_1->Text="";edGameF_2_2->Text="";edGameF_2_3->Text="";

	edGameVR1_3_1->Text="";edGameVR1_3_2->Text="";edGameVR1_3_3->Text="";
	edGameVR2_3_1->Text="";edGameVR2_3_2->Text="";edGameVR2_3_3->Text="";
	edGameF_3_1->Text="";edGameF_3_2->Text="";edGameF_3_3->Text="";

	edGameVR1_4_1->Text="";edGameVR1_4_2->Text="";edGameVR1_4_3->Text="";
	edGameVR2_4_1->Text="";edGameVR2_4_2->Text="";edGameVR2_4_3->Text="";
	edGameF_4_1->Text="";edGameF_4_2->Text="";edGameF_4_3->Text="";

	edGameVR1_5_1->Text="";edGameVR1_5_2->Text="";edGameVR1_5_3->Text="";
	edGameVR2_5_1->Text="";edGameVR2_5_2->Text="";edGameVR2_5_3->Text="";
	edGameF_5_1->Text="";edGameF_5_2->Text="";edGameF_5_3->Text="";

	edGameVR1_6_1->Text="";edGameVR1_6_2->Text="";edGameVR1_6_3->Text="";
	edGameVR2_6_1->Text="";edGameVR2_6_2->Text="";edGameVR2_6_3->Text="";
	edGameF_6_1->Text="";edGameF_6_2->Text="";edGameF_6_3->Text="";

	lblVorrunde1_1->Caption="";lblVorrunde1_2->Caption="";lblVorrunde1_3->Caption="";
	lblVorrunde1_4->Caption="";lblVorrunde1_5->Caption="";lblVorrunde1_6->Caption="";

	lblVorrunde2_1->Caption="";lblVorrunde2_2->Caption="";lblVorrunde2_3->Caption="";
	lblVorrunde2_4->Caption="";lblVorrunde2_5->Caption="";lblVorrunde2_6->Caption="";

	lblFinale_1->Caption="";lblFinale_2->Caption="";lblFinale_3->Caption="";
	lblFinale_4->Caption="";lblFinale_5->Caption="";lblFinale_6->Caption="";

	lblVorrunde_1->Caption="";lblVorrunde_2->Caption="";lblVorrunde_3->Caption="";
	lblVorrunde_4->Caption="";lblVorrunde_5->Caption="";lblVorrunde_6->Caption="";

	lblVR1_D1->Caption="";lblVR1_D2->Caption="";lblVR1_D3->Caption="";
	lblVR2_D1->Caption="";lblVR2_D2->Caption="";lblVR2_D3->Caption="";
	lblF_D1->Caption="";lblF_D2->Caption="";lblF_D3->Caption="";

	lblHDC1->Caption="";lblHDC2->Caption="";lblHDC3->Caption="";
	lblHDC4->Caption="";lblHDC5->Caption="";lblHDC6->Caption="";
	lblHdcF1->Caption="";lblHdcF2->Caption="";lblHdcF3->Caption="";

	lblAllVR1_D1->Caption="";lblAllVR1_D2->Caption="";lblAllVR1_D3->Caption="";
	lblAllVR2_D1->Caption="";lblAllVR2_D2->Caption="";lblAllVR2_D3->Caption="";
	lblAllF_D1->Caption="";lblAllF_D2->Caption="";lblAllF_D3->Caption="";

	lblVR1->Caption="";lblVR2->Caption="";lblF->Caption="";
	lblHdcVR1->Caption="";lblHdcVR2->Caption="";lblHdcF->Caption="";
	lblAllVR1->Caption="";lblAllVR2->Caption="";lblAllF->Caption="";

	lblVR->Caption="";lblHdcVR->Caption="";lblAllVR->Caption="";

	lblPlayerIdx1->Caption="0";lblPlayerIdx2->Caption="0";lblPlayerIdx3->Caption="0";
	lblPlayerIdx4->Caption="0";lblPlayerIdx5->Caption="0";lblPlayerIdx6->Caption="0";


	edGameVR1_1_1->Visible=false;edGameVR1_1_2->Visible=false;edGameVR1_1_3->Visible=false;
	edGameVR2_1_1->Visible=false;edGameVR2_1_2->Visible=false;edGameVR2_1_3->Visible=false;
	edGameF_1_1->Visible=false;edGameF_1_2->Visible=false;edGameF_1_3->Visible=false;

	edGameVR1_2_1->Visible=false;edGameVR1_2_2->Visible=false;edGameVR1_2_3->Visible=false;
	edGameVR2_2_1->Visible=false;edGameVR2_2_2->Visible=false;edGameVR2_2_3->Visible=false;
	edGameF_2_1->Visible=false;edGameF_2_2->Visible=false;edGameF_2_3->Visible=false;

	edGameVR1_3_1->Visible=false;edGameVR1_3_2->Visible=false;edGameVR1_3_3->Visible=false;
	edGameVR2_3_1->Visible=false;edGameVR2_3_2->Visible=false;edGameVR2_3_3->Visible=false;
	edGameF_3_1->Visible=false;edGameF_3_2->Visible=false;edGameF_3_3->Visible=false;

	edGameVR1_4_1->Visible=false;edGameVR1_4_2->Visible=false;edGameVR1_4_3->Visible=false;
	edGameVR2_4_1->Visible=false;edGameVR2_4_2->Visible=false;edGameVR2_4_3->Visible=false;
	edGameF_4_1->Visible=false;edGameF_4_2->Visible=false;edGameF_4_3->Visible=false;

	edGameVR1_5_1->Visible=false;edGameVR1_5_2->Visible=false;edGameVR1_5_3->Visible=false;
	edGameVR2_5_1->Visible=false;edGameVR2_5_2->Visible=false;edGameVR2_5_3->Visible=false;
	edGameF_5_1->Visible=false;edGameF_5_2->Visible=false;edGameF_5_3->Visible=false;

	edGameVR1_6_1->Visible=false;edGameVR1_6_2->Visible=false;edGameVR1_6_3->Visible=false;
	edGameVR2_6_1->Visible=false;edGameVR2_6_2->Visible=false;edGameVR2_6_3->Visible=false;
	edGameF_6_1->Visible=false;edGameF_6_2->Visible=false;edGameF_6_3->Visible=false;





	setChanged(this,false);
}
//---------------------------------------------------------------------------

TModalResult __fastcall TfrmResult::show(int team)
{
	isLoading=true;
	fillCombo();
	cmbTeam->ItemIndex=cmbTeam->Items->IndexOfObject((TObject*)dmMain->eucTeam->TeamIdx);
	fillEdits();
	rgInput->ItemIndex=0;
	rgInputChange(this,0);
	isLoading=false;
	setChanged(this,false);

	return ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::cmbTeamChange(TObject *Sender)
{
	if (isLoading) {
		return;
	}

	//setChanged(Sender,true);
	int TeamIdx=(int)(cmbTeam->Items->Objects[cmbTeam->ItemIndex]);
	dmMain->qryTeams->Locate("TeamIdx",TeamIdx,TLocateOptions());
	edTeamnumber->Text=IntToStr(dmMain->eucTeam->Teamnummer);
	isLoading=true;
	fillEdits();
	isLoading=false;
	//setChanged(Sender,false);
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edTeamnumberChange(TObject *Sender)
{
	if (isLoading || edTeamnumber->Text.Length()<3) {
		return;
	}

//	setChanged(true);
	int Teamnumber=edTeamnumber->Text.ToInt();
	bool found=dmMain->qryTeams->Locate("Nummer",Teamnumber,TLocateOptions());
	isLoading=true;
	if (found) {
		cmbTeam->ItemIndex=cmbTeam->Items->IndexOfObject((TObject*)(dmMain->eucTeam->TeamIdx));
		fillEdits();
	}
	else {
		cmbTeam->ItemIndex=-1;
		clearEdits();
	}
	isLoading=false;
	if (rgInput->ItemIndex==0) {
		edGameVR1_1_1->SetFocus();
	}
	else if (rgInput->ItemIndex==1) {
			edGameVR2_1_1->SetFocus();
		 }
		 else {
			edGameF_1_1->SetFocus();
		 }
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::FormShow(TObject *Sender)
{
	dmMain->eucTeam->Active=true;
	dmMain->qryTeams->DisableControls();
	FModified=false;
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::FormHide(TObject *Sender)
{
	if (FModified) {
		int idx=dmMain->eucTeam->TeamIdx;
		dmMain->qryTeams->Close();
		dmMain->qryTeams->Open();
		dmMain->qryTeams->Locate("TeamIdx",idx,TLocateOptions());
	}
	dmMain->qryTeams->EnableControls();
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::btnOKClick(TObject *Sender)
{
	dmMain->eucTeam->writeToDB(dmMain->eucTeam->TeamIdx);
	FModified=true;
	setChanged(Sender,false);
	edTeamnumber->SetFocus();
	edTeamnumber->SelectAll();
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::btnCancelClick(TObject *Sender)
{
	dmMain->eucTeam->readFromDB(dmMain->eucTeam->TeamIdx);
	fillEdits();
	edTeamnumberChange(Sender);
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR1_1_1Enter(TObject *Sender)
{
	dmMain->eucTeam->setActivePlayer(lblPlayerIdx1->Caption.ToInt());
	dmMain->eucTeam->locateResult(dmMain->eucTeam->CurrentPlayer->idx);
	FCurrentIdx=1;
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR1_2_1Enter(TObject *Sender)
{
	dmMain->eucTeam->setActivePlayer(lblPlayerIdx2->Caption.ToInt());
	dmMain->eucTeam->locateResult(dmMain->eucTeam->CurrentPlayer->idx);
	FCurrentIdx=2;
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR1_3_1Enter(TObject *Sender)
{
	dmMain->eucTeam->setActivePlayer(lblPlayerIdx3->Caption.ToInt());
	dmMain->eucTeam->locateResult(dmMain->eucTeam->CurrentPlayer->idx);
	FCurrentIdx=3;
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR1_4_1Enter(TObject *Sender)
{
	dmMain->eucTeam->setActivePlayer(lblPlayerIdx4->Caption.ToInt());
	dmMain->eucTeam->locateResult(dmMain->eucTeam->CurrentPlayer->idx);
	FCurrentIdx=4;
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR1_5_1Enter(TObject *Sender)
{
	dmMain->eucTeam->setActivePlayer(lblPlayerIdx5->Caption.ToInt());
	dmMain->eucTeam->locateResult(dmMain->eucTeam->CurrentPlayer->idx);
	FCurrentIdx=5;
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR1_6_1Enter(TObject *Sender)
{
	dmMain->eucTeam->setActivePlayer(lblPlayerIdx6->Caption.ToInt());
	dmMain->eucTeam->locateResult(dmMain->eucTeam->CurrentPlayer->idx);
	FCurrentIdx=6;
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::btnCloseClick(TObject *Sender)
{
	Close();
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR1_1_1Exit(TObject *Sender)
{
	if (isLoading) {
		return;
	}
	TLMDMaskEdit *ed=(TLMDMaskEdit*)Sender;
	if (dmMain->eucTeam->CurrentResult==NULL) {
		dmMain->eucTeam->newResult(dmMain->eucTeam->CurrentPlayer->idx);
	}
	if (dmMain->eucTeam->CurrentResult->spiel1!=ed->Text.ToInt()) {
		dmMain->eucTeam->CurrentResult->spiel1=ed->Text.ToInt();
		dmMain->eucTeam->calcResultData();
		fillSum();
		fillPlayerSum(FCurrentIdx);
		setChanged(Sender,true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR1_1_2Exit(TObject *Sender)
{
	if (isLoading) {
		return;
	}
	TLMDMaskEdit *ed=(TLMDMaskEdit*)Sender;
	if (dmMain->eucTeam->CurrentResult==NULL) {
		dmMain->eucTeam->newResult(dmMain->eucTeam->CurrentPlayer->idx);
	}
	if (dmMain->eucTeam->CurrentResult->spiel2!=ed->Text.ToInt()) {
		dmMain->eucTeam->CurrentResult->spiel2=ed->Text.ToInt();
		dmMain->eucTeam->calcResultData();
		fillSum();
		fillPlayerSum(FCurrentIdx);
		setChanged(Sender,true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR1_1_3Exit(TObject *Sender)
{
	if (isLoading) {
		return;
	}
	TLMDMaskEdit *ed=(TLMDMaskEdit*)Sender;
	if (dmMain->eucTeam->CurrentResult==NULL) {
		dmMain->eucTeam->newResult(dmMain->eucTeam->CurrentPlayer->idx);
	}
	if (dmMain->eucTeam->CurrentResult->spiel3!=ed->Text.ToInt()) {
		dmMain->eucTeam->CurrentResult->spiel3=ed->Text.ToInt();
		dmMain->eucTeam->calcResultData();
		fillSum();
		fillPlayerSum(FCurrentIdx);
		setChanged(Sender,true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR2_1_1Exit(TObject *Sender)
{
	if (isLoading) {
		return;
	}
	TLMDMaskEdit *ed=(TLMDMaskEdit*)Sender;
	if (dmMain->eucTeam->CurrentResult==NULL) {
		dmMain->eucTeam->newResult(dmMain->eucTeam->CurrentPlayer->idx);
	}
	if (dmMain->eucTeam->CurrentResult->spiel4!=ed->Text.ToInt()) {
		dmMain->eucTeam->CurrentResult->spiel4=ed->Text.ToInt();
		dmMain->eucTeam->calcResultData();
		fillSum();
		fillPlayerSum(FCurrentIdx);
		setChanged(Sender,true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR2_1_2Exit(TObject *Sender)
{
	if (isLoading) {
		return;
	}
	TLMDMaskEdit *ed=(TLMDMaskEdit*)Sender;
	if (dmMain->eucTeam->CurrentResult==NULL) {
		dmMain->eucTeam->newResult(dmMain->eucTeam->CurrentPlayer->idx);
	}
	if (dmMain->eucTeam->CurrentResult->spiel5!=ed->Text.ToInt()) {
		dmMain->eucTeam->CurrentResult->spiel5=ed->Text.ToInt();
		dmMain->eucTeam->calcResultData();
		fillSum();
		fillPlayerSum(FCurrentIdx);
		setChanged(Sender,true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameVR2_1_3Exit(TObject *Sender)
{
	if (isLoading) {
		return;
	}
	TLMDMaskEdit *ed=(TLMDMaskEdit*)Sender;
	if (dmMain->eucTeam->CurrentResult==NULL) {
		dmMain->eucTeam->newResult(dmMain->eucTeam->CurrentPlayer->idx);
	}
	if (dmMain->eucTeam->CurrentResult->spiel6!=ed->Text.ToInt()) {
		dmMain->eucTeam->CurrentResult->spiel6=ed->Text.ToInt();
		dmMain->eucTeam->calcResultData();
		fillSum();
		fillPlayerSum(FCurrentIdx);
		setChanged(Sender,true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameF_1_1Exit(TObject *Sender)
{
	if (isLoading) {
		return;
	}
	TLMDMaskEdit *ed=(TLMDMaskEdit*)Sender;
	if (dmMain->eucTeam->CurrentResult==NULL) {
		dmMain->eucTeam->newResult(dmMain->eucTeam->CurrentPlayer->idx);
	}
	if (dmMain->eucTeam->CurrentResult->spiel7!=ed->Text.ToInt()) {
		dmMain->eucTeam->CurrentResult->spiel7=ed->Text.ToInt();
		dmMain->eucTeam->calcResultData();
		fillSum();
		fillPlayerSum(FCurrentIdx);
		setChanged(Sender,true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameF_1_2Exit(TObject *Sender)
{
	if (isLoading) {
		return;
	}
	TLMDMaskEdit *ed=(TLMDMaskEdit*)Sender;
	if (dmMain->eucTeam->CurrentResult==NULL) {
		dmMain->eucTeam->newResult(dmMain->eucTeam->CurrentPlayer->idx);
	}
	if (dmMain->eucTeam->CurrentResult->spiel8!=ed->Text.ToInt()) {
		dmMain->eucTeam->CurrentResult->spiel8=ed->Text.ToInt();
		dmMain->eucTeam->calcResultData();
		fillSum();
		fillPlayerSum(FCurrentIdx);
		setChanged(Sender,true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::edGameF_1_3Exit(TObject *Sender)
{
	if (isLoading) {
		return;
	}
	TLMDMaskEdit *ed=(TLMDMaskEdit*)Sender;
	if (dmMain->eucTeam->CurrentResult==NULL) {
		dmMain->eucTeam->newResult(dmMain->eucTeam->CurrentPlayer->idx);
	}
	if (dmMain->eucTeam->CurrentResult->spiel9!=ed->Text.ToInt()) {
		dmMain->eucTeam->CurrentResult->spiel9=ed->Text.ToInt();
		dmMain->eucTeam->calcResultData();
		fillSum();
		fillPlayerSum(FCurrentIdx);
		setChanged(Sender,true);
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::rgInputChange(TObject *Sender, int ButtonIndex)
{
	for (int i=0; i<pnlClient->ControlCount; i++) {
		TControl *c=pnlClient->Controls[i];
		if (c->Tag>0) {
			TLMDMaskEdit *e=(TLMDMaskEdit*)c;
			e->TabStop=e->Tag==(ButtonIndex+1);
		}
	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmResult::LMDSimpleLabel1Click(TObject *Sender)
{
	frmMain->acTeamEdit->Execute();
	
}
//---------------------------------------------------------------------------

