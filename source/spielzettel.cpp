//----------------------------------------------------------------------------
#pragma hdrstop

#include "spielzettel.h"
#include "datamodule.h"
//----------------------------------------------------------------------------
#pragma link "QRPDFFilt"
#pragma resource "*.dfm"
TqrpSpielzettel *qrpSpielzettel;
//----------------------------------------------------------------------------
__fastcall TqrpSpielzettel::TqrpSpielzettel(TComponent* Owner)
    : TForm(Owner)
{
}
//----------------------------------------------------------------------------

void __fastcall TqrpSpielzettel::PrepareData(int gruppe, int team)
{
	//FTeam anpassen bei mehr als einem Spielzettel drucken
	FTeam=team;
	FGruppe=gruppe;
	qryData->Open();
}
//----------------------------------------------------------------------------

void __fastcall TqrpSpielzettel::CloseData()
{
  	qryData->Close();
}
//----------------------------------------------------------------------------

void __fastcall TqrpSpielzettel::qryDataFilterRecord(TDataSet *DataSet,
      bool &Accept)
{
	bool b=true;
	if (FTeam>0) {
		b=DataSet->FieldByName("TeamIdx")->Value==FTeam;
	}
	else {  //FGruppe >0
		switch (FGruppe) {
			case  1 : 	b=DataSet->FieldByName("gruppe")->AsString=="A";
						break;
			case  2 : 	b=DataSet->FieldByName("gruppe")->AsString=="B";
						break;
			case  3 : 	b=(DataSet->FieldByName("gruppe")->AsString=="A") ||
						(DataSet->FieldByName("gruppe")->AsString=="B");
						break;
			case  4 : 	b=DataSet->FieldByName("gruppe")->AsString=="C";
						break;
			case  5 : 	b=DataSet->FieldByName("gruppe")->AsString=="A" ||
						(DataSet->FieldByName("gruppe")->AsString=="C");
						break;
			case  6 : 	b=DataSet->FieldByName("gruppe")->AsString=="B" ||
						(DataSet->FieldByName("gruppe")->AsString=="C");
						break;
			case  7 : 	b=DataSet->FieldByName("gruppe")->AsString=="A" ||
						(DataSet->FieldByName("gruppe")->AsString=="B") ||
						(DataSet->FieldByName("gruppe")->AsString=="C");
						break;
			case  8 : 	b=DataSet->FieldByName("gruppe")->AsString=="D";
						break;
			case  9 : 	b=(DataSet->FieldByName("gruppe")->AsString=="A") ||
						(DataSet->FieldByName("gruppe")->AsString=="D");
						break;
			case 10 : 	b=(DataSet->FieldByName("gruppe")->AsString=="B") ||
						(DataSet->FieldByName("gruppe")->AsString=="D");
						break;
			case 11 : 	b=DataSet->FieldByName("gruppe")->AsString=="A" ||
						(DataSet->FieldByName("gruppe")->AsString=="B") ||
						(DataSet->FieldByName("gruppe")->AsString=="D");
						break;
			case 12 : 	b=(DataSet->FieldByName("gruppe")->AsString=="C") ||
						(DataSet->FieldByName("gruppe")->AsString=="D");
						break;
			case 13 : 	b=DataSet->FieldByName("gruppe")->AsString=="A" ||
						(DataSet->FieldByName("gruppe")->AsString=="C") ||
						(DataSet->FieldByName("gruppe")->AsString=="D");
						break;
			case 14 : 	b=DataSet->FieldByName("gruppe")->AsString=="B" ||
						(DataSet->FieldByName("gruppe")->AsString=="C") ||
						(DataSet->FieldByName("gruppe")->AsString=="D");
						break;
/*
			case 15 : 	b=DataSet->FieldByName("gruppe")->AsString=="A" ||
						(DataSet->FieldByName("gruppe")->AsString=="B") ||
						(DataSet->FieldByName("gruppe")->AsString=="C") ||
						(DataSet->FieldByName("gruppe")->AsString=="D");
						break;
*/
		default:
			b=true;
		}
	}
	Accept=b;
}
//---------------------------------------------------------------------------

void __fastcall TqrpSpielzettel::detailbandBeforePrint(
      TQRCustomBand *Sender, bool &PrintBand)
{
  lblSpieler1->Caption="";
  lblSpieler2->Caption="";
  lblSpieler3->Caption="";
  lblSpieler4->Caption="";
  lblSpieler5->Caption="";
  lblSpieler6->Caption="";
  lblNummer1->Caption="";
  lblNummer2->Caption="";
  lblNummer3->Caption="";
  lblNummer4->Caption="";
  lblNummer5->Caption="";
  lblNummer6->Caption="";
  lblVR1_1_1->Caption="";lblVR1_2_1->Caption="";lblVR1_3_1->Caption="";lblVR1_4_1->Caption="";lblVR1_5_1->Caption="";lblVR1_6_1->Caption="";
  lblVR1_1_2->Caption="";lblVR1_2_2->Caption="";lblVR1_3_2->Caption="";lblVR1_4_2->Caption="";lblVR1_5_2->Caption="";lblVR1_6_2->Caption="";
  lblVR1_1_3->Caption="";lblVR1_2_3->Caption="";lblVR1_3_3->Caption="";lblVR1_4_3->Caption="";lblVR1_5_3->Caption="";lblVR1_6_3->Caption="";
  lblVR2_1_1->Caption="";lblVR2_2_1->Caption="";lblVR2_3_1->Caption="";lblVR2_4_1->Caption="";lblVR2_5_1->Caption="";lblVR2_6_1->Caption="";
  lblVR2_1_2->Caption="";lblVR2_2_2->Caption="";lblVR2_3_2->Caption="";lblVR2_4_2->Caption="";lblVR2_5_2->Caption="";lblVR2_6_2->Caption="";
  lblVR2_1_3->Caption="";lblVR2_2_3->Caption="";lblVR2_3_3->Caption="";lblVR2_4_3->Caption="";lblVR2_5_3->Caption="";lblVR2_6_3->Caption="";
  lblF_1_1->Caption="";lblF_2_1->Caption="";lblF_3_1->Caption="";lblF_4_1->Caption="";lblF_5_1->Caption="";lblF_6_1->Caption="";
  lblF_1_2->Caption="";lblF_2_2->Caption="";lblF_3_2->Caption="";lblF_4_2->Caption="";lblF_5_2->Caption="";lblF_6_2->Caption="";
  lblF_1_3->Caption="";lblF_2_3->Caption="";lblF_3_3->Caption="";lblF_4_3->Caption="";lblF_5_3->Caption="";lblF_6_3->Caption="";

	lblVR1_1->Caption="";lblVR1_2->Caption="";lblVR1_3->Caption="";lblVR1_4->Caption="";lblVR1_5->Caption="";lblVR1_6->Caption="";
	lblVR2_1->Caption="";lblVR2_2->Caption="";lblVR2_3->Caption="";lblVR2_4->Caption="";lblVR2_5->Caption="";lblVR2_6->Caption="";
	lblF_1->Caption="";lblF_2->Caption="";lblF_3->Caption="";lblF_4->Caption="";lblF_5->Caption="";lblF_6->Caption="";

	lblVR_1->Caption="";lblVR_2->Caption="";lblVR_3->Caption="";lblVR_4->Caption="";lblVR_5->Caption="";lblVR_6->Caption="";

	lblVR1_1_Netto->Caption="";lblVR1_2_Netto->Caption="";lblVR1_3_Netto->Caption="";
	lblVR2_1_Netto->Caption="";lblVR2_2_Netto->Caption="";lblVR2_3_Netto->Caption="";
	lblF_1_Netto->Caption="";lblF_2_Netto->Caption="";lblF_3_Netto->Caption="";
	lblVR1_Netto->Caption="";lblVR2_Netto->Caption="";lblVR_Netto->Caption="";
	lblF_Netto->Caption="";

	lblVR1_1_Brutto->Caption="";lblVR1_2_Brutto->Caption="";lblVR1_3_Brutto->Caption="";
	lblVR2_1_Brutto->Caption="";lblVR2_2_Brutto->Caption="";lblVR2_3_Brutto->Caption="";
	lblF_1_Brutto->Caption="";lblF_2_Brutto->Caption="";lblF_3_Brutto->Caption="";
	lblVR1_Brutto->Caption="";lblVR2_Brutto->Caption="";lblVR_Brutto->Caption="";
	lblF_Brutto->Caption="";

	if (dmMain->eucTeam->TeamHdcFinal!=0) lblHdcF1->Caption=IntToStr(dmMain->eucTeam->TeamHdcFinal); else lblHdcF1->Caption="";
	lblHdcF2->Caption=lblHdcF1->Caption;
	lblHdcF3->Caption=lblHdcF1->Caption;
	if (dmMain->eucTeam->TeamHdcFinal!=0) lblHdcF->Caption=IntToStr(3*dmMain->eucTeam->TeamHdcFinal); else lblHdcF->Caption="";

  lblHdc1->Caption=qryDatahdc->AsString;
  lblHdc2->Caption=qryDatahdc->AsString;
  lblHdc3->Caption=qryDatahdc->AsString;
  lblHdc4->Caption=qryDatahdc->AsString;
  lblHdc5->Caption=qryDatahdc->AsString;
  lblHdc6->Caption=qryDatahdc->AsString;
  lblHdcVR1->Caption=IntToStr(3*qryDatahdc->AsInteger);
  lblHdcVR2->Caption=IntToStr(3*qryDatahdc->AsInteger);
  lblHdcVR->Caption=IntToStr(6*qryDatahdc->AsInteger);
  //FTeam=qryDataTeamIdx->Value;
  qrySpieler->Close();
  qrySpieler->Parameters->ParamByName("TeamIdx")->Value=qryDataTeamIdx->Value;
  qrySpieler->Open();
  int count=0;
  TQRLabel *lbl,*vr1_1, *vr1_2, *vr1_3, *vr2_1, *vr2_2, *vr2_3, *f_1, *f_2, *f_3;
  TQRLabel *nr, *vr1, *vr2, *vr, *f;

  TDBVEUCResultClass *result;

  while (!qrySpieler->Eof && count<6) {
	//debug
	bool debug=false;
/*
	if ((qrySpieler->FieldByName("mnummer")->AsInteger==19392) || (qrySpieler->FieldByName("mnummer")->AsInteger==14428)) {
		debug=true;
	}
*/
	dmMain->eucTeam->setActiveResult(qrySpieler->FieldByName("Idx")->AsInteger);
	result=dmMain->eucTeam->CurrentResult;
	switch (count)
	{
	  case 0:lbl=lblSpieler1;nr=lblNummer1;if (!debug) {vr1_1=lblVR1_1_1;vr1_2=lblVR1_1_2;vr1_3=lblVR1_1_3;
		vr2_1=lblVR2_1_1;vr2_2=lblVR2_1_2;vr2_3=lblVR2_1_3;
		f_1=lblF_1_1;f_2=lblF_1_2;f_3=lblF_1_3;
		vr1=lblVR1_1;vr2=lblVR2_1;f=lblF_1;vr=lblVR_1;}
		break;
	  case 1:lbl=lblSpieler2;nr=lblNummer2;if (!debug) {vr1_1=lblVR1_2_1;vr1_2=lblVR1_2_2;vr1_3=lblVR1_2_3;
		vr2_1=lblVR2_2_1;vr2_2=lblVR2_2_2;vr2_3=lblVR2_2_3;
		f_1=lblF_2_1;f_2=lblF_2_2;f_3=lblF_2_3;
		vr1=lblVR1_2;vr2=lblVR2_2;f=lblF_2;vr=lblVR_2;}
		break;
	  case 2:lbl=lblSpieler3;nr=lblNummer3;if (!debug) {vr1_1=lblVR1_3_1;vr1_2=lblVR1_3_2;vr1_3=lblVR1_3_3;
		vr2_1=lblVR2_3_1;vr2_2=lblVR2_3_2;vr2_3=lblVR2_3_3;
		f_1=lblF_3_1;f_2=lblF_3_2;f_3=lblF_3_3;
		vr1=lblVR1_3;vr2=lblVR2_3;f=lblF_3;vr=lblVR_3;}
		break;
	  case 3:lbl=lblSpieler4;nr=lblNummer4;if (!debug) {vr1_1=lblVR1_4_1;vr1_2=lblVR1_4_2;vr1_3=lblVR1_4_3;
		vr2_1=lblVR2_4_1;vr2_2=lblVR2_4_2;vr2_3=lblVR2_4_3;
		f_1=lblF_4_1;f_2=lblF_4_2;f_3=lblF_4_3;
		vr1=lblVR1_4;vr2=lblVR2_4;f=lblF_4;vr=lblVR_4;}
		break;
	  case 4:lbl=lblSpieler5;nr=lblNummer5;if (!debug) {vr1_1=lblVR1_5_1;vr1_2=lblVR1_5_2;vr1_3=lblVR1_5_3;
		vr2_1=lblVR2_5_1;vr2_2=lblVR2_5_2;vr2_3=lblVR2_5_3;
		f_1=lblF_5_1;f_2=lblF_5_2;f_3=lblF_5_3;
		vr1=lblVR1_5;vr2=lblVR2_5;f=lblF_5;vr=lblVR_5;}
		break;
	  case 5:lbl=lblSpieler6;nr=lblNummer6;if (!debug) {vr1_1=lblVR1_6_1;vr1_2=lblVR1_6_2;vr1_3=lblVR1_6_3;
		vr2_1=lblVR2_6_1;vr2_2=lblVR2_6_2;vr2_3=lblVR2_6_3;
		f_1=lblF_6_1;f_2=lblF_6_2;f_3=lblF_6_3;
		vr1=lblVR1_6;vr2=lblVR2_6;f=lblF_6;vr=lblVR_6;}
		break;
	}
	lbl->Caption=qrySpieler->FieldByName("nachname")->AsString+", "+qrySpieler->FieldByName("vorname")->AsString;
	if (qrySpieler->FieldByName("mnummer")->AsString=="0") {
		nr->Caption="";
	}
	else {
		nr->Caption=qrySpieler->FieldByName("mnummer")->AsString;
	}
	if (result!=NULL) {
		if (result->spiel1!=0) vr1_1->Caption=IntToStr(result->spiel1);
		if (result->spiel2!=0) vr1_2->Caption=IntToStr(result->spiel2);
		if (result->spiel3!=0) vr1_3->Caption=IntToStr(result->spiel3);
		if (result->spiel4!=0) vr2_1->Caption=IntToStr(result->spiel4);
		if (result->spiel5!=0) vr2_2->Caption=IntToStr(result->spiel5);
		if (result->spiel6!=0) vr2_3->Caption=IntToStr(result->spiel6);
		if (result->spiel7!=0) f_1->Caption=IntToStr(result->spiel7);
		if (result->spiel8!=0) f_2->Caption=IntToStr(result->spiel8);
		if (result->spiel9!=0) f_3->Caption=IntToStr(result->spiel9);

		if (result->serieVR1!=0) vr1->Caption=IntToStr(result->serieVR1);
		if (result->serieVR2!=0) vr2->Caption=IntToStr(result->serieVR2);
		if (result->serieVR1+result->serieVR2!=0) vr->Caption=IntToStr(result->serieVR1+result->serieVR2);
		if (result->serieF!=0) f->Caption=IntToStr(result->serieF);

		TDBVEUCResultSumData sum=dmMain->eucTeam->getResultSumData();

		if (sum.DVR1_1!=0) lblVR1_1_Netto->Caption=IntToStr(sum.DVR1_1);
		if (sum.DVR1_2!=0) lblVR1_2_Netto->Caption=IntToStr(sum.DVR1_2);
		if (sum.DVR1_3!=0) lblVR1_3_Netto->Caption=IntToStr(sum.DVR1_3);
		if (sum.DVR1_1+sum.DVR1_2+sum.DVR1_3!=0) lblVR1_Netto->Caption=IntToStr(sum.DVR1_1+sum.DVR1_2+sum.DVR1_3);

		if (sum.DVR2_1!=0) lblVR2_1_Netto->Caption=IntToStr(sum.DVR2_1);
		if (sum.DVR2_2!=0) lblVR2_2_Netto->Caption=IntToStr(sum.DVR2_2);
		if (sum.DVR2_3!=0) lblVR2_3_Netto->Caption=IntToStr(sum.DVR2_3);
		if (sum.DVR2_1+sum.DVR2_2+sum.DVR2_3!=0) lblVR2_Netto->Caption=IntToStr(sum.DVR2_1+sum.DVR2_2+sum.DVR2_3);

		if (sum.DVR1_1+sum.DVR1_2+sum.DVR1_3+sum.DVR2_1+sum.DVR2_2+sum.DVR2_3!=0)
			lblVR_Netto->Caption=IntToStr(sum.DVR1_1+sum.DVR1_2+sum.DVR1_3+sum.DVR2_1+sum.DVR2_2+sum.DVR2_3);

		if (sum.DF_1!=0) lblF_1_Netto->Caption=IntToStr(sum.DF_1);
		if (sum.DF_2!=0) lblF_2_Netto->Caption=IntToStr(sum.DF_2);
		if (sum.DF_3!=0) lblF_3_Netto->Caption=IntToStr(sum.DF_3);
		if (sum.DF_1+sum.DF_2+sum.DF_3!=0) lblF_Netto->Caption=IntToStr(sum.DF_1+sum.DF_2+sum.DF_3);

		if (sum.AllDVR1_1!=0) lblVR1_1_Brutto->Caption=IntToStr(sum.AllDVR1_1);
		if (sum.AllDVR1_2!=0) lblVR1_2_Brutto->Caption=IntToStr(sum.AllDVR1_2);
		if (sum.AllDVR1_3!=0) lblVR1_3_Brutto->Caption=IntToStr(sum.AllDVR1_3);
		if (sum.AllDVR1_1+sum.AllDVR1_2+sum.AllDVR1_3!=0) lblVR1_Brutto->Caption=IntToStr(sum.AllDVR1_1+sum.AllDVR1_2+sum.AllDVR1_3);

		if (sum.AllDVR2_1!=0) lblVR2_1_Brutto->Caption=IntToStr(sum.AllDVR2_1);
		if (sum.AllDVR2_2!=0) lblVR2_2_Brutto->Caption=IntToStr(sum.AllDVR2_2);
		if (sum.AllDVR2_3!=0) lblVR2_3_Brutto->Caption=IntToStr(sum.AllDVR2_3);
		if (sum.AllDVR2_1+sum.AllDVR2_2+sum.AllDVR2_3!=0) lblVR2_Brutto->Caption=IntToStr(sum.AllDVR2_1+sum.AllDVR2_2+sum.AllDVR2_3);

		if (sum.AllDVR1_1+sum.AllDVR1_2+sum.AllDVR1_3+sum.AllDVR2_1+sum.AllDVR2_2+sum.AllDVR2_3!=0)
			lblVR_Brutto->Caption=IntToStr(sum.AllDVR1_1+sum.AllDVR1_2+sum.AllDVR1_3+sum.AllDVR2_1+sum.AllDVR2_2+sum.AllDVR2_3);

		if (sum.AllDF_1!=0) lblF_1_Brutto->Caption=IntToStr(sum.AllDF_1);
		if (sum.AllDF_2!=0) lblF_2_Brutto->Caption=IntToStr(sum.AllDF_2);
		if (sum.AllDF_3!=0) lblF_3_Brutto->Caption=IntToStr(sum.AllDF_3);
		if (sum.AllDF_1+sum.AllDF_2+sum.AllDF_3!=0) lblF_Brutto->Caption=IntToStr(sum.AllDF_1+sum.AllDF_2+sum.AllDF_3);

		vr->Enabled=result->serieVR2>0;
		lblVR_Netto->Enabled=lblVR_Netto->Caption!=lblVR1_Netto->Caption;
		lblVR_Brutto->Enabled=lblVR_Brutto->Caption!=lblVR1_Brutto->Caption;
	}

	lblTotal->Caption=lblF_Brutto->Caption;


	qrySpieler->Next();
	count++;
  }

  qrySpieler->Close();
}
//---------------------------------------------------------------------------

