//---------------------------------------------------------------------------

#ifndef startzeitenH
#define startzeitenH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "LMDButton.hpp"
#include "LMDControl.hpp"
#include "LMDCustomBevelPanel.hpp"
#include "LMDCustomButton.hpp"
#include "LMDCustomControl.hpp"
#include "LMDCustomPanel.hpp"
#include "LMDSimplePanel.hpp"
#include "DbAGrids.hpp"
#include <DBGrids.hpp>
#include <Grids.hpp>
#include "dagTmplt.hpp"
//---------------------------------------------------------------------------
class TfrmStartTime : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TLMDSimplePanel *LMDSimplePanel1;
	TLMDButton *btnClose;
	TDbAltGrid *DbAltGrid1;
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall btnCloseClick(TObject *Sender);
private:	// Benutzer-Deklarationen
public:		// Benutzer-Deklarationen
	__fastcall TfrmStartTime(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmStartTime *frmStartTime;
//---------------------------------------------------------------------------
#endif
