//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "report_startliste.h"
#include "main.h"
#include "datamodule.h"
//---------------------------------------------------------------------
#pragma link "QRPDFFilt"
#pragma resource "*.dfm"
TfrmReportStartliste *frmReportStartliste;
//---------------------------------------------------------------------
__fastcall TfrmReportStartliste::TfrmReportStartliste(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------

void __fastcall TfrmReportStartliste::PrepareData()
{
	qrpStartliste->ReportTitle=dmMain->eucEUC->Name1;
	lblStand->Caption=FormatDateTime("dd/' 'mmmm' 'yyyy'  'hh:nn 'Uhr'",Now());
	lblSubcaption->Caption=dmMain->eucEUC->Name2;
	qryTeams->Parameters->ParamByName("Idx")->Value=dmMain->eucEUC->Idx;
	qryTeams->Open();
	gruppenfuss->Height=0;
}
//---------------------------------------------------------------------

void __fastcall TfrmReportStartliste::CloseData()
{
	qryTeams->Close();
}
//---------------------------------------------------------------------

void __fastcall TfrmReportStartliste::qryTeamsCalcFields(TDataSet *DataSet)
{
	DataSet->FieldByName("Nummer2")->AsString="("+DataSet->FieldByName("Nummer")->AsString+")";
}
//---------------------------------------------------------------------------

