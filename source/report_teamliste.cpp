//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "report_teamliste.h"
#include "math.hpp"
#include "datamodule.h"
#include "DBVRoutines.h"
//---------------------------------------------------------------------
#pragma link "DBVEUCBaseComponent"
#pragma link "DBVEUCTeam"
#pragma resource "*.dfm"
TfrmAuswertung_Team *frmAuswertung_Team;
//--------------------------------------------------------------------- 
__fastcall TfrmAuswertung_Team::TfrmAuswertung_Team(TComponent* AOwner)
	: TForm(AOwner)
{
}
//---------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team::PrepareData(TDBVEUCItemType itemtype,bool scratch)
{
	Screen->Cursor=crHourGlass;
	lblCaption->Font->Size=24;
	FItemType=itemtype;
	lblSubcaption->Enabled=true;
	counter=0;
	qrpAuswertung->ReportTitle=dmMain->eucEUC->Name1;
	lblStand->Caption=FormatDateTime("dd/' 'mmmm' 'yyyy'  'hh:nn 'Uhr'",Now());
	lblSubcaption->Caption=dmMain->eucEUC->Name2;

	deleteTable(dmMain->conMain,"auswertung_team");

	if (createTable(dmMain->conMain,"CREATE TABLE auswertung_team (idx AUTOINCREMENT PRIMARY KEY, team INT, Netto INT, Brutto int, BestDurchgang INT, Qualifiziert BIT, Schnitt FLOAT)")) {
		switch (itemtype) {
			case itAuswertungVR1:
				lblTitel->Caption="Vorrunde 1";
				netto->Enabled=false;
				QRLabel3->Enabled=false;
				break;
			case itAuswertungVR2int:
				lblTitel->Caption="Vorrunde Scratch";
				netto->Enabled=false;
				QRLabel3->Enabled=false;
				hdc_finale->Enabled=true;
				QRLabel7->Enabled=true;
				break;
			case itAuswertungVR2:
				lblTitel->Caption="Vorrunde 2";
				netto->Enabled=false;
				QRLabel3->Enabled=false;
				break;
			case itAuswertungF:
				lblTitel->Caption="Finale";
				netto->Enabled=true;
				QRLabel3->Enabled=true;
				lblSubcaption->Enabled=false;
				if (scratch) {
					qrpAuswertung->ReportTitle=lblSubcaption->Caption;
					lblCaption->Font->Size=20;
				}
				else {
					netto->Enabled=false;
					QRLabel3->Enabled=false;
				}
				break;
	  }
	}

	//�bertrage jetzt die Daten in die Target-Tabelle
	int gesamt;
	qryTeams->Close();
	qryTeams->Parameters->ParamByName("Idx")->Value=dmMain->eucEUC->Idx;
	qryTeams->Open();
	tblTarget->Open();
	while (!qryTeams->Eof) {
		if ((itemtype==itAuswertungVR2int) && (qryTeams->FieldByName("Ausland")->AsBoolean)) {
			qryTeams->Next();
		}
		TDBVEUCResultSumData data=eucTeam->getResultSumData();
		tblTarget->Append();
		tblTargetteam->Value=eucTeam->TeamIdx;
		switch (itemtype) {
			case itAuswertungVR1:
				tblTargetNetto->Value=eucTeam->ErgebnisVRNetto;
				tblTargetBrutto->Value=eucTeam->ErgebnisVRBrutto;
				tblTargetBestDurchgang->Value=eucTeam->MaxDurchgangNettoVR;
				tblTargetSchnitt->Value=eucTeam->SchnittBruttoVR;
				break;
			case itAuswertungVR2int:
				tblTargetNetto->Value=eucTeam->ErgebnisVRNetto+eucTeam->ErgebnisZRNetto;
//				tblTargetBrutto->Value=eucTeam->ErgebnisVRBrutto+eucTeam->ErgebnisZRBrutto;
				tblTargetBestDurchgang->Value=eucTeam->MaxDurchgangNettoVR;
				tblTargetSchnitt->Value=eucTeam->SchnittBruttoVR;
				break;
			case itAuswertungVR2:
				tblTargetNetto->Value=eucTeam->ErgebnisVRNetto+eucTeam->ErgebnisZRNetto;
				tblTargetBrutto->Value=eucTeam->ErgebnisVRBrutto+eucTeam->ErgebnisZRBrutto;
				tblTargetBestDurchgang->Value=eucTeam->MaxDurchgangNettoVR;
				tblTargetSchnitt->Value=eucTeam->SchnittBruttoVR;
				break;
			case itAuswertungF:
				tblTargetNetto->Value=eucTeam->ErgebnisFNetto;
				tblTargetBrutto->Value=eucTeam->ErgebnisFBrutto;
				tblTargetBestDurchgang->Value=eucTeam->MaxDurchgangNettoF;
				tblTargetSchnitt->Value=eucTeam->SchnittBruttoF;
				break;
		default:
			;
		}
		tblTarget->Post();
		qryTeams->Next();
	}
	tblTarget->Close();
	
	//und jetzt noch den qualifiziert-Status in die DB eintragen
	int count=0;
	qryTarget2->Open();
	while ((!qryTarget2->Eof) && (count<dmMain->eucEUC->QualiFinale)) {
		qryTarget2->Edit();
		qryTarget2->FieldByName("qualifiziert")->AsBoolean=true;
		qryTarget2->Post();
		count++;

		qryTarget2->Next();
    }


  //jetzt noch ein paar Anpassungen
  brutto->Enabled=!scratch;
  QRLabel4->Enabled=!scratch;
  netto->Enabled=scratch;
  QRLabel3->Enabled=scratch;


  //jetzt m�ssen wir noch die neue Spalte "HDC Finale" anzeigen.
  //das aber nur, wenn wir in der letzten Vorrunde sind und �berhaupt
  //das HDC f�rs Finale separat ermittelt werden soll
	if (dmMain->eucEUC->FinaleHDC && ((itemtype==itAuswertungVR2) || (itemtype==itAuswertungVR2int))) {
		if (itemtype==itAuswertungVR2) {
			hdc_visible=false;
			const int diff=-120;
			QRLabel4->Left=540;
			brutto->Left=548;
			//schnitt->Left=460;
			QRLabel7->Enabled=false;
			hdc_finale->Enabled=false;
			QRLabel8->Enabled=false;
			schnitt->Enabled=false;
			QRLabel3->Enabled=false;
			netto->Enabled=false;
		}
		else {
			hdc_visible=true;
			const int diff=-120;
			QRLabel3->Left=424+diff;
			QRLabel4->Left=517+diff-30;
			QRLabel8->Left=460;
			netto->Left=424+diff;
			brutto->Left=529+diff-30;
			//schnitt->Left=460;
			QRLabel7->Enabled=true;
			hdc_finale->Enabled=true;
			QRLabel8->Enabled=true;
			schnitt->Enabled=true;
//			QRLabel3->Enabled=true;
//			netto->Enabled=true;
		}
	}
	else {
		hdc_visible=false;
		//normale Position der Steuerelemente
		QRLabel3->Left=424;
		QRLabel4->Left=517;

		netto->Left=423;
		brutto->Left=528;

		QRLabel7->Visible=false;
		hdc_finale->Enabled=false;
		QRLabel7->Enabled=false;
		hdc_finale->Enabled=false;
		QRLabel8->Enabled=false;
		schnitt->Enabled=false;
//		QRLabel3->Enabled=true;
//		netto->Enabled=true;
	}

	//und nun endlich Auswertung anzeigem
	qryTarget->Close();
  	qryTarget->Open();
	Screen->Cursor=crDefault;
}
//---------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team::CloseData()
{
	qryTeams->Close();
	deleteTable(dmMain->conMain,"auswertung_team");
}
//---------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team::DetailBand1BeforePrint(
      TQRCustomBand *Sender, bool &PrintBand)
{
	if (FItemType==itAuswertungVR2int) {
		return;
	}

  PrintBand=netto->DataSet->FieldByName(netto->DataField)->AsInteger!=0;

  counter++;
  hdc_finale->Enabled=hdc_visible && (qryTargetNetto->Value>0) && (counter<=dmMain->eucEUC->QualiFinale);

	if (counter==dmMain->eucEUC->QualiFinale) {
		DetailBand1->Height=48;
	}
	else {
		DetailBand1->Height=24;
  	}
}
//---------------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team::qrpAuswertungBeforePrint(
      TCustomQuickRep *Sender, bool &PrintReport)
{
	counter=0;
}
//---------------------------------------------------------------------------

void __fastcall TfrmAuswertung_Team::qryTeamsAfterScroll(TDataSet *DataSet)
{
	eucTeam->readFromDB(qryTeams->FieldByName("TeamIdx")->AsInteger);	
}
//---------------------------------------------------------------------------

